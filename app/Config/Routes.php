<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Main');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Login::index');
$routes->get('/login', 'Login::index');
$routes->get('/display', 'Login::display');
$routes->get('/getdisplay', 'Login::pulldisplay');
$routes->post('/pauth', 'Login::auth');


$routes->group('panel',['filter' => 'auth'], function($routes){
    $routes->get('home', 'Main::index');	
    $routes->get('home_nfc', 'Main::nfc');	
	$routes->get('grup', 'Main::grup');	
	$routes->get('grupajax', 'Main::grup_ajax');	
	$routes->post('d/grup_add', 'Main::grup_add');
	$routes->post('d/grup_update', 'Main::grup_add');
	$routes->post('d/grup_delete', 'Main::grup_delete');

	$routes->get('member', 'Main::member');	
	$routes->get('memberajax', 'Main::member_ajax');		 
	$routes->post('d/check_nis', 'Main::member_nis_check');		 	
	$routes->post('d/member_add', 'Main::member_add');
	$routes->post('d/member_update', 'Main::member_add');
	$routes->post('d/member_delete', 'Main::member_delete');
	
	$routes->get('d/member_detail/(:segment)', 'Main::member_json/$1');
	$routes->get('d/member_search', 'Main::member_search');
	$routes->get('d/getcard', 'Main::kartu_ajax');
	$routes->get('absensi', 'Main::daftar_absensi');
	$routes->get('absensi_ajax', 'Main::absensi_ajax');

	$routes->get('pesan', 'Main::pesan_wa');
	

	$routes->get('laporan', 'Main::daftar_laporan');
	$routes->get('laporan_manual', 'Main::daftar_laporan_manual');
	$routes->get('report_ajax', 'Main::laporan_absensi_ajax');
	$routes->get('report_manual_ajax', 'Main::laporan_absensi_ajax_B');
	$routes->get('riwayat', 'Main::daftar_riwayat');
	$routes->get('riwayat_ajax', 'Main::daftar_riwayat_ajax');
	$routes->get('set/year/(:segment)', 'Main::generate_calendar/$1');
	$routes->post('a/absensi', 'Main::absensi_member');
	$routes->post('a/absensi_manual', 'Main::absensi_manual');

	$routes->get('setting', 'Main::setting');
	$routes->get('getsetting', 'Main::setting_libur_ajax');
	$routes->post('setting/save', 'Main::setting_save');
	$routes->get('setting/reload', 'Main::setting_reload');
	$routes->get('setting_libur', 'Main::setting_libur');
	$routes->post('setting_libur/save', 'Main::setting_libur_save');
	$routes->post('setting_tanggal_libur/save', 'Main::setting_tanggal_save');
	$routes->get('setting_jam', 'Main::setting_jam');
	// $routes->post('setting_jam_absen/save', 'Main::setting_jam_save');
	$routes->post('d/jam_add', 'Main::jam_add');
	$routes->post('d/jam_update', 'Main::jam_add');
	$routes->post('d/jam_delete', 'Main::jam_delete');
	$routes->get('d/jam_absen', 'Main::jam_absen_ajax');
	$routes->get('devicenfc', 'Main::devicenfc');
	$routes->post('device/save', 'Main::device_save');
	$routes->get('d/tanggal_libur', 'Main::tanggal_libur_ajax');
	$routes->post('d/tanggal_delete', 'Main::tanggal_libur_delete');
	$routes->get('device', 'Main::device');
	$routes->get('notifikasi', 'Main::notifikasi');
	$routes->post('notifikasi/save', 'Main::notifikasi_save');
	$routes->post('tespesan', 'Main::percobaan');
     
	$routes->get('users', 'Main::user_admin');
	$routes->get('useradminajax', 'Main::user_admin_ajax');
	$routes->post('u/checkadmin', 'Main::admin_username_check');	
	$routes->post('u/admin_add', 'Main::admin_user_add');
	$routes->post('u/admin_delete', 'Main::admin_user_delete');

	$routes->get('historywa', 'Main::riwayatwa');	
	$routes->get('logwa_ajax', 'Main::riwayatwa_ajax');		

	$routes->add('logout/', 'Login::logout');    
     
	
});
// API ROUTING
$routes->get('/api', 'Absensi::index');
$routes->group('api',['filter' => 'apikey'], function($routes){
    
	$routes->add('absen', 'Absensi::addnfc');

	
});

$routes->get('notif_harian', 'Notifikasi::notif_harian_email');
$routes->get('notif_harian_wa', 'Notifikasi::notif_harian_wa');
$routes->get('qr_wa', 'Notifikasi::qrcode_wa');
$routes->get('qrimg_wa', 'Notifikasi::qrcode_image_wa');
$routes->get('device', 'Notifikasi::device_wa');
$routes->get('wa_tes', 'Notifikasi::testing');

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
