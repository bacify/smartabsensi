<?php

namespace App\Models;

use CodeIgniter\Model;

class Mgrup extends Model
{
    protected $table      = 'grup';
    protected $primaryKey = 'id_grup';
    protected $useTimestamps = true;
    protected $useSoftDeletes = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $useAutoIncrement = true;
    protected $allowedFields = ['id_grup','nama_grup'];
}