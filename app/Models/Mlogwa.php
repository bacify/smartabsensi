<?php

namespace App\Models;

use CodeIgniter\Model;

class Mlogwa extends Model
{
    protected $table      = 'log_wa';
    protected $primaryKey = 'id';
    
    

    protected $useAutoIncrement = true;
    protected $allowedFields = ['id','no','text','status','keterangan'];
}