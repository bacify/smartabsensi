<?php

namespace App\Models;

use CodeIgniter\Model;

class Mstorage extends Model
{
    protected $table      = 'temp_storage';
    protected $primaryKey = 'id_storage';
    
    protected $useAutoIncrement = true;
    protected $allowedFields = ['id_absensi','nfc_id','device'];
}