<?php

namespace App\Models;

use CodeIgniter\Model;

class Mapi extends Model
{
    protected $table      = 'api';
    protected $primaryKey = 'idkey';
    protected $useTimestamps = true;
    protected $useSoftDeletes = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $useAutoIncrement = true;
    protected $allowedFields = ['idkey','key_app'];
}