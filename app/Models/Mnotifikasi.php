<?php

namespace App\Models;

use CodeIgniter\Model;

class Mnotifikasi extends Model
{
    protected $table      = 'notifikasi';
    protected $primaryKey = 'id_notifikasi';
    protected $useTimestamps = true;
    protected $useSoftDeletes = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    

    protected $useAutoIncrement = true;
    protected $allowedFields = ['id_notifikasi','text','status'];
}