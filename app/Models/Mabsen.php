<?php

namespace App\Models;

use CodeIgniter\Model;

class Mabsen extends Model
{
    protected $table      = 'absensi';
    protected $primaryKey = 'id_absensi';
    protected $useTimestamps = true;
    protected $useSoftDeletes = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $useAutoIncrement = true;
    protected $allowedFields = ['id_absensi','member_id','kehadiran','keterangan','device_id','created_by','masuk','keluar','device_masuk','device_keluar','gambar_masuk','gambar_keluar'];
}