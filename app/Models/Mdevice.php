<?php

namespace App\Models;

use CodeIgniter\Model;

class Mdevice extends Model
{
    protected $table      = 'device';
    protected $primaryKey = 'id_device';
    protected $useTimestamps = true;
    protected $useSoftDeletes = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $useAutoIncrement = true;
    protected $allowedFields = ['id_device','keterangan','mode'];
}