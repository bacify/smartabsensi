<?php

namespace App\Models;

use CodeIgniter\Model;

class Mjam extends Model
{
    protected $table      = 'setting_jam';
    protected $primaryKey = 'id_jam';
    protected $useTimestamps = true;
    protected $useSoftDeletes = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $useAutoIncrement = true;
    protected $allowedFields = ['id_jam','nama_jam','jam_masuk_start','jam_masuk_end','jam_pulang_start','jam_pulang_end'];
}