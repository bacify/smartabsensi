<?php namespace App\Filters;
 
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;
use App\Models\Mapi; 
use App\Models\Mdevice;
use CodeIgniter\API\ResponseTrait;
use Config\Services;
 

class Apikey implements FilterInterface 
{
    use ResponseTrait;
    public function before(RequestInterface $request, $arguments = null)
    {
       //print_r($_SERVER);
		$key = $request->getPost('key');
        $device = $request->getPost('device');

        $api  = new Mapi();
        $apidata = $api->first();
        
		if($key != $apidata['key_app']){
          
			$response = [
                'status' => 200,
                'error' => false,
                'message' => 'APIKEY Salah',
                'data' => 'Salah!',
                'text' => 'ApiKey'
            ];
          
            return Services::response()
            ->setJSON($response)
            ->setStatusCode(400);
            
			die;
		}

        $dev = new Mdevice;
        $d = $dev->findAll();
        $devicelist = array();
        foreach ($d  as $x)
            $devicelist[] = $x['id_device'];

        if(!in_array($device,$devicelist)){
            
			$response = [
                'status' => 200,
                'error' => false,
                'message' => 'Device Tidak Terdaftar',
                'data' => 'Tak Terdaftar!',
                'text' => 'Device'
            ];
          
			 
            return Services::response()
            ->setJSON($response)
            ->setStatusCode(400);
            
			die;
        }
    }
 
    //--------------------------------------------------------------------
 
    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        // Do something here
    }
}