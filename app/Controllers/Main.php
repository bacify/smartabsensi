<?php

namespace App\Controllers;
use App\Models\Mabsen; 
use App\Models\Mgrup; 
use App\Models\Mjam; 
use App\Models\Msetting; 
use App\Models\Mmember; 
use App\Models\Musers; 
use App\Models\Mnotifikasi; 
use App\Models\Mdevice; 
use \Hermawan\DataTables\DataTable;
use App\Libraries\Api_wa; // Import library


class Main extends BaseController
{
	public function index()
	{
         
        $data['title']='Dashboard Absensi member';
		return view('home_nfc',$data);
    }
    public function nfc(){
        $data['title']='Dashboard Absensi member';
		return view('home_nfc',$data);
    }
    public function home(){
        if($this->request->getVar('kata_kunci')){
            echo 'aaa';
            $katakunci=$this->request->getVar('kata_kunci');
            return redirect()->to('/cari/'.urlencode($katakunci));            
        }     
		$data['title']='Limasys';
		return view('home',$data);
    }
    public function grup()
	{
        

        
        /* generate random token */
        $bytes = random_bytes(20);
        $token= bin2hex($bytes);
        $session = session();
        $session->set(['token'=>$token]);
        $session->markAsTempdata('token', 300);
        $data['token']=$token;
        $data['title']='Data Grup';
       
		return view('data_grup',$data);
    }
    public function grup_ajax()
	{
          
        $db = db_connect();
        $builder = $db->table('grup')
                        ->select('id_grup,nama_grup')                        
                        ->where('grup.deleted_at IS NULL');
                         

         
        return DataTable::of($builder)                
                ->add('action', function($row){ 
                        return  '<a href="javascript:void(0);" class="edit_record btn btn-outline-primary btn-sm" data-id="'.$row->id_grup.'" data-nama="'.$row->nama_grup.'"  title="Edit Nama Grup"><i class="cil-pencil"></i></a> '.
                            ' <a href="javascript:void(0);" class="delete_record btn btn-outline-danger btn-sm" data-id="'.$row->id_grup.'" title="Hapus Grup"> <i class="cil-trash"></i></a>';                   
                    
                }, 'last')                 
                ->hide('id_grup')
                ->addNumbering() //it will return data output with numbering on first column
               ->toJson();
    }
    public function grup_delete(){
        if( $this->request->getPost('id') !== NULL){		
            $session = session();	 
            $m=new Mgrup();
			$r=$m->delete($this->request->getPost('id'));			
			if($r){
                $session->setFlashdata('error', 'Grup berhasil dihapus');
			}	else{
                $session->setFlashdata('error', 'Grup GAGAL dihapus');
			}
		}			

        return redirect()->to('panel/grup');
    }
    public function grup_add(){
        $session = session();        
        $validation =  \Config\Services::validation();
        $validation->setRules(  [                                                              
                                 
                                'token' => 'required',                                
                                'nama' => 'required'
                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
        if($isDataValid){
            if($this->request->getPost('token')== $session->getTempdata('token')){
                $session->remove('token');                
                    $value=array();
                    $value['id_grup']=$this->request->getPost('ids');
                    $value['nama_grup']=$this->request->getPost('nama');
                  
                     
                    $m = new Mgrup();
                    $result = $m->save($value);
                    if($result){
                        if($this->request->getPost('ids') == '' || $this->request->getPost('ids') == '0'){
                            $id=$m->getInsertID();
                            //echo 'valid';
                            $session->setFlashdata('error', 'Data berhasil disimpan '.$id);         
                        }else{                         
                            $session->setFlashdata('error', 'Perubahan Data berhasil dilakukan');    
                        }                    
                    }else{
                        $session->setFlashdata('error', 'Update Data GAGAL');  
                    }
                
            }else{
                $session->setFlashdata('error', 'Permintaan Token tidak sesuai, Silahkan ulangi');                
                return redirect()->to('/panel/grup');
            }
        }
        else{
            $errors = $validation->getErrors();
            $session->setFlashdata('error', 'GAGAL. error:'.json_encode($errors));    
        }

        return redirect()->to('panel/grup');
    }
     

    public function member()
	{
        $grup= new Mgrup();
        $data['grup']= $grup->findAll();
        $jam= new Mjam();
        $data['jam_absen']= $jam->findAll();
        /* generate random token */
        $bytes = random_bytes(20);
        $token= bin2hex($bytes);
        $session = session();
        $session->set(['token'=>$token]);
        $session->markAsTempdata('token', 300);
        
        $data['token']=$token;
		$data['title']='Data member';
		return view('data_member',$data);
    }
    public function member_ajax()
	{
          
        $db = db_connect();
        $builder = $db->table('member')
                        ->select('id_member,nama,nfc_id,nama_grup,alamat,no_telp,grup_id,nama_jam,jam_id,notifikasi')
                        ->join('grup','grup.id_grup = member.grup_id')
                        ->join('setting_jam','setting_jam.id_jam = member.jam_id')
                        ->where('member.deleted_at IS NULL');
                         

         
        return DataTable::of($builder)                
                ->add('action', function($row){ 
                        return  '<a href="javascript:void(0);" class="edit_record btn btn-outline-primary btn-sm " data-id="'.$row->id_member.'" data-id="'.$row->id_member.'" data-nama="'.$row->nama.'" data-nfc="'.$row->nfc_id.'" data-grup="'.$row->grup_id.'" data-telp="'.$row->no_telp.'" data-alamat="'.$row->alamat.'" data-notifikasi="'.$row->notifikasi.'" data-jam="'.$row->jam_id.'" title="Edit User"><i class="cil-pencil"></i></a>'.                            
                            ' <a href="javascript:void(0);" class="delete_record btn btn-outline-danger btn-sm " data-id="'.$row->id_member.'" title="Hapus Transaksi"><i class="cil-trash"></i></a>';                   
                    
                }, 'last')                 
                ->hide('id_member')
                ->hide('grup_id')
                ->hide('jam_id')
                ->addNumbering() //it will return data output with numbering on first column
                ->filter(function ($builder, $request) {
        
                    if ($request->grup)
                        $builder->where('member.grup_id', $request->grup);
            
                })
               ->toJson();
    }

    public function member_nfc_check(){
        if( $this->request->getPost('nfc') !== NULL){
			$con=array();
            $con['nfc_id']=$this->request->getPost('nfc');
            $u= new Mmember();
            $user = $u->where($con)->findAll();
			
			if(count($user)>0){
				echo '0';
			}else{
				echo '1';
			}
		}else{
			echo '0';
		}
    }
    public function member_add(){
        $session = session();        
        $validation =  \Config\Services::validation();
        $validation->setRules(  [                                                              
                                'nokartu' => 'required',
                                'token' => 'required',
                                'telp' => 'required|numeric',
                                'nama' => 'required',
                                'notifikasi' =>'required'
                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
        if($isDataValid){
            if($this->request->getPost('token')== $session->getTempdata('token')){
                $session->remove('token');
                $con=array();
                $con['nfc_id']=$this->request->getPost('nokartu');
                $m = new Mmember();
                $x = $m->where($con)->findAll();

                if($this->request->getPost('ids') == '' && count($x)>0){
                    $session->setFlashdata('error', 'Identitas Sudah Dipakai');  
                }else{
                    // GET WA NUMBER 
                    $set = new Msetting();
                    $setting = $set->first();
                    
                    if($setting['wa_status']==1){
                        //echo 'aaaaa';
                        $wa = new Api_wa();
                        $r=$wa->ceknomor($this->request->getPost('telp'));
                        $rx= json_decode($r);
                         
                        if($rx->status == 'false'){
                             
                            $session->setFlashdata('error', 'No '.$rx->pesan);  
                            return redirect()->to('/panel/member');
                        }

                    }
                     
                        $value=array();
                        $value['id_member']=$this->request->getPost('ids');
                        $value['nama']=$this->request->getPost('nama');
                        $value['nfc_id']=$this->request->getPost('nokartu');
                        $value['alamat']=$this->request->getPost('alamat');
                        $value['grup_id']=$this->request->getPost('grup');
                        $value['jam_id']=$this->request->getPost('jam');
                        $value['no_telp']=$this->request->getPost('telp'); 
                        $value['notifikasi']=$this->request->getPost('notifikasi'); 

                        $m = new Mmember();
                        $result = $m->save($value);
                        if($result){
                            if($this->request->getPost('ids') == '' || $this->request->getPost('ids') == '0'){
                                $id=$m->getInsertID();

                                // delete temp table
                                $db      = \Config\Database::connect();
                                $builder = $db->table('temp_storage');
                                $builder->where('nfc_id', $value['nfc_id']);
                                $builder->delete();


                               // $this->notifikasi_pendaftaran($value['no_telp'],$value['nama']);
                                $setting = new Msetting();
                                $sett = $setting->first();
                                $arrdata= array('instansi'=>$sett['nama_perusahaan'],
                                                'nama' =>$value['nama'],
                                                'telp_instansi'=>'0',
                                                'grup'=>$value['grup_id'],
                                                'nfc_id'=>$value['nfc_id']);
                                $p = $this->m_pesan(2,$arrdata);

                                if($p['status']== 1 && $p['enable'] == 1){
                                    //kirim notifikasi pendaftaran 
                                    $wa = new Api_wa();
                                     $wa->send_wa($value['no_telp'],$p['pesan']);
                                }else{

                                }
                                
                                
                                


                                //echo 'valid';
                                $session->setFlashdata('error', 'Data berhasil disimpan '.$id);         
                            }else{                         
                                $session->setFlashdata('error', 'Perubahan Data berhasil dilakukan');    
                            }                    
                        }else{
                            $session->setFlashdata('error', 'Update Data GAGAL');  
                        }
                    
                }
            }else{
                $session->setFlashdata('error', 'Permintaan Token tidak sesuai, Silahkan ulangi');                
                return redirect()->to('/panel/member');
            }
        }
        else{
            $errors = $validation->getErrors();
            $session->setFlashdata('error', 'GAGAL. error:'.json_encode($errors));    
        }

        return redirect()->to('panel/member');
    }
    
    public function member_delete(){
        if( $this->request->getPost('id') !== NULL){		
            $session = session();	 
            $m=new Mmember();
            $value=array();
            $value['id_member']=$this->request->getPost('id');
            $value['nfc_id']=$this->request->getPost('nokartu');
            $result = $m->save($value);
            $r=false;
            if($result)
			    $r=$m->delete($this->request->getPost('id'));			

			if($r){
                $session->setFlashdata('error', 'user berhasil dihapus');
			}	else{
                $session->setFlashdata('error', 'user GAGAL dihapus');
			}
		}

			

		return redirect()->to('panel/member');
    }
    public function member_json($id){
        if($id==null)
		{
            $result['status']=0;
            $result['result']=array();;
            die();
        }	
        $m = new Mmember();
		$user=$m->find($id);
		$result=array();
		if($user!=''){
			$result['status']=1;
			$result['result']=$user;
		}else{
			$result['status']=0;
			$result['result']=array();;
		}
		header('Content-Type: application/json');
		
		echo  json_encode($result);
    }
    public function member_search(){
        $con=$this->request->getGet('term');
		if($con==''){
			echo json_encode(array());
			die();
		}
		$like=array();
		
		$like['nama'] = $con;		
        $cs= new Mmember();    
        $r = $cs->select('id_member,
                         nama,telp')->like($like)
                         ->where('deleted_at IS null')
                        ->findAll(10);
       
                $result=array();
                foreach($r as $rr){
                    $temp=array();
                    
                    $temp=array('value'=>$rr['id_member'], 'label'=>$rr['nama'].'| '.$rr['telp']);
                    $result[]=$temp;
                };
                

        header('Content-Type: application/json');
		echo json_encode($result);
    }

    public function daftar_absensi()
	{      

        $begin = new \DateTime( '-3 day' );
        $end = new \DateTime( 'now' );        
        $interval = new \DateInterval('P1D');
        $daterange = new \DatePeriod($begin, $interval ,$end);        
        $t=array();
        foreach($daterange as $date){
            $t[]=array('input'=>$date->format("Y-m-d"), 'value'=>$date->format("d-m-Y"));

        }      

        $t[]=array('input'=>$end->format("Y-m-d"), 'value'=>$end->format("d-m-Y"));;
        $data['tanggal']=array_reverse($t);

        $grup= new Mgrup();
        $data['grup']= $grup->findAll();
        
        /* generate random token */
        $bytes = random_bytes(20);
        $token= bin2hex($bytes);
        $session = session();
        $session->set(['token'=>$token]);
        $session->markAsTempdata('token', 300);
        $grup= new Mgrup();
        $data['grup']= $grup->findAll();
        
        $data['token']=$token;
		$data['title']='Data Absensi';
		return view('data_absensi',$data);
    }
    public function kartu_ajax()
	{
          
        $db = db_connect();
        $builder = $db->table('temp_storage')
                        ->select('id_storage,nfc_id,keterangan,temp_storage.created_at' )
                        ->join('device','temp_storage.device = device.id_device')          ;                                       
                        
                        
                         

         
        return DataTable::of($builder)                
                ->add('action', function($row){ 
                   return  '<button onclick="newmember(\''.$row->nfc_id.'\')" class="btn btn-info btn-sm"  title="Buat Member Baru"><i class="cil-note-add"></i></button>';                                               
                    
                }, 'last')   
                         
                ->hide('id_storage')                
                ->addNumbering() //it will return data output with numbering on first column                 
               ->toJson();
    }
    public function absensi_ajax()
	{
          
        $db = db_connect();
        $builder = $db->table('member_kalender')
                        ->select('id_absensi,COALESCE(kehadiran,tanggal) as kh,nama,nama_grup,COALESCE(masuk,"-") as masuk,COALESCE(keluar,"-") as keluar,COALESCE(IF(masuk IS NOT NULL AND keluar is NOT NULL,"OK",if((masuk is not null and keluar is null) or (keluar is not null and masuk is null),1,null)),keterangan,"A") as absen,id_member' )
                        ->join('absensi','absensi.member_id = member_kalender.member_id AND absensi.kehadiran = member_kalender.tanggal','LEFT')                                                 
                        ->join('member','member.id_member = member_kalender.member_id ')  
                        ->join('grup','grup.id_grup = member.grup_id ')  
                        ->where('member.grup_id', $this->request->getGet('grup'))						
						->where('member_kalender.tanggal', $this->request->getGet('tanggal'))						                                           
                        ->where('member.deleted_at IS NULL')    ;                    
                        
                        
                         

         
        return DataTable::of($builder)                
                ->add('action', function($row){ 
                    if($row->id_absensi == '')
                        return  '<a href="javascript:void(0);" class="add_record btn btn-info btn-sm"  data-id="'.$row->id_member.'" title="Tambahkan Keterangan"><i class="cil-note-add"></i></a>';                           
                    else
                        return  ' <a href="javascript:void(0);" class="edit_record btn btn-info btn-sm " data-id="'.$row->id_member.'" data-idabsen="'.$row->id_absensi.'" title="Edit keterangan"><i class="cil-info"></i></a>';                   
                    
                }, 'last')   
                ->edit('absen', function($row){
                    if($row->absen=='A')
                        return '<span class="badge badge-danger">'.$row->absen.'</span>';
                    else if($row->absen=='I')
                        return '<span class="badge badge-primary">'.$row->absen.'</span>';
                    else if($row->absen=='S')
                        return '<span class="badge badge-info">'.$row->absen.'</span>';
                    else 
                        return '<span class="badge badge-success">'.$row->absen.'</span>';
                    
                })              
                ->hide('id_absensi')                
                ->hide('id_member')                
                ->addNumbering() //it will return data output with numbering on first column                 
               ->toJson();
    }

    public function absensi_member(){
        $session = session();        
        $validation =  \Config\Services::validation();
        $validation->setRules(  [                                                              
                                'nfc' => 'required'                                 
                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
        if($isDataValid){
                $nfc= $this->request->getPost('nfc');                 

                $m = new Mmember();
                        $member = $m->where(array('nfc'=>$nfc))->first();
                         
                        if($member == ''){
                            $result['status'] = 0;
                            $result['text'] = 'Gagal! Data Tidak Ditemukan';
                        }else{
                            /* insert ABSENSI */
                            $a = new Mabsen();
                            $val = array('member_id'=>$member['id_member'],
                                        'keterangan'=>'M',
                                        'user_id'=>session()->get('uid'),
                                        'created_by'=>'AUTO');
                            $r=$a->save($val);
                            if($r){
                                $result['status']=1;
                                $result['member']=$member;
                                $result['text'] = 'Berhasil';
                            }else{
                                $result['status']=0;
                                $result['member']=$member;
                                $result['text'] = 'Gagal';
                            }
                        }
                
        }
        else{
            $errors = $validation->getErrors();
            $result['status']=0;            
            $result['text'] =json_encode($errors);            
        }

        header('Content-Type: application/json');		
		echo  json_encode($result);
    }

    public function absensi_manual(){
        $session = session();        
        $validation =  \Config\Services::validation();
        $validation->setRules(  [                                                              
                                'ids' => 'required|numeric',
                                'token' => 'required',
                                'created_by' => 'required',
                                'keterangan' => 'required'
                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
        if($isDataValid){
            if($this->request->getPost('token')== $session->getTempdata('token')){
                $session->remove('token');
                
                 
                    
                     
                        $value=array();
                        $value['id_absensi']=$this->request->getPost('id_absen');
                        $value['member_id']=$this->request->getPost('ids');
                        $keterangan = $this->request->getPost('keterangan');;
                        if($keterangan == 'M')
                            $value['masuk'] = date('H:i:s');
                        else if($keterangan == 'K')
                            $value['keluar'] = date('H:i:s');
                        else   
                        $value['keterangan']=$keterangan;
                        $value['created_by']=$this->request->getPost('created_by');
                        $value['user_id']=session()->get('uid');
                         
                        $m = new Mabsen();
                        $result = $m->save($value);
                        if($result){
                            if($this->request->getPost('id_absen') == '' || $this->request->getPost('id_absen') == '0'){
                                $id=$m->getInsertID();                                 
                                //echo 'valid';
                                $session->setFlashdata('error', 'Data berhasil disimpan '.$id);         
                            }else{                         
                                $session->setFlashdata('error', 'Perubahan Data berhasil dilakukan');    
                            }                    
                        }else{
                            $session->setFlashdata('error', 'Update Data GAGAL');  
                        }
                    
               
            }else{
                $session->setFlashdata('error', 'Permintaan Token tidak sesuai, Silahkan ulangi');                
                return redirect()->to('panel/absensi');
            }
        }
        else{
            $errors = $validation->getErrors();
            $session->setFlashdata('error', 'GAGAL. error:'.json_encode($errors));    
        }

        return redirect()->to('panel/absensi');
    }
    public function daftar_laporan()
	{
        $db = db_connect();
        $query = $db->query("select min(YEAR(tanggal)) as t from kalender");
        $r = $query->getResult();
         $min = $r[0]->t;
        $n = range($min,date('Y'));
        $data['tahun']=$n;
        $bulan = array(
                      ['input' => '1','value'=>'Januari'],
                      ['input' => '2','value'=>'Februari'],
                      ['input' => '3','value'=>'Maret'],
                      ['input' => '4','value'=>'April'],
                      ['input' => '5','value'=>'Mei'],
                      ['input' => '6','value'=>'Juni'],
                      ['input' => '7','value'=>'Juli'],
                      ['input' => '8','value'=>'Agustus'],
                      ['input' => '9','value'=>'September'],
                      ['input' => '10','value'=>'Oktober'],
                      ['input' => '11','value'=>'November'],
                      ['input' => '12','value'=>'Desember'],
                    );
         
        $data['bulan'] = $bulan;        

        $grup= new Mgrup();
        $data['grup']= $grup->findAll();
        $s=new Msetting();
        $data['setting']=$s->first();

        /* generate random token */
        $bytes = random_bytes(20);
        $token= bin2hex($bytes);
        $session = session();
        $session->set(['token'=>$token]);
        $session->markAsTempdata('token', 300);
        
        $data['token']=$token;
		$data['title']='Laporan Absensi member ';
		return view('data_laporan',$data);
    }
    public function daftar_laporan_manual()
	{
        $db = db_connect();
        $query = $db->query("select min(YEAR(tanggal)) as t from kalender");
        $r = $query->getResult();
         $min = $r[0]->t;
        $n = range($min,date('Y'));
        $data['tahun']=$n;
        $bulan = array(
                      ['input' => '1','value'=>'Januari'],
                      ['input' => '2','value'=>'Februari'],
                      ['input' => '3','value'=>'Maret'],
                      ['input' => '4','value'=>'April'],
                      ['input' => '5','value'=>'Mei'],
                      ['input' => '6','value'=>'Juni'],
                      ['input' => '7','value'=>'Juli'],
                      ['input' => '8','value'=>'Agustus'],
                      ['input' => '9','value'=>'September'],
                      ['input' => '10','value'=>'Oktober'],
                      ['input' => '11','value'=>'November'],
                      ['input' => '12','value'=>'Desember'],
                    );
         
        $data['bulan'] = $bulan;        

        $grup= new Mgrup();
        $data['grup']= $grup->findAll();
        
        /* generate random token */
        $bytes = random_bytes(20);
        $token= bin2hex($bytes);
        $session = session();
        $session->set(['token'=>$token]);
        $session->markAsTempdata('token', 300);
        
        $data['token']=$token;
		$data['title']='Laporan Absensi member ';
		return view('data_laporan_manual',$data);
    }
    public function laporan_absensi_ajax()
	{
          
        $db = db_connect();
        $builder = $db->table('absensi')
                        ->select('member_kalender.member_id,id_absensi,nfc_id,nama,nama_grup,
                        max(case when day(tanggal) = 1 AND date(tanggal) <= date(NOW())  then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(member_kalender.tanggal)),"X",null),IF(masuk IS NOT NULL AND keluar is NOT NULL,2,if((masuk is not null and keluar is null) or (keluar is not null and masuk is null),1,null)),keterangan,"A") end) as "A1",
                                max(case when day(tanggal) = 2 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(member_kalender.tanggal)),"X",null),IF(masuk IS NOT NULL AND keluar is NOT NULL,2,if((masuk is not null and keluar is null) or (keluar is not null and masuk is null),1,null)),keterangan,"A") end) as "A2",
                                max(case when day(tanggal) = 3 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(member_kalender.tanggal)),"X",null),IF(masuk IS NOT NULL AND keluar is NOT NULL,2,if((masuk is not null and keluar is null) or (keluar is not null and masuk is null),1,null)),keterangan,"A") end) as "A3",
                                max(case when day(tanggal) = 4 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(member_kalender.tanggal)),"X",null),IF(masuk IS NOT NULL AND keluar is NOT NULL,2,if((masuk is not null and keluar is null) or (keluar is not null and masuk is null),1,null)),keterangan,"A") end) as "A4",
                                max(case when day(tanggal) = 5 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(member_kalender.tanggal)),"X",null),IF(masuk IS NOT NULL AND keluar is NOT NULL,2,if((masuk is not null and keluar is null) or (keluar is not null and masuk is null),1,null)),keterangan,"A") end) as "A5",
                                max(case when day(tanggal) = 6 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(member_kalender.tanggal)),"X",null),IF(masuk IS NOT NULL AND keluar is NOT NULL,2,if((masuk is not null and keluar is null) or (keluar is not null and masuk is null),1,null)),keterangan,"A") end) as "A6",
                                max(case when day(tanggal) = 7 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(member_kalender.tanggal)),"X",null),IF(masuk IS NOT NULL AND keluar is NOT NULL,2,if((masuk is not null and keluar is null) or (keluar is not null and masuk is null),1,null)),keterangan,"A") end) as "A7",
                                max(case when day(tanggal) = 8 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(member_kalender.tanggal)),"X",null),IF(masuk IS NOT NULL AND keluar is NOT NULL,2,if((masuk is not null and keluar is null) or (keluar is not null and masuk is null),1,null)),keterangan,"A") end) as "A8",
                                max(case when day(tanggal) = 9 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(member_kalender.tanggal)),"X",null),IF(masuk IS NOT NULL AND keluar is NOT NULL,2,if((masuk is not null and keluar is null) or (keluar is not null and masuk is null),1,null)),keterangan,"A") end) as "A9",
                                max(case when day(tanggal) = 10 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(member_kalender.tanggal)),"X",null),IF(masuk IS NOT NULL AND keluar is NOT NULL,2,if((masuk is not null and keluar is null) or (keluar is not null and masuk is null),1,null)),keterangan,"A") end) as "A10",
                                max(case when day(tanggal) = 11 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(member_kalender.tanggal)),"X",null),IF(masuk IS NOT NULL AND keluar is NOT NULL,2,if((masuk is not null and keluar is null) or (keluar is not null and masuk is null),1,null)),keterangan,"A") end) as "A11",
                                max(case when day(tanggal) = 12 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(member_kalender.tanggal)),"X",null),IF(masuk IS NOT NULL AND keluar is NOT NULL,2,if((masuk is not null and keluar is null) or (keluar is not null and masuk is null),1,null)),keterangan,"A") end) as "A12",
                                max(case when day(tanggal) = 13 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(member_kalender.tanggal)),"X",null),IF(masuk IS NOT NULL AND keluar is NOT NULL,2,if((masuk is not null and keluar is null) or (keluar is not null and masuk is null),1,null)),keterangan,"A") end) as "A13",
                                max(case when day(tanggal) = 14 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(member_kalender.tanggal)),"X",null),IF(masuk IS NOT NULL AND keluar is NOT NULL,2,if((masuk is not null and keluar is null) or (keluar is not null and masuk is null),1,null)),keterangan,"A") end) as "A14",
                                max(case when day(tanggal) = 15 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(member_kalender.tanggal)),"X",null),IF(masuk IS NOT NULL AND keluar is NOT NULL,2,if((masuk is not null and keluar is null) or (keluar is not null and masuk is null),1,null)),keterangan,"A") end) as "A15",
                                max(case when day(tanggal) = 16 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(member_kalender.tanggal)),"X",null),IF(masuk IS NOT NULL AND keluar is NOT NULL,2,if((masuk is not null and keluar is null) or (keluar is not null and masuk is null),1,null)),keterangan,"A") end) as "A16",
                                max(case when day(tanggal) = 17 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(member_kalender.tanggal)),"X",null),IF(masuk IS NOT NULL AND keluar is NOT NULL,2,if((masuk is not null and keluar is null) or (keluar is not null and masuk is null),1,null)),keterangan,"A") end) as "A17",
                                max(case when day(tanggal) = 18 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(member_kalender.tanggal)),"X",null),IF(masuk IS NOT NULL AND keluar is NOT NULL,2,if((masuk is not null and keluar is null) or (keluar is not null and masuk is null),1,null)),keterangan,"A") end) as "A18",
                                max(case when day(tanggal) = 19 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(member_kalender.tanggal)),"X",null),IF(masuk IS NOT NULL AND keluar is NOT NULL,2,if((masuk is not null and keluar is null) or (keluar is not null and masuk is null),1,null)),keterangan,"A") end) as "A19",
                                max(case when day(tanggal) = 20 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(member_kalender.tanggal)),"X",null),IF(masuk IS NOT NULL AND keluar is NOT NULL,2,if((masuk is not null and keluar is null) or (keluar is not null and masuk is null),1,null)),keterangan,"A") end) as "A20",
                                max(case when day(tanggal) = 21 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(member_kalender.tanggal)),"X",null),IF(masuk IS NOT NULL AND keluar is NOT NULL,2,if((masuk is not null and keluar is null) or (keluar is not null and masuk is null),1,null)),keterangan,"A") end) as "A21",
                                max(case when day(tanggal) = 22 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(member_kalender.tanggal)),"X",null),IF(masuk IS NOT NULL AND keluar is NOT NULL,2,if((masuk is not null and keluar is null) or (keluar is not null and masuk is null),1,null)),keterangan,"A") end) as "A22",
                                max(case when day(tanggal) = 23 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(member_kalender.tanggal)),"X",null),IF(masuk IS NOT NULL AND keluar is NOT NULL,2,if((masuk is not null and keluar is null) or (keluar is not null and masuk is null),1,null)),keterangan,"A") end) as "A23",
                                max(case when day(tanggal) = 24 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(member_kalender.tanggal)),"X",null),IF(masuk IS NOT NULL AND keluar is NOT NULL,2,if((masuk is not null and keluar is null) or (keluar is not null and masuk is null),1,null)),keterangan,"A") end) as "A24",
                                max(case when day(tanggal) = 25 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(member_kalender.tanggal)),"X",null),IF(masuk IS NOT NULL AND keluar is NOT NULL,2,if((masuk is not null and keluar is null) or (keluar is not null and masuk is null),1,null)),keterangan,"A") end) as "A25",
                                max(case when day(tanggal) = 26 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(member_kalender.tanggal)),"X",null),IF(masuk IS NOT NULL AND keluar is NOT NULL,2,if((masuk is not null and keluar is null) or (keluar is not null and masuk is null),1,null)),keterangan,"A") end) as "A26",
                                max(case when day(tanggal) = 27 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(member_kalender.tanggal)),"X",null),IF(masuk IS NOT NULL AND keluar is NOT NULL,2,if((masuk is not null and keluar is null) or (keluar is not null and masuk is null),1,null)),keterangan,"A") end) as "A27",
                                max(case when day(tanggal) = 28 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(member_kalender.tanggal)),"X",null),IF(masuk IS NOT NULL AND keluar is NOT NULL,2,if((masuk is not null and keluar is null) or (keluar is not null and masuk is null),1,null)),keterangan,"A") end) as "A28",
                                max(case when day(tanggal) = 29 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(member_kalender.tanggal)),"X",null),IF(masuk IS NOT NULL AND keluar is NOT NULL,2,if((masuk is not null and keluar is null) or (keluar is not null and masuk is null),1,null)),keterangan,"A") end) as "A29",
                                max(case when day(tanggal) = 30 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(member_kalender.tanggal)),"X",null),IF(masuk IS NOT NULL AND keluar is NOT NULL,2,if((masuk is not null and keluar is null) or (keluar is not null and masuk is null),1,null)),keterangan,"A") end) as "A30",
                                max(case when day(tanggal) = 31 AND date(tanggal) <= date(NOW()) then COALESCE(IF(EXISTS(select tanggal from tanggal_libur where date(tanggal_libur.tanggal) = date(member_kalender.tanggal)),"X",null),IF(masuk IS NOT NULL AND keluar is NOT NULL,2,if((masuk is not null and keluar is null) or (keluar is not null and masuk is null),1,null)),keterangan,"A") end) as "A31"
                                 ')
                        ->join('member_kalender','absensi.member_id = member_kalender.member_id and date(absensi.kehadiran) = date(member_kalender.tanggal)','right')
                        ->join('member','member_kalender.member_id = member.id_member ')    
                        ->join('grup','grup.id_grup = member.grup_id')                                                             
                        ->where('member.deleted_at IS NULL')
                        ->where('absensi.deleted_at IS NULL')  
						->where('Year(tanggal)', $this->request->getGet('tahun'))						
						->where('month(tanggal)', $this->request->getGet('bulan'))
						->where('member.grup_id', $this->request->getGet('grup'))
                        ->groupBy('id_member,nama,nama_grup');
                         
			//print_r($builder->getCompiledSelect());  //print query

					
         
        $result = DataTable::of($builder) 
                ->hide('id_absensi') 
                ->hide('member_id')                     
                ->hide('nama_grup')                
                ->addNumbering() //it will return data output with numbering on first column
                // ->filter(function ($builder, $request) {        
                    // if ($request->tahun)
                        // $builder->where('Year(tanggal)', $request->tahun);
                    // if ($request->bulan)
                        // $builder->where('month(tanggal)', $request->bulan);  
                    // if ($request->grup)
                        // $builder->where('member_kalender.grup', $request->grup);           
                // })
               ->toJson();


        return $result;
        
			    
    }
    public function get_break_date($bulan_a,$tahun_a,$bulan_b,$tahun_b){
        $set = new Msetting();
        $setting= $set->first();

        $libur = $setting['hari_libur'];
        $hari = explode(',',$libur);

        $listday=array();
        if($hari[0] == 1)
            $listday[]=1;
        if($hari[1] == 1)
            $listday[]=2;
        if($hari[2] == 1)
            $listday[]=3;
        if($hari[3] == 1)
            $listday[]=4;
        if($hari[4] == 1)
            $listday[]=5;
        if($hari[5] == 1)
            $listday[]=6;
        if($hari[6] == 1)
            $listday[]=0;

        
        
        $t_a = new \DateTime($tahun_a.'-'.$bulan_a.'-01');
        $lastdate = date('t',strtotime($tahun_b.'-'.$bulan_b.'-01'));
       // echo $lastdate;
        $t_b = new \DateTime($tahun_b.'-'.$bulan_b.'-'.$lastdate);
        $t_b = $t_b->modify( '+1 day' );
         
        $interval = new \DateInterval('P1D');
        $daterange = new \DatePeriod($t_a, $interval ,$t_b);
        $listbreakdate=array();
        foreach($daterange as $date){
            $day = $date->format('w');

            if(in_array($day, $listday))
            $listbreakdate[] =$date->format('Y-m-d');
        }

        // REMOVE DATE IF SAME WITH DB
        $db = db_connect();
        $builder = $db->table('tanggal_libur')
                        ->select('id_tanggal,tanggal' );
        $r = $builder->get();     
        $tgl_libur=array();
        foreach ($r->getResult() as $row)
        {
            if (($key = array_search($row->tanggal, $listbreakdate)) !== false) {
                unset($listbreakdate[$key]);
            }

        }    
         
        return $listbreakdate;
    }
    public function laporan_absensi_ajax_B()
	{
        
        $tahun_a =$this->request->getGet('tahun');
        $bulan_a =$this->request->getGet('bulan');
        $tahun_b =$this->request->getGet('tahunakhir');
        $bulan_b =$this->request->getGet('bulanakhir');

        $listbreakdate = $this->get_break_date($bulan_a,$tahun_a,$bulan_b,$tahun_b);
        
         

        $db = db_connect();
        $builder = $db->table('laporan')
                        ->select('mid,nfc_id,nama,nama_grup,
                        SUM(CASE WHEN note = "A" AND date(tanggal) <= date(NOW()) THEN 1 ELSE 0 END) AS A, 
                        SUM(CASE WHEN note = "I" AND date(tanggal) <= date(NOW()) THEN 1 ELSE 0 END) AS I,
                        SUM(CASE WHEN note = "S" AND date(tanggal) <= date(NOW()) THEN 1 ELSE 0 END) AS S,
                        SUM(CASE WHEN note = "1" AND date(tanggal) <= date(NOW()) THEN 1 ELSE 0 END) AS M1,
                        SUM(CASE WHEN note = "2" AND date(tanggal) <= date(NOW()) THEN 1 ELSE 0 END) AS M2,
                        SUM(CASE WHEN note = "X" AND date(tanggal) <= date(NOW()) THEN 1 ELSE 0 END) AS X',
                        )                        
                        ->join('member','member.id_member = laporan.mid ')                        
                        ->join('grup','grup.id_grup = member.grup_id ')                        
                        ->where('member.deleted_at IS NULL')   
						->where('Year(tanggal)>=', $this->request->getGet('tahun'))						
						->where('month(tanggal)>=', $this->request->getGet('bulan'))
						->where('Year(tanggal)<=', $this->request->getGet('tahunakhir'))
						->where('month(tanggal)<=', $this->request->getGet('bulanakhir'))
						->where('month(tanggal)<=', $this->request->getGet('bulanakhir'))
                        ->whereNotIn('tanggal',$listbreakdate)
						->where('member.grup_id', $this->request->getGet('grup'))							
                        ->groupBy('mid');
                         
                         

         
        return DataTable::of($builder) 
                ->edit('X', function($row){
                    $tahun_a =$this->request->getGet('tahun');
                    $bulan_a =$this->request->getGet('bulan');
                    $tahun_b =$this->request->getGet('tahunakhir');
                    $bulan_b =$this->request->getGet('bulanakhir');

                    $listbreakdate = $this->get_break_date($bulan_a,$tahun_a,$bulan_b,$tahun_b);
                    $now = (int)$row->X;
                    $add = count($listbreakdate);
                    
                    $total = $now + $add ;
                    
                    return $total;
                    
                    
                })  
                ->hide('mid')                
                ->hide('nama_grup')                
                ->addNumbering() //it will return data output with numbering on first column
                // ->filter(function ($builder, $request) {        
                    // if ($request->tahun)
                        // $builder->where('Year(tanggal_absen)>=', $request->tahun);
                    // if ($request->bulan)
                        // $builder->where('month(tanggal_absen)>=', $request->bulan);  
                    // if ($request->tahunakhir)
                        // $builder->where('Year(tanggal_absen)<=', $request->tahunakhir);
                    // if ($request->bulanakhir)
                        // $builder->where('month(tanggal_absen)<=', $request->bulanakhir);  
                    // if ($request->grup)
                        // $builder->where('laporan_absensi.grup', $request->grup);           
                // })
               ->toJson();
    }

    public function daftar_riwayat()
	{
        
		$data['title']='Riwayat Absensi member ';
		return view('data_riwayat',$data);
    }
    public function daftar_riwayat_ajax()
	{
          
        $db = db_connect();
        $builder = $db->table('riwayat_absen')
                        ->select('id_absensi,waktu,device_absen,nama,nama_grup,ket,gambar');
                     
						
                       
                         
			//print_r($builder->getCompiledSelect());  //print query

					
         
        return DataTable::of($builder)
                ->edit('gambar', function($row){
                    if($row->gambar!= NULL)
                        return '<a href="'.base_url('/uploads/'.$row->gambar).'" data-lightbox="'.$row->waktu.'">
                      <img src="'.base_url('/uploads/'.$row->gambar).'" class="img-fluid img-thumbnail" alt="...">
                    </a> ';
                    else                    
                     return '<a href="'.base_url('/uploads/noimage.png').'" data-lightbox="'.$row->waktu.'">
                      <img src="'.base_url('/uploads/noimage.png').'" class="img-fluid img-thumbnail" alt="...">
                    </a> ';

                    // if($row->gambar!= NULL)
                    // return '<picture><a href="'.base_url('/uploads/'.$row->gambar).'" data-lightbox="'.$row->gambar.'">
                    //   <source srcset="'.base_url('/uploads/'.$row->gambar).'" type="image/svg+xml">
                    //   <img src="'.base_url('/uploads/'.$row->gambar).'" class="img-fluid img-thumbnail" alt="...">
                    // </a></picture> ';
                    // else
                    //      return '<picture><a href="'.base_url('/uploads/noimage.png').'" data-lightbox="'.$row->gambar.'">
                    //   <source srcset="'.base_url('/uploads/noimage.png').'" type="image/svg+xml">
                    //   <img src="'.base_url('/uploads/noimage.png').'" class="img-fluid img-thumbnail" alt="...">
                    // </a></picture> ';
                        
                })
                ->hide('id_absensi')
                ->addNumbering() //it will return data output with numbering on first column
               ->toJson();
			    
    }

    public function pesan_wa()
	{
        
        
        
        $grup= new Mgrup();
        $data['grup']= $grup->findAll();                
        /* generate random token */
        $bytes = random_bytes(20);
        $token= bin2hex($bytes);
        $session = session();
        $session->set(['token'=>$token]);
        $session->markAsTempdata('token', 300);        
        $data['token']=$token;
		$data['title']='Kirim Pesan WhatsApp';
		return view('kirim_pesan',$data);
    }

    public function kirim_pesan_wa()
	{
        
        
        
        $session = session();        
        $validation =  \Config\Services::validation();
        $validation->setRules(  [                                                              
                                 
                                'token' => 'required',                                
                                'nomor' => 'required|numeric',
                                'pesan' => 'required',
                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
        if($isDataValid){
            if($this->request->getPost('token')== $session->getTempdata('token')){
                $session->remove('token');                
                    $value=array();
                    $value['id_grup']=$this->request->getPost('ids');
                    $value['nama_grup']=$this->request->getPost('nama');
                    $value['nfc']=$this->request->getPost('nfc');
                     
                    $m = new Mgrup();
                    $result = $m->save($value);
                    if($result){
                        if($this->request->getPost('ids') == '' || $this->request->getPost('ids') == '0'){
                            $id=$m->getInsertID();
                            //echo 'valid';
                            $session->setFlashdata('error', 'Data berhasil disimpan '.$id);         
                        }else{                         
                            $session->setFlashdata('error', 'Perubahan Data berhasil dilakukan');    
                        }                    
                    }else{
                        $session->setFlashdata('error', 'Update Data GAGAL');  
                    }
                
            }else{
                $session->setFlashdata('error', 'Permintaan Token tidak sesuai, Silahkan ulangi');                
                return redirect()->to('/panel/pesan');
            }
        }
        else{
            $errors = $validation->getErrors();
            $session->setFlashdata('error', 'GAGAL. error:'.json_encode($errors));    
        }

        return redirect()->to('panel/pesan');
    }

    public function setting(){
    
        /* generate random token */
        $bytes = random_bytes(20);
        $token= bin2hex($bytes);
        $session = session();
        $session->set(['token'=>$token]);
        $session->markAsTempdata('token', 300);
        
        
        $data['token']=$token;
        $data['title']='Pengaturan Aplikasi';
        $s=new Msetting();
        $data['setting']=$s->first();
        return view('setting',$data);

        
   
    }
    public function device(){
    
        $data['title']='Status Device WhatsApp';
        $wa = new Api_wa();
        $r=$wa->device();
        $x=$wa->view_qrcode();
         
        $data['device']=json_decode($r);
        $data['qr']=json_decode($x);
        return view('device',$data);

        
   
    }

    public function devicenfc(){
    
        /* generate random token */
        $bytes = random_bytes(20);
        $token= bin2hex($bytes);
        $session = session();
        $session->set(['token'=>$token]);
        $session->markAsTempdata('token', 300);
        
        
        $data['token']=$token;
        $data['title']='Pengaturan Aplikasi';
        $s=new Mdevice();
        $data['device']=$s->findAll();
        return view('devicenfc',$data);

        
   
    }
    public function setting_save(){
        $session = session();        
        $validation =  \Config\Services::validation();
        $validation->setRules(  [ 
                                    'masuk_start' => 'required',
                                    'masuk_end' => 'required',
                                    'keluar_start' => 'required',
                                    'keluar_end' => 'required',
                                     
                                    'token' => 'required',
                                    'instansi' => 'required',
                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
       
        if($isDataValid){
            if($this->request->getPost('token')== $session->getTempdata('token')){
                $session->remove('token');
                    $value=array();
                    $value['id_setting']=1;
                    $value['nama_perusahaan']=$this->request->getPost('instansi');
                    $value['jam_masuk_start']=$this->request->getPost('masuk_start');
                    $value['jam_masuk_end']=$this->request->getPost('masuk_end');
                    $value['jam_pulang_start']=$this->request->getPost('keluar_start');
                    $value['jam_pulang_end']=$this->request->getPost('keluar_end');
                    $value['wa_token']=$this->request->getPost('watoken');
                    $value['email']=$this->request->getPost('email');
                    $value['email_jam']=$this->request->getPost('email_jam');
                    $value['wa_jam']=$this->request->getPost('wa_jam');
                    
                    if($this->request->getPost('status') !==null)
                        $value['wa_status']=1;
                    else
                        $value['wa_status']=0;
                    
                    if($this->request->getPost('email_notif') !==null && $this->request->getPost('email')!=='')
                        $value['email_notif']=1;
                    else
                        $value['email_notif']=0;                    
                     
                    if($this->request->getPost('wa_notif') !==null)
                        $value['wa_notif']=1;
                    else
                        $value['wa_notif']=0;

                    $m = new Msetting();
                    
                    $result = $m->save($value);
                    if($result){
                            
                            $this->setting_reload();                  
                            $session->setFlashdata('error', 'Perubahan Data berhasil dilakukan');    
                                           
                    }else{
                        $session->setFlashdata('error', 'Update Data GAGAL');  
                    }
                
            }else{
                $session->setFlashdata('error', 'Permintaan Token tidak sesuai, Silahkan ulangi');                
                return redirect()->to('/panel/setting');
            }
        }
        else{
            $errors = $validation->getErrors();
            $session->setFlashdata('error', 'GAGAL. error:'.json_encode($errors));    
        }
        return redirect()->to('panel/setting');
    }

    public function setting_reload(){
        
        $wa = new Api_wa();
        $return = $wa->reload();         
        $rr = json_decode($return);
        $ms = new Msetting();
         
        
        
        if($rr->status== 'true'){
            
            $data =array();                
            $data['wa_exp']=$rr->device->expired;
            $data['id_setting']=1;
            $r=$wa->device();
            
            $r = json_decode($r);
            
             
            if($r->status == 'false'){            
                $data['wa_notif']=0;
            }
            
            $x= $ms->save($data);

        }else{
            
            $data['wa_status'] = 0;
            $data['wa_exp']='';
            $data['id_setting']=1;
            $data['wa_notif']=0;
            
            $ms->save($data);
        }
         return $return;
    }

    public function setting_libur(){
    
        /* generate random token */
        $bytes = random_bytes(20);
        $token= bin2hex($bytes);
        $session = session();
        $session->set(['token'=>$token]);
        $session->markAsTempdata('token', 300);
        
        
        $data['token']=$token;
        $data['title']='Pengaturan Hari Libur';
        $s=new Msetting();
        $data['setting']=$s->select('hari_libur')->first();
        return view('setting_libur',$data);        
   
    }
    public function setting_libur_save(){
        $session = session();        
        $validation =  \Config\Services::validation();
        $validation->setRules(  [ 
                                                                        
                                    'token' => 'required',
                                     
                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
        $day = $this->request->getPost('day');
         
        if($isDataValid){
            if($this->request->getPost('token')== $session->getTempdata('token')){
                $session->remove('token');
                    $value=array();
                    $value['id_setting']=1;                   
                    $day = $this->request->getPost('day');
                    
                    if(!isset($day[0])){
                        $day[0]=0;
                    }else{
                        $day[0]=1;
                    }
                    
                    if(!isset($day[1])){
                        $day[1]=0;
                    }else{
                        $day[1]=1;
                    }
                    
                    if(!isset($day[2])){
                        $day[2]=0;
                    }else{
                        $day[2]=1;
                    }

                    if(!isset($day[3])){
                        $day[3]=0;
                    }else{
                        $day[3]=1;
                    }

                    if(!isset($day[4])){
                        $day[4]=0;
                    }else{
                        $day[4]=1;
                    }

                    if(!isset($day[5])){
                        $day[5]=0;
                    }else{
                        $day[5]=1;
                    }

                    if(!isset($day[6])){
                        $day[6]=0;
                    }else{
                        $day[6]=1;
                    }
                    ksort($day);
                    $value['hari_libur']= implode(',',$day);
                    
                    
                    $m = new Msetting();
                    
                    $result = $m->save($value);
                    if($result){
                            
                                          
                            $session->setFlashdata('error', 'Perubahan Data berhasil dilakukan');    
                                           
                    }else{
                        $session->setFlashdata('error', 'Update Data GAGAL');  
                    }
                
            }else{
                $session->setFlashdata('error', 'Permintaan Token tidak sesuai, Silahkan ulangi');                
                return redirect()->to('/panel/setting_libur');
            }
        }
        else{
            $errors = $validation->getErrors();
            $session->setFlashdata('error', 'GAGAL. error:'.json_encode($errors));    
        }
        return redirect()->to('panel/setting_libur');
    }
    public function setting_tanggal_save(){
        $session = session();        
        
        $validation =  \Config\Services::validation();
        $validation->setRules(  [ 
                                    'tanggal_libur' => 'required',
                                    'token' => 'required'
                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
       
        if($isDataValid){
            if($this->request->getPost('token')== $session->getTempdata('token')){
                $session->remove('token');
                    $value=array();
                    
                    $tanggal=explode('-',$this->request->getPost('tanggal_libur'));
                    $value['tanggal']=$tanggal[2].'-'.$tanggal[1].'-'.$tanggal[0];
                    $db      = \Config\Database::connect();
                    $builder = $db->table('tanggal_libur');
                                       
                    $result = $builder->insert($value);
                    if($result){
                                                                   
                            $session->setFlashdata('error', 'Tanggal Berhasil Ditambah');    
                                           
                    }else{
                        $session->setFlashdata('error', 'Update Data GAGAL');  
                    }
                
            }else{
                $session->setFlashdata('error', 'Permintaan Token tidak sesuai, Silahkan ulangi');                
                return redirect()->to('/panel/setting_libur');
            }
        }
        else{
            $errors = $validation->getErrors();
            $session->setFlashdata('error', 'GAGAL. error:'.json_encode($errors));    
        }
        return redirect()->to('panel/setting_libur');
    }
    public function tanggal_libur_ajax()
	{
          
        $db = db_connect();
        $builder = $db->table('tanggal_libur')
                        ->select('id_tanggal,tanggal' );                        
                        
                        
                         

         
        return DataTable::of($builder)                
                ->add('action', function($row){ 
                   return  '<button onclick="deleteTanggal(\''.$row->id_tanggal.'\')" class="btn btn-info btn-sm"  title="Hapus Tanggal"><i class="cil-x-circle"></i></button>';                                               
                    
                }, 'last')   
                         
                ->hide('id_tanggal')                
                ->addNumbering() //it will return data output with numbering on first column                 
               ->toJson();
    }
   
    public function tanggal_libur_delete(){
        if( $this->request->getPost('id') !== NULL){		
            $session = session();	
            $db      = \Config\Database::connect(); 
            $builder = $db->table('tanggal_libur');
			$r=$builder->delete(['id_tanggal'=>$this->request->getPost('id')]);			
			if($r){
                $session->setFlashdata('pesan', 'Tanggal berhasil dihapus');
			}	else{
                $session->setFlashdata('pesan', 'Tanggal GAGAL dihapus');
			}
		}

			

		return redirect()->to('panel/setting_libur');
    }

    public function setting_jam(){
    
        /* generate random token */
        $bytes = random_bytes(20);
        $token= bin2hex($bytes);
        $session = session();
        $session->set(['token'=>$token]);
        $session->markAsTempdata('token', 300);
        
        
        $data['token']=$token;
        $data['title']='Pengaturan Jam Absensi';
        $s=new Mjam();
        $data['setting_jam']=$s->findAll();
        return view('setting_jam',$data);        
   
    }
    public function jam_absen_ajax()
    {
          
        $db = db_connect();
        $builder = $db->table('setting_jam')
                        ->select('id_jam,nama_jam,concat(jam_masuk_start," - ",jam_masuk_end) as masuk,concat(jam_pulang_start," - ",jam_pulang_end) as pulang,jam_masuk_start,jam_masuk_end,jam_pulang_start,jam_pulang_end' )
                        ->where('deleted_at IS NULL');                        
                        
                        
                         

         
        return DataTable::of($builder)                
                ->add('action', function($row){ 
                    if($row->id_jam != 1)                   
                        return  '<a href="javascript:void(0);" class="edit_record btn btn-outline-primary btn-sm " data-id="'.$row->id_jam.'" data-nama="'.$row->nama_jam.'" data-masuk_start="'.$row->jam_masuk_start.'" data-masuk_end="'.$row->jam_masuk_end.'" data-keluar_start="'.$row->jam_pulang_start.'" data-keluar_end="'.$row->jam_pulang_end.'" title="Edit User"><i class="cil-pencil"></i></a>
                        <button onclick="deleteJam(\''.$row->id_jam.'\')" class="btn btn-info btn-sm"  title="Hapus Tanggal"><i class="cil-x-circle"></i></button>';
                    else
                        return '<a href="javascript:void(0);" class="edit_record btn btn-outline-primary btn-sm " data-id="'.$row->id_jam.'" data-nama="'.$row->nama_jam.'" data-masuk_start="'.$row->jam_masuk_start.'" data-masuk_end="'.$row->jam_masuk_end.'" data-keluar_start="'.$row->jam_pulang_start.'" data-keluar_end="'.$row->jam_pulang_end.'" title="Edit User"><i class="cil-pencil"></i></a>';                                               
                    
                }, 'last')   
                         
                ->hide('id_jam')                
                ->hide('jam_masuk_start')
                ->hide('jam_masuk_end')
                ->hide('jam_pulang_start')
                ->hide('jam_pulang_end')
                ->addNumbering() //it will return data output with numbering on first column                 
               ->toJson();
    }
    public function jam_add(){
        $session = session();        
        $validation =  \Config\Services::validation();
        $validation->setRules(  [                                                              
                                     'masuk_start' => 'required',
                                    'masuk_end' => 'required',
                                    'keluar_start' => 'required',
                                    'keluar_end' => 'required',
                                    'token' => 'required'
                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
        if($isDataValid){
            if($this->request->getPost('token')== $session->getTempdata('token')){
                $session->remove('token');
                
                    // GET WA NUMBER                    
                     
                        $value=array();
                        $value['id_jam']=$this->request->getPost('ids');
                        $value['nama_jam']=$this->request->getPost('nama');
                        $value['jam_masuk_start']=$this->request->getPost('masuk_start');
                        $value['jam_masuk_end']=$this->request->getPost('masuk_end');
                        $value['jam_pulang_start']=$this->request->getPost('keluar_start');
                        $value['jam_pulang_end']=$this->request->getPost('keluar_end');

                        $m = new Mjam();
                        $result = $m->save($value);
                        if($result){
                            if($this->request->getPost('ids') == '' || $this->request->getPost('ids') == '0'){
                                $id=$m->getInsertID();
                                //echo 'valid';
                                $session->setFlashdata('error', 'Data berhasil disimpan '.$id);         
                            }else{                         
                                $session->setFlashdata('error', 'Perubahan Data berhasil dilakukan');    
                            }                    
                        }else{
                            $session->setFlashdata('error', 'Update Data GAGAL');  
                        }
                    
                
            }else{
                $session->setFlashdata('error', 'Permintaan Token tidak sesuai, Silahkan ulangi');                
                return redirect()->to('/panel/setting_jam');
            }
        }
        else{
            $errors = $validation->getErrors();
            $session->setFlashdata('error', 'GAGAL. error:'.json_encode($errors));    
        }

        return redirect()->to('panel/setting_jam');
    }
    
    public function jam_delete(){
        if( $this->request->getPost('id') !== NULL){        
            $session = session();    
            $m=new Mjam();
            $r=false;
                $r=$m->delete($this->request->getPost('id'));           
            if($r){
                $session->setFlashdata('error', 'Jam berhasil dihapus');
            }   else{
                $session->setFlashdata('error', 'Jam GAGAL dihapus');
            }
        }

            

        return redirect()->to('panel/setting_jam');
    }
    public function device_absensi(){
    
        /* generate random token */
        $bytes = random_bytes(20);
        $token= bin2hex($bytes);
        $session = session();
        $session->set(['token'=>$token]);
        $session->markAsTempdata('token', 300);
        
        
        $data['token']=$token;
        $data['title']='Pengaturan Device Absensi';
        $s=new Msetting();
        $data['setting']=$s->first();
        return view('setting',$data);

        
   
    }
    public function device_save(){
        $session = session();        
        $validation =  \Config\Services::validation();
        $validation->setRules(  [ 
                                    
                                    'token' => 'required',
                                    'keterangan' => 'required'
                                    
                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
       
        if($isDataValid){
            if($this->request->getPost('token')== $session->getTempdata('token')){
                $session->remove('token');
                    $value=array();
                   
                    $result;
                    $dev = $this->request->getPost('dev'); 
                    $keterangan = $this->request->getPost('keterangan'); 
                    
                    foreach($keterangan as $key => $va){
                       
                        
                            $val =array();
                            $val['keterangan']=$va;
                            $val['id_device'] =$key;
                            if(!isset($dev[$key])){
                                $val['mode']=0;
                            }else{
                                $val['mode']=1;
                            }
                             
                            $d= new Mdevice();
                            $result=$d->save($val);

                            
                    }
                    if($result){                            
                            
                            $session->setFlashdata('error', 'Perubahan Data berhasil dilakukan');    
                                           
                    }else{
                        $session->setFlashdata('error', 'Update Data GAGAL');  
                    }
                
            }else{
                $session->setFlashdata('error', 'Permintaan Token tidak sesuai, Silahkan ulangi');                
                return redirect()->to('/panel/devicenfc');
            }
        }
        else{
            $errors = $validation->getErrors();
            $session->setFlashdata('error', 'GAGAL. error:'.json_encode($errors));    
        }
        return redirect()->to('panel/devicenfc');
    }

function notifikasi_pendaftaran($no,$nama){
        $set = new Msetting();
        $setting = $set->first();
$text='*Sistem Absensi '.$setting['nama_perusahaan'].'*

nomor ini telah didaftarkan sebagai notifikasi absensi atas nama *'.$nama.'* 

_nb: jika ada kesalahan, segera hubungi kami_'; 

        $wa = new Api_wa();
        $r=$wa->send_wa($no,$text);
}

    /* WARNING DEVELOPER ONLY!! ADDING CALENDAR FOR PIVOTING */
    public function generate_calendar($year){

        $db = db_connect();
        $query = $db->query("select count(*) as jumlah from kalender where YEAR(tanggal) = ".$year);
        $r = $query->getResult();
        if($r[0]->jumlah == 0){
            // GENERATE DATE AND INSERT
            $begin = new \DateTime( $year.'-01-01' );
            $end = new \DateTime( $year.'-12-31 +1day'  );        
            $interval = new \DateInterval('P1D');
            $daterange = new \DatePeriod($begin, $interval ,$end);        
            $t=array();
            $tanggal=array();
            foreach($daterange as $date){
                $tanggal[]=array('tanggal'=>$date->format("Y-m-d"));

            }      

            $db      = db_connect();
            $builder = $db->table('kalender');
            $result = $builder->insertBatch($tanggal);
            //$result = $builder->affectedRows();
            echo $result.' untuk tahun '.$year.' data telah ditambahkan';

        }else{
            echo 'TAHUN SUDAH DITAMBAHKAN';
        }
         


    }

    public function notifikasi(){
    
        /* generate random token */
        $bytes = random_bytes(20);
        $token= bin2hex($bytes);
        $session = session();
        $session->set(['token'=>$token]);
        $session->markAsTempdata('token', 300);
        
        
        $data['token']=$token;
        $data['title']='Pengaturan Aplikasi';
        $s=new Mnotifikasi();
         
        $data['notifikasi']=$s->findAll();

         
        return view('setting_notifikasi',$data);

        
   
    }
    public function notifikasi_save(){
    
        $session = session();        
        $validation =  \Config\Services::validation();
        $validation->setRules(  [ 
                                    'text' => 'required',                                     
                                    'token' => 'required'                                    
                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
       
        if($isDataValid){
            if($this->request->getPost('token')== $session->getTempdata('token')){
                $session->remove('token');
                $text = $this->request->getPost('text');
                $status = $this->request->getPost('status_text');
                $vall = array();
                $m = new Mnotifikasi();

                foreach ($text as $key => $value) {
                    if(!empty($status[$key]))
                        echo 'key '.$key.', text '.$value.', status 1 <br>';
                    else
                        echo 'key '.$key.', text '.$value.', status 0 <br>';
                    $data = array();
                    if(!empty($status[$key]))
                        $data=array('id_no'=>$key,'text'=>$value,'status'=>1);
                    else 
                        $data=array('id_no'=>$key,'text'=>$value,'status'=>0);
                        $result = $m->save($data);
                    
                }
                 
                
                    
                    
                    if($result){                            
                            
                            $session->setFlashdata('error', 'Perubahan Data berhasil dilakukan');    
                                           
                    }else{
                        $session->setFlashdata('error', 'Update Data GAGAL');  
                    }
                
            }else{
                $session->setFlashdata('error', 'Permintaan Token tidak sesuai, Silahkan ulangi');                
                return redirect()->to('/panel/notifikasi');
            }
        }
        else{
            $errors = $validation->getErrors();
            $session->setFlashdata('error', 'GAGAL. error:'.json_encode($errors));    
        }
        return redirect()->to('panel/notifikasi');

        
   
    }
    public function percobaan(){
        $request = $this->request->getPost();
        $laporan =array(2,4,5,6,7);
        $result =array();
        if(in_array($request['id'],$laporan)){
            $s =new Msetting();
            $setting = $s->first();
            $arrdata= array('instansi'=>$setting['nama_perusahaan'],
                            'nama' =>'Mas Arwin',
                            'telp_instansi'=>'0',
                            'grup'=>'X',
                            'nfc'=>'20019283');
            $status = $this->m_pesan($request['id'],$arrdata);
           
            if($status['status'] == 1){
                $wa = new Api_wa();
                $r=$wa->send_wa($request['no'],$status['pesan']);
                
                $rx = json_decode($r);
                
                if($rx->status=='true'){
                    $result['status']=1;
                    $result['pesan']='Pesan Terkirim';        
                }else{
                    $result['status']=0;
                    $result['pesan']='Pesan Tidak Dapat Terkirim Cek API WA';        
                }
            }else{
                    $result['status']=0;
                    $result['pesan']=$rx->status;   
            }             
        }else if(in_array($request['id'],array(1,3) )){
            $result['status']=0;
            $result['pesan']='fitur belum tersedia';
            
        }else{
            $result['status']=0;
            $result['pesan']='Pesan tidak ditemukan';
        }
        echo json_encode($result);
    }

    public function m_pesan($idpesan,$arrdata){

        $result = array();
            if($idpesan > 0 && $idpesan < 8){
                $n = new Mnotifikasi();
                $m = $n->find($idpesan);

                if($m!=''){
                    $pesan = $m['text'];
                    
                   
                    if(!empty($arrdata['instansi'])){
                        $pesan=str_replace('[INSTANSI]',$arrdata['instansi'],$pesan);       
                                         
                    }
                    
                    if(!empty($arrdata['telp_instansi'])){
                        $pesan=str_replace('[TELP_INSTANSI]',$arrdata['telp_instansi'],$pesan);                        
                    }
                    if(!empty($arrdata['nama'])){
                        $pesan=str_replace('[NAMA]',$arrdata['nama'],$pesan);                        
                    }
                    if(!empty($arrdata['grup'])){
                        $pesan=str_replace('[grup]',$arrdata['grup'],$pesan);                        
                    }
                    if(!empty($arrdata['nfc'])){
                        $pesan=str_replace('[nfc]',$arrdata['nfc'],$pesan);                        
                    }
                    
                    $pesan=str_replace('[DATE]',date('d-m-Y'),$pesan);                        
                    
                    $result['status']=1;
                    $result['pesan']=$pesan;                     
                    $result['enable']=$m['status'];                     

                }else{
                    $result['status']=0;
                    $result['pesan']='Pesan Tidak Ditemukan';    
                    $result['enable']=0;                     
                }
            }else{
                $result['status']=0;
                $result['pesan']='id pesan tidak sesuai';
                $result['enable']=0;

            }

            return $result;
    }

    /* USER ADMIN */
    public function user_admin(){

        
        $data['title']= 'Daftar User Admin';
        return view('data_users',$data);
    }

    public function user_admin_ajax(){
        $db = db_connect();
        $builder = $db->table('users')
                        ->select('uid,nama,username,email,password,akses')
                        ->where('users.deleted_at IS NULL');
         
        return DataTable::of($builder)   
                ->add('action', function($row){
                    $text='';
                    
                    if(session()->get('akses') == '1' || $row->uid == session()->get('uid'))
                    $text = '<a href="javascript:void(0);" class="edit_record btn btn-transparent btn-sm text-primary" data-id="'.$row->uid.'" data-nama="'.$row->nama.'" data-username="'.$row->username.'" data-email="'.$row->email.'" data-akses="'.$row->akses.'"   title="Edit User"><i class="cil-pencil"></i></a>';

                    if($row->uid == 1 || $row->uid == session()->get('uid') ){

                    }else
                     {
                         if(session()->get('akses') == 1)
                            $text .='<a href="javascript:void(0);" class="delete_record btn btn-transparent btn-sm text-danger" data-id="'.$row->uid.'" title="Hapus User"><i class="cil-trash"></i></a>';
                     }   
                    if(session()->get('akses') == '1' || $row->uid == session()->get('uid'))											   
                        $text .='<button class="btn btn-transparent btn-sm text-info" title="Lihat Password" onclick="alert(\''.$row->password.'\')"><i class="cil-low-vision"></i></button>';
                    return $text;
                }, 'last')
                 
                ->edit('akses', function($row){
                    if($row->akses==1)
                        return 'Admin';                    
                    else
                        return 'Operator';
                })
               ->hide('uid')
               ->hide('password')
               ->addNumbering() //it will return data output with numbering on first column
               ->toJson();
    }

    public function tes(){
        print_r(session()->get());
        print_r(session()->get('akses'));
    }

    public function admin_username_check(){
        if( $this->request->getPost('username') !== NULL){
			$con=array();
            $con['username']=$this->request->getPost('username');
            $u=new Musers();
            $user = $u->where($con)->findAll();
			
			if(count($user)>0){
				echo '0';
			}else{
				echo '1';
			}
		}else{
			echo '0';
		}
    }

    public function admin_user_add(){

        $session = session();
        $validation =  \Config\Services::validation();
        $validation->setRules(  [                                                              
                                'nama' => 'required',
                                'username' => 'required',                                
                                'akses' => 'numeric'
                                 ]
                                );
        
                              //  print_r($this->request);
        $isDataValid = $validation->withRequest($this->request)->run();
        if($isDataValid){
            $con=array();
            $con['uid']=$this->request->getPost('id');
            $con['nama']=$this->request->getPost('nama');
            $con['username']=$this->request->getPost('username');
            if($this->request->getPost('id') == '')
                $con['password']=$this->request->getPost('password');
            else if($this->request->getPost('password')!='')
                $con['password']=$this->request->getPost('password');
            $con['email']=$this->request->getPost('email');
            $con['akses']=$this->request->getPost('akses');
             
            $u= new Musers();
            $result= $u->save($con);
                if($result){
                    if($this->request->getPost('id') == ''){
                        $id=$u->getInsertID();
                        //echo 'valid';
                        $session->setFlashdata('error', 'Data berhasil disimpan');         
                    }else{                         
                         $session->setFlashdata('error', 'Perubahan Data berhasil dilakukan');    
                    }                    
                }else{
                    $session->setFlashdata('error', 'Update Data GAGAL');  
                }
        }
        else{
            $errors = $validation->getErrors();
            $session->setFlashdata('error', 'Update Admin GAGAL. error:'.json_encode($errors));    
        }

        return redirect()->to('panel/users');
    }
    public function admin_user_delete(){
        if( $this->request->getPost('id') !== NULL){		
            $session = session();	 
            $m=new Musers();
			$r=$m->delete($this->request->getPost('id'));			
			if($r){
                $session->setFlashdata('pesan', 'user berhasil dihapus');
			}	else{
                $session->setFlashdata('pesan', 'user GAGAL dihapus');
			}
		}

			
        
		return redirect()->to('panel/users');
		 
    }


    public function riwayatwa()
	{
        $grup= new Mgrup();
        $data['grup']= $grup->findAll();
        /* generate random token */
        $bytes = random_bytes(20);
        $token= bin2hex($bytes);
        $session = session();
        $session->set(['token'=>$token]);
        $session->markAsTempdata('token', 300);
        
        $data['token']=$token;
		$data['title']='Riwayat Pesan WA';
		return view('data_log_wa',$data);
    }
    public function riwayatwa_ajax()
	{
          
        $db = db_connect();
        $builder = $db->table('log_wa')
                        ->select('id,no,text,status,keterangan,created_at');
                         
                         

         
        return DataTable::of($builder)                                       
                ->hide('id')                
                ->addNumbering() //it will return data output with numbering on first column                
               ->toJson();
    }

}