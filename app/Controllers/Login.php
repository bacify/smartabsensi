<?php

namespace App\Controllers;
use \Hermawan\DataTables\DataTable;
use App\Models\Musers; 
 

class Login extends BaseController
{
	public function index()
    {
        if(session()->get('logged_in')){          
            return redirect()->to('/panel/home/');
            
        }
        helper(['form']);
        echo view('login');
    }
    public function te()
    {
        
        helper(['form']);
        echo view('test');
    } 
    public function auth()
    {
        
        $session = session();
       
        $username = $this->request->getVar('username');
        $password = $this->request->getVar('password');
        $m = new Musers();
        $d= $m->where('username',$username)->first();
         
        
        
        if($d){
            $pass = $d['password'];
            $verify_pass = ($password === $pass);
            if($verify_pass){
                $ses_data = [
                    'uid' => $d['uid'],                     
                    'nama'     => $d['nama'],                    
                    'email'     => $d['email'],                    
                    'akses'     => $d['akses'],                    
                    'logged_in'     => TRUE
                ];
                $session->set($ses_data);
                return redirect()->to('/panel/home/');
            }else{
                $session->setFlashdata('error', 'Wrong Password');                
                return redirect()->to('/login');
            }
        }else{
            
            $session->setFlashdata('error', 'Username not Found');
            
            return redirect()->to('/login');
        }               

    }


    public function logout()
    {
        $session = session();
        $session->destroy();
        return redirect()->to('/login');
    }

    public function display()
    {
        $data['title'] = 'Smart Absensi';
        echo view('display_panel',$data);
    }

    public function pulldisplay(){
		session_write_close();
		ignore_user_abort(true);
		// set php runtime to unlimited
		set_time_limit(0);

				/**
		 * Server-side file.
		 * This file is an infinitive loop. Seriously.
		 * It gets the file data.txt's last-changed timestamp, checks if this is larger than the timestamp of the
		 * AJAX-submitted timestamp (time of last ajax request), and if so, it sends back a JSON with the data from
		 * data.txt (and a timestamp). If not, it waits for one seconds and then start the next while step.
		 *
		 * Note: This returns a JSON, containing the content of data.txt and the timestamp of the last data.txt change.
		 * This timestamp is used by the client's JavaScript for the next request, so THIS server-side script here only
		 * serves new content after the last file change. Sounds weird, but try it out, you'll get into it really fast!
		 */

		

		// where does the data come from ? In real world this would be a SQL query or something
		
		 $result=array();
		// main loop
		while (true) {


		    // if ajax request has send a timestamp, then $last_ajax_call = timestamp, else $last_ajax_call = null
		    $last_ajax_call = isset($_GET['timestamp']) ? (int)$_GET['timestamp'] : null;

		    // PHP caches file data, like requesting the size of a file, by default. clearstatcache() clears that cache
		    clearstatcache();
		    // get timestamp of when file has been changed the last time
		    $queue=null;
		    
            $db      = \Config\Database::connect();
            $builder = $db->table('display'); 
            $query = $builder->get();
            $display=array();
            foreach ($query->getResult() as $row) {
                $display['id_display']= $row->id_display;
                $display['tipe']= $row->tipe;
                $display['nama']= $row->nama;
                $display['nomorkartu']= $row->nomorkartu;
                $display['updated_at']= $row->updated_at;
            }

		    
		    $last_change_in_data_file=strtotime($display['updated_at']);     
		    
		    


		    // if no timestamp delivered via ajax or data.txt has been changed SINCE last ajax timestamp
		    if ($last_ajax_call == null || $last_change_in_data_file > $last_ajax_call) {

		        

		        // put data.txt's content and timestamp of last data.txt change into array
		         

		        // encode to JSON, render the result (for AJAX)
                $display['timestamp']=$last_change_in_data_file;
		        $json = json_encode($display);
		        echo $json;

		        // leave this loop step
		        break;

		    } else {
		        // wait for 1 sec (not very sexy as this blocks the PHP/Apache process, but that's how it goes)
		        sleep( 2 );
		        continue;
		    }
		}
	}

}