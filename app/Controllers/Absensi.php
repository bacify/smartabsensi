<?php

namespace App\Controllers;
use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use App\Models\Mabsen; 
use App\Models\Mmember; 
use App\Models\Msetting; 
use App\Models\Mjam; 
use App\Models\Mdevice; 
use App\Models\Mstorage; 
use CodeIgniter\Files\File;

class Absensi extends ResourceController
{
    public function index()
    {
        echo 'Ini Adalah halaman API';
    }

    public function addnfc()
	{
		$rules = [
			"idkartu" => "required",
			"device" => "required"			
		];

		$messages = [
			"idkartu" => [
				"required" => "No Kartu Diperlukan"
			],
			"email" => [
				"required" => "Device id Diperlukan"				
			],
		];

		if (!$this->validate($rules, $messages)) {

			$response = [
				'status' => 500,
				'error' => true,
				'message' => $this->validator->getErrors(),
				'data' => "",
				'text'=>$this->validator->getErrors()
			];
		} else {

			
			
			$data['idkartu'] = $this->request->getPost('idkartu');
			$data['device'] = $this->request->getVar("device");
			
			$dev = new Mdevice;
			$devicelist = $dev->where('id_device',$data['device'])->first();

			
			if($devicelist['mode']==0)
			{
				$m = new Mmember();
				$member = $m->where('nfc_id',$data['idkartu'])->first();
				/*IMAGE UPLOAD CODE*/
				

				// Check if image file is a actual image or fake image
				 


		       
		       // print_r($img);
		       
		       

		        // END OF IMAGE UPLOAD

				if($member!=null){

					//get ID ABSEN IF EXIST
					$ma = new Mabsen();
					$currabsen = $ma->where(array('kehadiran'=> date('Y-m-d'),
									'member_id'=>$member['id_member'])) -> first();
					$currtime = date('H:i:s');
					$set = new Msetting();
					$setting = $set->first();
					
					// $in_start = $setting['jam_masuk_start'];
					// $in_end = $setting['jam_masuk_end'];
					// $out_start = $setting['jam_pulang_start'];
					// $out_end = $setting['jam_pulang_end'];

					$j = new Mjam();
					$jam = $j->where('id_jam',$member['jam_id'])->first();
					
					$in_start = $jam['jam_masuk_start'];
					$in_end = $jam['jam_masuk_end'];
					$out_start = $jam['jam_pulang_start'];
					$out_end = $jam['jam_pulang_end'];

					
					$dd['member_id'] = $member['id_member'];
					$dd['device_id'] = $data['device'];
					$dd['created_by'] = 'SYS';
					if($currabsen!=null)
						$dd['id_absensi'] = $currabsen['id_absensi'];
					$status = 0;
					$text= '';
					
					
					// JIKA LIBUR
					$s = new Msetting();
					$setting = $s->first();
					
					$libur = $setting['hari_libur'];
					$hari = explode(',',$libur);

					$listday=array();
					if($hari[0] == 1)
						$listday[]=1;
					if($hari[1] == 1)
						$listday[]=2;
					if($hari[2] == 1)
						$listday[]=3;
					if($hari[3] == 1)
						$listday[]=4;
					if($hari[4] == 1)
						$listday[]=5;
					if($hari[5] == 1)
						$listday[]=6;
					if($hari[6] == 1)
						$listday[]=0;
					
					$db      = \Config\Database::connect();
					$builder = $db->table('tanggal_libur')
									->select('id_tanggal,tanggal' ); 
									$query = $builder->get();
					$tanggal_libur=array();
					foreach ($query->getResult() as $row) {
						$tanggal_libur []= $row->tanggal;
					}

					 
					
					// PENGECEKAN LIBUR
					if(in_array(date('w'), $listday) || in_array(date('Y-m-d'),$tanggal_libur)){

						//LIBUR
						$builder = $db->table('display');
								$builder->set('tipe',0);
								$builder->set('updated_at','NOW()', FALSE);
								$builder->update();
						$status = 1;
						$text='HARI';
						$obj='LIBUR';
					}else{

						$currtime = strtotime($currtime);
						 
						if($currtime >= strtotime($in_start) && $currtime <= strtotime($in_end)){
							if($currabsen!=null&&$currabsen['masuk'] !=null){
								// Sudah ABSEN
								$status=1;
								$text='Sudah';
								$obj='Absen';
								$builder = $db->table('display');
								$builder->set('tipe',3);
								$builder->set('updated_at','NOW()', FALSE);
								$builder->update();
							}else{
								$dd['masuk'] = date('H:i:s');
								$dd['device_masuk'] = $data['device'];
								$img = $this->request->getFile('imageFile');
			        
						         //echo $img->getName();
						        if (! $img->hasMoved()) {		            
						            $newName = date('Ymd_His_').$img->getName();
				            		$img->move('uploads/', $newName);
				            		$dd['gambar_masuk'] = $newName;
						         //   echo 'upload dilakukan';		           
						        }  
								$text='Selamat Datang';
								$obj=$member['nama'];
								$db      = \Config\Database::connect();								
								$builder = $db->table('display');
								$builder->set('tipe',1);
								$builder->set('nama',$member['nama']);
								$builder->set('nomorkartu',$data['idkartu']);
								$builder->set('updated_at','NOW()', FALSE);
								$builder->update();
							}
						}else if($currtime >= strtotime($out_start) && $currtime <= strtotime($out_end)){
							if($currabsen!=null&&$currabsen['keluar'] !=null){
								$status=1;
								$text='Sudah';
								$obj='Absen';
								$builder = $db->table('display');
								$builder->set('tipe',3);
								$builder->set('updated_at','NOW()', FALSE);
								$builder->update();
							}else{
								$dd['keluar'] = date('H:i:s');
								$dd['device_keluar'] = $data['device'];
								$img = $this->request->getFile('imageFile');
			        
						         //echo $img->getName();
						        if (! $img->hasMoved()) {		            
						            $newName = date('Ymd_His_').$img->getName();
				            		$img->move('uploads/', $newName);
				            		$dd['gambar_keluar'] = $newName;
						         //   echo 'upload dilakukan';		           
						        }  
								$text='Selamat Tinggal';
								$obj=$member['nama'];
								$db      = \Config\Database::connect();								
								$builder = $db->table('display');
								$builder->set('tipe',2);
								$builder->set('nama',$member['nama']);
								$builder->set('nomorkartu',$member['nama']);
								$builder->set('updated_at','NOW()', FALSE);
								$builder->update();
							}
							
						}else{
							$status = 1;
							$text='Belum';
							$obj='Waktunya';
							$db      = \Config\Database::connect();
								$builder = $db->table('display');
								$builder->set('tipe',0);
								$builder->set('updated_at','NOW()', FALSE);
								$builder->update();
						}
					}
					if($status == 1){
						$response = [
							'status' => 400,
							'error' => true,
							'message' => 'Error',
							'data' => $obj,
							'text' => $text
						];		
					}else{
						//print_r($dd);
						$ma->save($dd);
						$response = [
							'status' => 200,
							'error' => false,
							'data' =>$obj,
							'message' => 'OK',
							'text' => $text
						];
					}

				}else{
					$text='Kartu';
					$obj ='Tak Dikenal!';
					$response = [
						'status' => 400,
						'error' => true,
						'message' => 'Error',
						'data' => $obj,
						'text' => $text
					];
					$db      = \Config\Database::connect();
								$builder = $db->table('display');
								$builder->set('tipe',4);
								$builder->set('nomorkartu',$data['idkartu']);
								$builder->set('updated_at','NOW()', FALSE);
								$builder->update();
				}
			}else{
				$msto = new Mstorage();
				$storage = $msto->where('nfc_id',$data['idkartu'])->findAll();

				$mme = new Mmember();
				$mem = $mme->where('nfc_id',$data['idkartu'])->findAll();
				if(count($storage)> 0 || count($mem)>0){
					$text='Sudah';
					$obj='Terdaftar!';
					$response = [
						'status' => 400,
						'error' => true,
						'message' => 'Error',
						'data' => $obj,
						'text' => $text
					];
				}else{
					$msto->save(array('nfc_id'=>$data['idkartu'],
									'device'=>$data['device']));
					$text='Berhasil';
					$obj = 'Didaftarkan!';
					$response = [
						'status' => 200,
						'error' => false,
						'message' => 'Berhasil',
						'data' => $obj,
						'text' => $text
					];
				}

				
			}
			
			 
			
		}

		if($response['error']== true)
			return $this->respond($response,400);
		else
			return $this->respond($response,200);
	}
}
