<?php

namespace App\Controllers;
use App\Models\Mabsen; 
use App\Models\Mkelas; 
use App\Models\Msetting; 
use App\Models\Msiswa; 
use App\Models\musers; 
use App\Models\Mnotifikasi; 
use App\Models\Mlogwa; 
use \Hermawan\DataTables\DataTable;
use App\Libraries\Api_wa; // Import library

class Notifikasi extends BaseController
{
	public function index()
	{          
		echo ' Ini Adalah Halaman Notifikasi';		
    }

    public function notif_harian_email(){

        if(date('w')==0)
        die();

        $s = new Msetting();
        $setting = $s->first();
        
        $libur = $setting['hari_libur'];
        $hari = explode(',',$libur);

        $listday=array();
        if($hari[0] == 1)
            $listday[]=1;
        if($hari[1] == 1)
            $listday[]=2;
        if($hari[2] == 1)
            $listday[]=3;
        if($hari[3] == 1)
            $listday[]=4;
        if($hari[4] == 1)
            $listday[]=5;
        if($hari[5] == 1)
            $listday[]=6;
        if($hari[6] == 1)
            $listday[]=0;
        
        $db      = \Config\Database::connect();
        $builder = $db->table('tanggal_libur')
                        ->select('id_tanggal,tanggal' ); 
                        $query = $builder->get();
        $tanggal_libur=array();
        foreach ($query->getResult() as $row) {
            $tanggal_libur []= $row->tanggal;
        }
        
        if( !($setting['email_notif'] == 1 && $setting['email_jam']== (int)date('G') && !in_array(date('w'), $listday) && !in_array(date('Y-m-d'),$tanggal_libur) )){
            echo 'Belum Waktunya';
            die();
        }

        
        $email_to =$setting['email'] ;
        $email_from ='absensi@seranggalangit.xyz';
        $email_user ='absensi@seranggalangit.xyz';
        $email_host ='srv91.niagahoster.com';
        $email_pass ='Mojosari0k';
        $email_port ='465';



        // GET SISWA ALPHA HARIAN
        $db = db_connect();
        $builder = $db->table('siswa_kalender')
                        ->select('id_absensi,nama,nis,nama_kelas,COALESCE(kehadiran,tanggal) as kh,COALESCE(keterangan,"A") as absen' )
                        ->join('absensi_harian','absensi_harian.siswa_id = siswa_kalender.id_siswa AND date(absensi_harian.kehadiran) = date(siswa_kalender.tanggal)','LEFT')                                                 
                        ->join('kelas','kelas.id_kelas = siswa_kalender.kelas ')                                                                        
                        ->where('date(tanggal)',date('Y-m-d'))                        
                        ->where('siswa_kalender.deleted_at IS NULL')                       
                        ->where('COALESCE(keterangan,"A")','A')                        
                        ->where('absensi_harian.deleted_at IS NULL');

        $result = $builder->get()->getResult();

       // print_r($result);

        $text ='List Siswa Alpha '.date('d-m-Y').': <br>';
        $x=1;
        foreach ($result as $r) {
            $text.= $x++.'. '.$r->nama.' - '.$r->nis.' - '.$r->nama_kelas.' <br>';
        }


        $email = \Config\Services::email();

        $config['protocol'] = 'smtp';
        $config['SMTPHost'] = $email_host;
        $config['SMTPUser'] = $email_user;
        $config['SMTPPass'] = $email_pass;
        $config['SMTPPort'] = $email_port;
        $config['SMTPCrypto'] = 'ssl';
        $config['mailType'] = 'html';
        $config['charset']  = 'utf-8';
        $config['wordWrap'] = false;

        $email->initialize($config);


        $email->setFrom($email_from, 'Notifikasi Absensi');
        $email->setTo($email_to);
        // $email->setCC('another@another-example.com');
        // $email->setBCC('them@their-example.com');
        $email->setSubject('Daftar Siswa Alpha '.date('d-m-Y'));
        $email->setMessage($text);
         
        if ($email->send(false))
        {
               echo 'OK'; // Parameters won't be cleared
        }
       else{
               $r= $email->printDebugger(['headers']);
               print_r($r);
       } 
        // Will only print the email headers, excluding the message subject and body
        


    }
    public function notif_harian_wa(){

        $set = new Msetting();
        $setting = $set->first();
        $libur = $setting['hari_libur'];
        $hari = explode(',',$libur);

        $listday=array();
        if($hari[0] == 1)
            $listday[]=1;
        if($hari[1] == 1)
            $listday[]=2;
        if($hari[2] == 1)
            $listday[]=3;
        if($hari[3] == 1)
            $listday[]=4;
        if($hari[4] == 1)
            $listday[]=5;
        if($hari[5] == 1)
            $listday[]=6;
        if($hari[6] == 1)
            $listday[]=0;
        
        $db      = \Config\Database::connect();
        $builder = $db->table('tanggal_libur')
                        ->select('id_tanggal,tanggal' ); 
                        $query = $builder->get();
        $tanggal_libur=array();
        foreach ($query->getResult() as $row) {
            $tanggal_libur []= $row->tanggal;
        }
       
        if(in_array(date('w'), $listday)){
            echo 'Hari Libur';
            die();
        }

        if(in_array(date('Y-m-d'),$tanggal_libur)){
            echo 'Tanggal Libur';
            die();
        }

        $wa = new Api_wa();
        $r=$wa->device();
        $device=json_decode($r);
        if($device->status != 'true'){
            echo 'wa belum terhubung';
            die();
        }

        if(date('w')==0){
            echo 'hari minggu, belum waktunya';
            die();
        }
       
        
      
      

        if( !($setting['wa_notif'] == 1 && $setting['wa_jam']== (int)date('G') && $setting['wa_status']== 1 ))
        {
            echo 'Belum Waktunya';
            die();
        }

        $log= new Mlogwa(); 

        //  get status WA SYSTEM ALPHA
        $notifikasi = new Mnotifikasi();
        $n = $notifikasi->find(5); // 1 = email daftar, 2 = wa daftar, 3= email absen, 4 = wa masuk, 5 = wa alpha, 6 = wa ijin, 7 = wa sakit
        if($n['status'] == 1){

         // GET SISWA ALPHA HARIAN
         $db = db_connect();
         $builder = $db->table('member_kalender')
                         ->select('id_absensi,no_telp,nfc_id,COALESCE(kehadiran,tanggal) as kh,nama,nama_grup,COALESCE(masuk,"-") as masuk,COALESCE(keluar,"-") as keluar,COALESCE(IF(masuk IS NOT NULL AND keluar is NOT NULL,"OK",if((masuk is not null and keluar is null) or (keluar is not null and masuk is null),1,null)),keterangan,"A") as absen,id_member'  )
                         ->join('absensi','absensi.member_id = member_kalender.member_id AND absensi.kehadiran = member_kalender.tanggal','LEFT')                                                 
                        ->join('member','member.id_member = member_kalender.member_id ')  
                        ->join('grup','grup.id_grup = member.grup_id ')                                                                                         
                         ->where('date(tanggal)',date('Y-m-d'))                        
                         ->where('member.deleted_at IS NULL')                       
                         ->where('absen','A');
 
         $result = $builder->get()->getResult(); 

            // echo 'A <br>';
            // Kirim Notifikasi
            foreach ($result as $r) {
                if($r->notifikasi== 0)
                continue;
                $arrdata= array('instansi'=>$setting['nama_perusahaan'],
                'nama' =>$r->nama,
                'telp_instansi'=>'0',
                'kelas'=>$r->nama_grup,
                'nokartu'=>$r->nfc_id);
                $text= $this->filternotif($n['text'],$arrdata);
                $wa = new Api_wa();
               $st=$wa->send_wa($r->no_telp,$text);

               
               $hr = json_decode($st);
               
               if($hr->status== 'true'){
                   $xx= array('no'=>$r->no_telp,'text'=>$hr->pesan,'status'=>1,'keterangan'=>'A');
                   $log->save($xx);
               }else{
                    $xx= array('no'=>$r->no_telp,'text'=> $hr->pesan,'status'=>0,'keterangan'=>'A');
                    $log->save($xx);
               }
            //    delay 1second
               sleep(1);
            //    echo $r->no_telp.' : '.$text.'<br>';
            }
        }


        //  get status WA SYSTEM Masuk
        $notifikasi = new Mnotifikasi();
        $n4 = $notifikasi->find(4); // 1 = email daftar, 2 = wa daftar, 3= email absen, 4 = wa masuk, 5 = wa alpha, 6 = wa ijin, 7 = wa sakit
        if($n4['status'] == 1){

         // GET SISWA ALPHA HARIAN
         $db = db_connect();
         $builder = $db->table('member_kalender')
                         ->select('id_absensi,no_telp,nfc_id,COALESCE(kehadiran,tanggal) as kh,nama,nama_grup,COALESCE(masuk,"-") as masuk,COALESCE(keluar,"-") as keluar,COALESCE(IF(masuk IS NOT NULL AND keluar is NOT NULL,"OK",if((masuk is not null and keluar is null) or (keluar is not null and masuk is null),1,null)),keterangan,"A") as absen,id_member'  )
                         ->join('absensi','absensi.member_id = member_kalender.member_id AND absensi.kehadiran = member_kalender.tanggal','LEFT')                                                 
                        ->join('member','member.id_member = member_kalender.member_id ')  
                        ->join('grup','grup.id_grup = member.grup_id ')                                                                                         
                         ->where('date(tanggal)',date('Y-m-d'))                        
                         ->where('member.deleted_at IS NULL')                       
                         ->where('absen','2');
 
         $result = $builder->get()->getResult(); 

        //  echo 'M <br>';
            // Kirim Notifikasi
            foreach ($result as $r) {
                if($r->notifikasi== 0)
                continue;
                $arrdata= array('instansi'=>$setting['nama_perusahaan'],
                'nama' =>$r->nama,
                'telp_instansi'=>'0',
                'kelas'=>$r->nama_grup,
                'nokartu'=>$r->nfc_id);
                $text= $this->filternotif($n4['text'],$arrdata);
                $wa = new Api_wa();
                $st=$wa->send_wa($r->no_telp,$text);
                $hr = json_decode($st);
               
               if($hr->status== 'true'){
                    $xx= array('no'=>$r->no_telp,'text'=> $hr->pesan,'status'=>1,'keterangan'=>'M');
                    $log->save($xx);
                }else{
                     $xx= array('no'=>$r->no_telp,'text'=> $hr->pesan,'status'=>0,'keterangan'=>'M');
                     $log->save($xx);
                }
                //    delay 1second
               sleep(1);
            
                // echo $r->no_telp.' : '.$text.'<br>';
            }
        }


        //  get status WA SYSTEM IJIN
        $notifikasi = new Mnotifikasi();
        $n5 = $notifikasi->find(6); // 1 = email daftar, 2 = wa daftar, 3= email absen, 4 = wa masuk, 5 = wa alpha, 6 = wa ijin, 7 = wa sakit
        if($n5['status'] == 1){

         // GET SISWA ALPHA HARIAN
         $db = db_connect();
         $builder = $db->table('member_kalender')
                         ->select('id_absensi,no_telp,nfc_id,COALESCE(kehadiran,tanggal) as kh,nama,nama_grup,COALESCE(masuk,"-") as masuk,COALESCE(keluar,"-") as keluar,COALESCE(IF(masuk IS NOT NULL AND keluar is NOT NULL,"OK",if((masuk is not null and keluar is null) or (keluar is not null and masuk is null),1,null)),keterangan,"A") as absen,id_member'  )
                         ->join('absensi','absensi.member_id = member_kalender.member_id AND absensi.kehadiran = member_kalender.tanggal','LEFT')                                                 
                        ->join('member','member.id_member = member_kalender.member_id ')  
                        ->join('grup','grup.id_grup = member.grup_id ')                                                                                         
                         ->where('date(tanggal)',date('Y-m-d'))                        
                         ->where('member.deleted_at IS NULL')                       
                         ->where('absen','I');
 
         $result = $builder->get()->getResult(); 

        //  echo 'I <br>';
            // Kirim Notifikasi
            foreach ($result as $r) {
                if($r->notifikasi== 0)
                continue;
                $arrdata= array('instansi'=>$setting['nama_perusahaan'],
                'nama' =>$r->nama,
                'telp_instansi'=>'0',
                'kelas'=>$r->nama_grup,
                'nokartu'=>$r->nfc_id);
                $text= $this->filternotif($n5['text'],$arrdata);
                $wa = new Api_wa();
                $st=$wa->send_wa($r->no_telp,$text);

                $hr = json_decode($st);
               
               if($hr->status== 'true'){
                    $xx= array('no'=>$r->no_telp,'text'=> $hr->pesan,'status'=>1,'keterangan'=>'I');
                    $log->save($xx);
                }else{
                     $xx= array('no'=>$r->no_telp,'text'=> $hr->pesan,'status'=>0,'keterangan'=>'I');
                     $log->save($xx);
                }
                //    delay 1second
               sleep(1);
            
                // echo $r->no_telp.' : '.$text.'<br>';
            }
        }

        //  get status WA SYSTEM Sakit
        $notifikasi = new Mnotifikasi();
        $n7 = $notifikasi->find(7); // 1 = email daftar, 2 = wa daftar, 3= email absen, 4 = wa masuk, 5 = wa alpha, 6 = wa ijin, 7 = wa sakit
        if($n7['status'] == 1){

         // GET SISWA ALPHA HARIAN
         $db = db_connect();
         $builder = $db->table('member_kalender')
                         ->select('id_absensi,no_telp,nfc_id,COALESCE(kehadiran,tanggal) as kh,nama,nama_grup,COALESCE(masuk,"-") as masuk,COALESCE(keluar,"-") as keluar,COALESCE(IF(masuk IS NOT NULL AND keluar is NOT NULL,"OK",if((masuk is not null and keluar is null) or (keluar is not null and masuk is null),1,null)),keterangan,"A") as absen,id_member'  )
                         ->join('absensi','absensi.member_id = member_kalender.member_id AND absensi.kehadiran = member_kalender.tanggal','LEFT')                                                 
                        ->join('member','member.id_member = member_kalender.member_id ')  
                        ->join('grup','grup.id_grup = member.grup_id ')                                                                                         
                         ->where('date(tanggal)',date('Y-m-d'))                        
                         ->where('member.deleted_at IS NULL')                       
                         ->where('absen','S');
 
         $result = $builder->get()->getResult(); 

        //  echo 'S <br>';
            // Kirim Notifikasi
            foreach ($result as $r) {
                if($r->notifikasi== 0)
                continue;
                $arrdata= array('instansi'=>$setting['nama_perusahaan'],
                'nama' =>$r->nama,
                'telp_instansi'=>'0',
                'kelas'=>$r->nama_grup,
                'nokartu'=>$r->nfc_id);
                $text= $this->filternotif($n7['text'],$arrdata);
                $wa = new Api_wa();
                $st=$wa->send_wa($r->no_telp,$text);

                $hr = json_decode($st);
               
               if($hr->status== 'true'){
                    $xx= array('no'=>$r->no_telp,'text'=>$hr->pesan,'status'=>1,'keterangan'=>'S');
                    $log->save($xx);
                }else{
                     $xx= array('no'=>$r->no_telp,'text'=> $hr->pesan,'status'=>0,'keterangan'=>'S');
                     $log->save($xx);
                }
                //    delay 1second
               sleep(1);
            
                // echo $r->no_telp.' : '.$text.'<br>';
            }
            
        }

//          $setting = new Msetting();
//                                 $sett = $setting->first();
//                                 $arrdata= array('instansi'=>$sett['nama_perusahaan'],
//                                                 'nama' =>$value['nama'],
//                                                 'telp_instansi'=>'0',
//                                                 'kelas'=>$value['kelas'],
//                                                 'nis'=>$value['nis']);
//                                 $p = $this->m_pesan(2,$arrdata);
//                                 if($p['status']== 1 && $p['enable'] == 1){
//                                     //kirim notifikasi pendaftaran 
//                                     $wa = new Api_wa();
//                                      $wa->send_wa($value['no_telp'],$p['pesan']);
//                                 }else{

//                                 }
                               

//          foreach ($result as $r) {
// $text='*Notifikasi Sistem Absensi '.$setting['nama_perusahaan'].'*

// menginformasikan bahwa
// Nama: *'.$r->nama.'*
// NIS: *'.$r->nis.'*
// Kelas: *'.$r->nama_kelas.'*
// Tanggal: *'.Date('d-m-Y').'*
// tidak mengikuti absensi / (ALPHA)

// _nb: jika ada kesalahan, segera hubungi kami_'; 
// $wa = new Api_wa();
// $r=$wa->send_wa($r->no_telp,$text);
//          }
    }
    public function qrcode_wa(){
         
        $wa = new Api_wa();
        $x=$wa->view_qrcode();

        return $x;
    }
    public function qrcode_image_wa(){
         
        $wa = new Api_wa();
        $x=$wa->view_qrcode_image();

        echo $x;
    }
    public function device_wa(){
         
        $wa = new Api_wa();
        $r=$wa->device();

        print_r($r);
    }
    public function testing(){
        $log= new Mlogwa(); 
        $wa = new Api_wa();
        //$wa->view_qrcode();
        // $r=$wa->ceknomor('0857305434111');
        // $rx = json_decode($r);
        // print_r($rx);
       // $r=$wa->view_qrcode();
        // $r=$wa->device();
        // $r=$wa->reload();
        // print_r($r);

       // $wa->reload();

        $no = '085730534634';
$pesan = 'Notifikasi Sistem 

Siswa ABSBDSDB tidak Hadir';
        $wa = new Api_wa();
        $r=$wa->send_wa($no,$pesan);
        if($r->status == 'true'){
            $xx= array('no'=>$no,'text'=>$r->pesan,'status'=>1,'keterangan'=>'S');
            $log->save($xx);
        }else{
             $xx= array('no'=>$no,'text'=> $r->pesan,'status'=>0,'keterangan'=>'S');
             $log->save($xx);
        }
    }

    public function filternotif($pesan, $arrdata){
        $result = array();
            
        if(!empty($arrdata['instansi'])){
            $pesan=str_replace('[INSTANSI]',$arrdata['instansi'],$pesan);       
                                
        }
        
        if(!empty($arrdata['telp_instansi'])){
            $pesan=str_replace('[TELP_INSTANSI]',$arrdata['telp_instansi'],$pesan);                        
        }
        if(!empty($arrdata['nama'])){
            $pesan=str_replace('[NAMA]',$arrdata['nama'],$pesan);                        
        }
        if(!empty($arrdata['kelas'])){
            $pesan=str_replace('[KELAS]',$arrdata['kelas'],$pesan);                        
        }
        if(!empty($arrdata['nokartu'])){
            $pesan=str_replace('[kartu]',$arrdata['nokartu'],$pesan);                        
        }
        
        $pesan=str_replace('[DATE]',date('d-m-Y'),$pesan);                        
        
         
        $result=$pesan;                     
                           

        return $result;
    }
}