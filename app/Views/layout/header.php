<header class="header header-sticky mb-4">
        <div class="container-fluid">
          <button class="header-toggler px-md-0 me-md-3" type="button" onclick="coreui.Sidebar.getInstance(document.querySelector('#sidebar')).toggle()">
            <span class="icon icon-lg">
              <i class="cil-menu"></i>
            </span>
          </button><a class="header-brand d-md-none" href="#">
            <span width="118" height="46" alt="CoreUI Logo">
              <i class="full"></i>
            </span></a>
           
          <ul class="header-nav ms-auto">
            
            <li class="nav-item"><a class="nav-link" href="<?=base_url()?>/panel/logout">
                <span class="icon icon-lg">
                <i class="cil-account-logout"></i>
                </span></a></li>
          </ul>
           
        </div>
        <div class="header-divider"></div>
        <div class="container-fluid">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb my-0 ms-2">
              <li class="breadcrumb-item">
                <!-- if breadcrumb is single--><span>Home</span>
              </li>
              <li class="breadcrumb-item active"><span><?php if(isset($title)) echo $title;?></span></li>
            </ol>
          </nav>
        </div>
      </header>