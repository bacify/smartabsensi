<!doctype html>
<html lang="en">
<head>
    
    <meta charset="utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="Sistem Absensi Sekolah">
    <meta name="author" content="Seranggalangit">
    
    <title>System Absensi Sekolah | <?php if(isset($title)) echo $title;?></title>
    <link rel="shortcut icon" href="<?=base_url();?>/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?=base_url();?>/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="57x57" href="<?=base_url();?>/assets/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?=base_url();?>/assets/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?=base_url();?>/assets/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?=base_url();?>/assets/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?=base_url();?>/assets/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?=base_url();?>/assets/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?=base_url();?>/assets/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?=base_url();?>/assets/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?=base_url();?>/assets/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="<?=base_url();?>/assets/favicon/android-icon-192x192.png">
    
    <link rel="manifest" href="<?=base_url();?>/assets/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?=base_url();?>/assets/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    
    <!-- Bootstrap -->
    <link href="<?php echo base_url();?>/assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Core UI -->
    <link href="<?php echo base_url();?>/assets/css/style.min.css" rel="stylesheet">
    
    <link href="<?php echo base_url();?>/assets/css/custom.css" rel="stylesheet">

    <!-- CORE UI ICON -->
    <link href="<?php echo base_url();?>/assets/icons/css/all.min.css" rel="stylesheet">

    <!-- Simplebar styles-->
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/vendor/simplebar/dist/simplebar.css">
    
    <!-- Datatables -->
    <link rel="stylesheet" href="<?php echo base_url('assets/vendor/datatables/datatables.min.css');?>">
    <!-- Bootstrap Select -->
    <link rel="stylesheet" href="<?php echo base_url('assets/vendor/bootstrap-select/css/bootstrap-select.min.css'); ?>">

    <!-- tempusdominus -->
    <link rel="stylesheet" href="<?php echo base_url('assets/vendor/tempusdominus/tempusdominus-bootstrap-4.min.css'); ?>">
    

    <!-- Bootstrap toogle -->
    <link rel="stylesheet" href="<?php echo base_url('assets/vendor/bootstrap4-toggle/css/bootstrap4-toggle.min.css'); ?>">
    
    <!-- Lightbox2 -->
    <link rel="stylesheet" href="<?php echo base_url('assets/vendor/lightbox2/css/lightbox.min.css'); ?>">

  </head>
  <head>
  <body class="c-app">

    
    <!-- Sidebar -->
    <?= $this->include('layout/sidebar') ?>    
    <div class="wrapper d-flex flex-column min-vh-100 bg-light">

        <!-- HEADER -->
      <?= $this->include('layout/header') ?>    
    
     
      <!-- MAIN BODY -->
      <div class="body flex-grow-1 px-2">
        <div class="container-xl px-1">       
        <?= $this->renderSection('content') ?>    
        </div>
      </div>

      <!-- FOOTER -->
       <?= $this->include('layout/footer') ?>

    </div>


    <!-- FOOTER -->
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->    
    
	  <script src="<?php echo base_url();?>/assets/js/jquery-3.6.0.min.js"></script>	  	  
    <script src="<?php echo base_url();?>/assets/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo base_url();?>/assets/js/coreui.min.js"></script>
    <!-- Bootstrap Select -->
    <script src="<?php echo base_url('assets/vendor/bootstrap-select/js/bootstrap-select.min.js'); ?>"></script>

    <!-- TIMEPICKER -->
    <script src="<?php echo base_url('assets/vendor/moment/moment.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/vendor/tempusdominus/tempusdominus-bootstrap-4.min.js'); ?>"></script>


    <!-- BOOTSTRAP4 Toogle -->
    <script src="<?php echo base_url('assets/vendor/bootstrap4-toggle/js/bootstrap4-toggle.min.js'); ?>"></script>
    
    <script src="<?php echo base_url();?>/assets/vendor/simplebar/dist/simplebar.min.js"></script>
    
    
    
    <?= $this->renderSection('jslibrary') ?>    
   
  </body>
  
</html>







    



    