<div class="sidebar sidebar-light sidebar-fixed sidebar-self-hiding-xl" id="sidebar">
      <div class="sidebar-brand d-none d-md-flex">
      <div class="sidebar-brand-full"  alt="CoreUI Logo">
            <img src="<?=base_url();?>/assets/img/glogo.png" class="img-fluid" alt="Responsive image" width="70" height="70">          
        </div>
        <div class="sidebar-brand-narrow"  alt="CoreUI Logo">
            <img src="<?=base_url();?>/assets/img/glogo.png" class="img-fluid" alt="Responsive image" width="50" height="50">          
        </div>       
      </div>
      <ul class="sidebar-nav" data-coreui="navigation" data-simplebar>
        <li class="nav-item"><a class="nav-link" href="<?=base_url('panel/home');?>">
            <span class="nav-icon">
              <i class="cil-speedometer"></i>
            </span> Dashboard</a></li>
        <li class="nav-title">menu utama</li>
        <li class="nav-item"><a class="nav-link" href="<?=base_url().'/panel/grup';?>">
            <span class="nav-icon">
              <i class="cil-room"></i>
            </span> Data Grup</a></li>
        <li class="nav-item"><a class="nav-link" href="<?=base_url().'/panel/member';?>">
            <span class="nav-icon">
              <i class="cil-people"></i>
            </span> Data Member</a></li>        
        <li class="nav-item"><a class="nav-link" href="<?=base_url().'/panel/absensi';?>">
            <span class="nav-icon">
              <i class="cil-pencil"></i>
            </span> Absensi</a></li> 
        <!-- <li class="nav-item"><a class="nav-link" href="<?=base_url().'/panel/pesan';?>">
            <span class="nav-icon">
              <i class="cil-envelope-closed"></i>
            </span> Pesan</a></li>                  -->
         
        <li class="nav-title">Laporan</li>
        <li class="nav-item"><a class="nav-link" href="<?=base_url().'/panel/laporan';?>">
            <span class="nav-icon">
              <i class="cil-book"></i>
            </span> Laporan Absensi</a></li>    
        <li class="nav-item"><a class="nav-link" href="<?=base_url().'/panel/laporan_manual';?>">
            <span class="nav-icon">
              <i class="cil-book"></i>
            </span> Laporan Absensi manual </a></li>    
       <li class="nav-item"><a class="nav-link" href="<?=base_url().'/panel/riwayat';?>">
            <span class="nav-icon">
              <i class="cil-clock"></i>
            </span> Riwayat Absensi </a></li>   
        <li class="nav-title">Pengaturan</li>
        <li class="nav-item"><a class="nav-link" href="<?=base_url().'/panel/users';?>">
            <span class="nav-icon">
              <i class="cib-superuser"></i>
            </span> Users</a></li>   
        <li class="nav-item"><a class="nav-link" href="<?=base_url().'/panel/setting';?>">
            <span class="nav-icon">
              <i class="cil-cog"></i>
            </span> Pengaturan Aplikasi</a></li> 
        <li class="nav-item"><a class="nav-link" href="<?=base_url().'/panel/setting_libur';?>">
            <span class="nav-icon">
              <i class="cil-cloud"></i>
            </span> Hari Libur</a></li>
        <li class="nav-item"><a class="nav-link" href="<?=base_url().'/panel/setting_jam';?>">
            <span class="nav-icon">
              <i class="cil-clock"></i>
            </span> Jam Absen</a></li>
        <li class="nav-item"><a class="nav-link" href="<?=base_url().'/panel/devicenfc';?>">
        <span class="nav-icon">
          <i class="cil-devices"></i>
        </span> Device Absensi</a></li>       
        <li class="nav-item"><a class="nav-link" href="<?=base_url().'/panel/device';?>">
        <span class="nav-icon">
          <i class="cib-whatsapp"></i>
        </span> Status WhatsApp</a></li>   
        <li class="nav-item"><a class="nav-link" href="<?=base_url().'/panel/notifikasi';?>">
        <span class="nav-icon">
          <i class="cil-comment-bubble"></i>
        </span> Notifikasi</a></li>  
        <li class="nav-item"><a class="nav-link" href="<?=base_url().'/panel/historywa';?>">
        <span class="nav-icon">
          <i class="cil-history"></i>
        </span> Riwayat WA</a></li>  
        
      </ul>
      <button class="sidebar-toggler" type="button" data-coreui-toggle="unfoldable"></button>
    </div>