<?php $this->extend('layout/page_layout'); ?>

<?= $this->section('content') ?>
        <?php if(session()->get('error')):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong><?php echo session()->get('error');?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif;?>
     <div class="card mb-4">
            <div class="card-header"> 
                <h3 class="card-title float-left"><?=$title;?></h3>
                
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-12">
                 
                <div class="table-responsive">
                    <table id="tabel-utama" class="table table-striped table-bordered datatable table-sm">
                        <thead>
                            <tr class=text-center>
                            <th>No</th>
                            <th>Waktu</th>
                            <th>Device</th>
                            <th>Nama</th>    
                            <th>Grup</th>                                               
                            <th>Ketarangan</th>                                               
                            <th style="max-width: 100px;">Foto</th>                                               
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        
                    </table>
                </div>
                </div>
                 
              </div>
            </div>
          </div>
 
 
  
<?= $this->endSection() ?>


<?= $this->section('jslibrary') ?>
<script src="<?php echo base_url('assets/vendor/datatables/datatables.min.js');?>"></script>
<script src="<?php echo base_url('assets/vendor/lightbox2/js/lightbox.min.js');?>"></script>
 
<script>

        
    lightbox.option({
      'resizeDuration': 200,
      'wrapAround': true
    })
 

 $(document).ready(function() {
      
    let table = $('#tabel-utama').DataTable({ 
            "language": 
                {
                 "url" :"<?php echo base_url('assets/vendor/datatables/lang/Indonesian.json');?>" ,
                 
                },
            processing: true,
            serverSide: true,
            responsive: true,
            "pageLength": 100,
            "searching": true, 
            order: [[1,'desc']], //init datatable not ordering
            
            ajax: {
                url: "<?php echo site_url('panel/riwayat_ajax')?>"                
                },
            "createdRow": function( row, data, dataIndex ) {                 
                $(row).addClass( 'align-middle' );                
                
            },            
            columnDefs: [
                { targets: [1,2], className: 'text-nowrap'}, //last column center.                
                { targets: '_all', className: 'text-center'}, //last column center.                               
                { targets: [1,2,3,4], searchable: true}, //last column center.              
                { targets: '_all', searchable: false}, //last column center.                
                { targets: [1,2], orderable: true}, //last column center.              
                { targets: '_all', orderable: false}, //last column center.  
                               
                
            ],
            "dom": 'Bfrtip',
            buttons: [             
             
             {  extend: 'copy',                
                className: 'btn btn-info',
                 
             },
             {  extend: 'excel',
                title: function (){
                    return '<?php echo $title.' '; ?>'+kel_text+' '+bb_text+' '+tahun;
                },
                className: 'btn btn-success',
               
             },
             {  extend: 'print',                
                className: 'btn btn-default',
                 title: '',
                messageTop : function (){
                    return '<?php echo $title.' '; ?>'+kel_text+' '+bb_text+' '+tahun;
                },
                customize: function(win)
                    {
        
                        var last = null;
                        var current = null;
                        var bod = [];
        
                        var css = '@page { size: landscape; }',
                            head = win.document.head || win.document.getElementsByTagName('head')[0],
                            style = win.document.createElement('style');
        
                        style.type = 'text/css';
                        style.media = 'print';
        
                        if (style.styleSheet)
                        {
                        style.styleSheet.cssText = css;
                        }
                        else
                        {
                        style.appendChild(win.document.createTextNode(css));
                        }
        
                        head.appendChild(style);
                }
             },
         ],
    });


  
    $('#tabel-utama').attr('style', 'border-collapse: collapse !important');
 
});
</script>
<?= $this->endSection() ?>
