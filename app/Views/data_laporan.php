<?php $this->extend('layout/page_layout'); ?>

<?= $this->section('content') ?>
        <?php if(session()->get('error')):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong><?php echo session()->get('error');?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif;?>
     <div class="card mb-4">
            <div class="card-header"> 
                <h3 class="card-title float-left"><?=$title;?></h3>
                
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-12">
                <div class="form-inline mb-2">
                    <div class="form-group">
                    <label for="tahun" class="mb-0 pb-0 mr-2">Tahun :</label> 
                                <select name="tahun"  class="filtertahun form-control" title="Pilih grup" >                                 
                                    <?php 
                                        foreach ($tahun as $t) {
                                            if(date('Y')==$t)
                                                echo '<option value="'.$t.'" selected> '.$t.' </option>';
                                            else
                                            echo '<option value="'.$t.'"> '.$t.' </option>';
                                        }
                                    ?>
                                </select>
                    </div>
                    <div class="form-group">
                    <label for="tanggal" class="mb-0 pb-0 mr-2">bulan :</label> 
                                <select name="bulan"  class="filtertanggal form-control" title="Pilih Bulan" >                                 
                                    <?php 
                                        foreach ($bulan as $k) {
                                            
                                            if(date('m') == $k['input'])
                                                echo '<option value="'.$k['input'].'" selected> '.$k['value'].' </option>';
                                            else 
                                                echo '<option value="'.$k['input'].'" > '.$k['value'].' </option>';
                                        }
                                    ?>
                                </select>
                    </div>
                    <div class="form-group mx-3">
                    <label for="grup" class="mb-0 pb-0 mr-2">grup :</label> 
                                <select name="grup"  class="filtergrup form-control" title="Pilih grup" >                                 
                                    <?php 
                                        $x=1;
                                        foreach ($grup as $k) {
                                            if($x==1)
                                                echo '<option value="'.$k['id_grup'].'" selected> '.$k['nama_grup'].' </option>';
                                            else 
                                                echo '<option value="'.$k['id_grup'].'"> '.$k['nama_grup'].' </option>';

                                                $x++;
                                        }
                                    ?>
                                </select>
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="tabel-utama" class="table table-striped table-bordered datatable table-sm">
                        <thead>
                            <tr class=text-center>
                            <th>No</th>
                            <th>noKartu</th>
                            <th>Nama</th>    
                            <th>1</th>                   
                            <th>2</th>
                            <th>3</th>
                            <th>4</th>
                            <th>5</th>
                            <th>6</th>
                            <th>7</th>
                            <th>8</th>
                            <th>9</th>
                            <th>10</th>
                            <th>11</th>
                            <th>12</th>
                            <th>13</th>
                            <th>14</th>
                            <th>15</th>
                            <th>16</th>
                            <th>17</th>
                            <th>18</th>
                            <th>19</th>
                            <th>20</th>
                            <th>21</th>
                            <th>22</th>
                            <th>23</th>
                            <th>24</th>
                            <th>25</th>
                            <th>26</th>
                            <th>27</th>
                            <th>28</th>
                            <th>29</th>
                            <th>30</th>
                            <th>31</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        
                    </table>
                </div>
                </div>
                 
              </div>
            </div>
          </div>
 
 
  
<?= $this->endSection() ?>


<?= $this->section('jslibrary') ?>
<script src="<?php echo base_url('assets/vendor/datatables/datatables.min.js');?>"></script>
 
<script>

        $('.tombolsubmit').hide();
function daysInMonth (month, year) { // Use 1 for January, 2 for February, etc.
  return new Date(year, month, 0).getDate();
}

function getLibur(){
    let bulan = $('.filtertanggal').selectpicker('val');
    let tahun = $('.filtertahun').selectpicker('val');    
    let total=daysInMonth(bulan,tahun);
    //console.log('bulan '+bulan+', tahun '+tahun);
    let list=[];

    let hari = "<?=$setting['hari_libur']?>";
    let libur = hari.split(',');

    let abc=[];
     let x =0;
     if(libur[0]==1){
        abc[x++] = 1;
     }
     if(libur[1]==1){
        abc[x++] = 2;
     }
     if(libur[2]==1){
        abc[x++] = 3;
     }
     if(libur[3]==1){
        abc[x++] = 4;
     }
     if(libur[4]==1){
        abc[x++] = 5;
     }
     if(libur[5]==1){
        abc[x++] = 6;
     }
     if(libur[6]==1){
        abc[x++] = 0;
     }
     let v=0;
     
     
     for(let x=1;x<=total;x++){
        let d = new Date(tahun,bulan-1,x);
           // console.log(d.getDay());
            if(abc.includes(d.getDay())){
                list[v++]=x;
            }
        }

      return list;
    
}


 $(document).ready(function() {
     
    $('select').selectpicker();
    let kel_text =$('.filtergrup option:selected' ).text();
    let bb = $('.filtertanggal').selectpicker('val');
    let bb_text= $('.filtertanggal option:selected' ).text();

    let tahun = $('.filtertahun').selectpicker('val');    
    let kel = $('.filtergrup').selectpicker('val');    
 
    

     
    let table = $('#tabel-utama').DataTable({ 
            "language": 
                {
                 "url" :"<?php echo base_url('assets/vendor/datatables/lang/Indonesian.json');?>" ,
                 
                },
            processing: true,
            serverSide: true,
            "pageLength": 100,
            "searching": false, 
            order: [], //init datatable not ordering
            
            ajax: {
                url: "<?php echo site_url('panel/report_ajax')?>",
                data:function(d){
                    d.grup = $('.filtergrup').selectpicker('val');
                    d.bulan = $('.filtertanggal').selectpicker('val');
                    d.tahun = $('.filtertahun').selectpicker('val');
                }
                },
            "createdRow": function( row, data, dataIndex ) {                 
                    $(row).addClass( 'align-middle' );  
                        let libur = getLibur();
                        //console.log(libur);
                         
                        console.log(libur);
                        $.each(data,function(key,val){
                        if(key >=3 && key <= data.length){
                             
                                if( val ==  "X"){
                                $(row).find('td:eq('+key+')').addClass('bg-danger');
                               // $(row).addClass('bg-danger');
                                }

                                if(libur.includes(key-2)){
                                    $(row).find('td:eq('+key+')').addClass('bg-warning');
                                
                                }
                                
                        }
                        
                        });
                    
                    
                    
                
                
            },
                   
            columnDefs: [
                {
                        // The `data` parameter refers to the data for the cell (defined by the
                        // `data` option, which defaults to the column being worked with, in
                        // this case `data: 0`.
                        "render": function ( data, type, row,meta) {
                             let libur = getLibur();
                            if(libur.includes(meta.col - 2))                       
                                return 'X';
                            else 
                                return data;
                           
                        },
                        "targets": '_all'
                 },
                { targets: [1,2], className: 'text-nowrap'}, //last column center.                
                { targets: '_all', className: 'text-center'}, //last column center.
                { targets: [1,2], searchable: true}, //last column center.              
                { targets: '_all', searchable: false}, //last column center.                
                { targets: [1,2], orderable: true}, //last column center.              
                { targets: '_all', orderable: false}, //last column center.                  

                               
                
            ],
            "dom": 'Bfrti',
            buttons: [             
             
             {  extend: 'copy',                
                className: 'btn btn-info',
                 
             },
             {  extend: 'excel',
                title: function (){
                    return '<?php echo $title.' '; ?>'+kel_text+' '+bb_text+' '+tahun;
                },
                className: 'btn btn-success',
               
             },
             {  extend: 'print',                
                className: 'btn btn-default',
                 title: '',
                messageTop : function (){
                    return '<?php echo $title.' '; ?>'+kel_text+' '+bb_text+' '+tahun;
                },
                customize: function(win)
                    {
        
                        var last = null;
                        var current = null;
                        var bod = [];
        
                        var css = '@page { size: landscape; }',
                            head = win.document.head || win.document.getElementsByTagName('head')[0],
                            style = win.document.createElement('style');
        
                        style.type = 'text/css';
                        style.media = 'print';
        
                        if (style.styleSheet)
                        {
                        style.styleSheet.cssText = css;
                        }
                        else
                        {
                        style.appendChild(win.document.createTextNode(css));
                        }
        
                        head.appendChild(style);
                }
             },
         ],
    });



    $('.filtergrup').change(function(event){
        kel = $('.filtergrup').selectpicker('val'); 
        kel_text =$('.filtergrup option:selected' ).text();   
        table.ajax.reload();
    })
    $('.filtertanggal').change(function(event){        
        bb = $('.filtertanggal').selectpicker('val');
        bb_text= $('.filtertanggal option:selected' ).text();
        table.ajax.reload();
    })
    $('.filtertahun').change(function(event){
        tahun = $('.filtertahun').selectpicker('val'); 
        table.ajax.reload();
    })
    function reloadtable(){
         
         var column = table.column( 1 );
 
        // Toggle the visibility
        column.visible( ! column.visible() );
        
        let bb = $('.filtertanggal').selectpicker('val');
        let tahun = $('.filtertahun').selectpicker('val');        
        let dm = new Date(tahun, bb+1, 0).getDate();

        // if(dm == 31){
        //     // Get the column API object
        // var column = table.column(-1); 
        // // Toggle the visibility
        // column.visible(true);
        // }else if(dm == 30){
        //     var column = table.column(-1); 
        //     // Toggle the visibility
        //     column.visible(false);
        // }else if(dm == 29){
        //     var column = table.column(32); 
        //     // Toggle the visibility
        //     column.visible(true);
        //     var column2 = table.column([33,34]);
        //     column2.visible(false);
        // }else{
            
        //     var column2 = table.column([31,32,33]);
        //     column2.visible(false);
        // }
        // table.ajax.reload();
    }

    $('#tabel-utama').attr('style', 'border-collapse: collapse !important');
 
});
</script>
<?= $this->endSection() ?>
