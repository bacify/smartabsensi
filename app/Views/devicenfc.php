<?php $this->extend('layout/page_layout'); ?>

<?= $this->section('content') ?>

        <?php if(session()->get('error')):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong><?php echo session()->get('error');?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif;?>
        <div class="card mb-4 col-lg-8 col-md-12 col-sm-12">
            <div class="card-header"> 
                <h3 class="card-title float-left"><?=$title;?></h3>
                 
            </div>
            <form id="form-pengaturan" action="<?php echo base_url('panel/device/save');?>" method="post">
            <div class="card-body">
            <input type="hidden" name="token" value="<?=$token;?>">
            <div class="row">
                <div class="col-2">
                <label  class="mb-0 pb-0">No</label>    
                </div>
                <div class="col">
                        Mode
                </div>
                <div class="col">
                        Keterangan
                </div>
            </div>
            <hr>
                <?php 
                    $x=1;
                    foreach($device as $dev){

                    ?>
                
            <div class="row my-1">
                <div class="col-2">
                <label  class="mb-0 pb-0"><?=$x++;?></label>    
                </div>
                <div class="col">
                        <input type="checkbox" <?php if($dev['mode']==1) echo 'checked';?> name="dev[<?=$dev['id_device'];?>]" data-toggle="toggle" data-on="Daftar" data-off="Absen" data-onstyle="primary" data-offstyle="success">
                </div>
                <div class="col">
                        <input type="text" class="form-control" name="keterangan[<?=$dev['id_device'];?>]" value="<?=$dev['keterangan'];?>">
                </div>
            </div>
            <?php }?>

                    
            </div>
            <div class="card-footer">
                <button type="submit" name="submit" value="submit" class="btn btn-primary tombolsubmit float-right mb-1">Simpan</button>
            </div>
            </form>
        </div>



          <!-- Modal delete Product-->
  <form id="deleteform" action="<?php echo base_url('panel/d/tanggal_delete');?>" method="post">
         <div class="modal fade" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Hapus Tanggal</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                           <input type="hidden" name="id" class="form-control" required>
                                                 <strong>Apakah anda yakin akan menghapus data ini?</strong>
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Hapus</button>
                   </div>
                    </div>
            </div>
         </div>
    </form>

<?= $this->endSection() ?>

<?= $this->section('jslibrary') ?>
<script src="<?php echo base_url('assets/vendor/datatables/datatables.min.js');?>"></script>
 
<script>
function deleteTanggal(id){
    
                
    $('#ModalDelete').modal('show');
    $('#deleteform [name="id"]').val(id);

}
function reloadsetting(btn){
     btn.disabled = true;
            
            $.ajax({
                method: "GET",
                url: "<?php echo base_url('panel/setting/reload');?>",
                dataType: "json",
                }).done(function(msg ) {
                    
                       if(msg.status){
                           alert(msg.pesan);
                           location.reload();
                       }else{
                            alert(msg.pesan);
                            btn.disabled = false;
                       }
                
                });
}
$(document).ready(function() {
    
    $('#tanggal-libur').datetimepicker({
                format: 'DD-MM-YYYY',                
                });  

    let table = $('#tabel-utama').DataTable({ 
                "language": 
                {
                 "url" :"<?php echo base_url('assets/vendor/datatables/lang/Indonesian.json');?>" ,
                 "infoFiltered": "",
                 "infoPostFix": ""
                },
            processing: true,
            serverSide: true,
            "pageLength": 50,
            // responsive: true,
            order: [], //init datatable not ordering
            ajax: {
                url: "<?php echo site_url('panel/d/tanggal_libur')?>",
                   },
            "createdRow": function( row, data, dataIndex ) {                 
                $(row).addClass( 'align-middle' );
                 
                
            },            
            columnDefs: [
                
                { targets: -1, className: 'text-nowrap text-center'}, //last column center.
                                
            ],
            "dom": 'ti',
          
    });



    

});
</script>
<?= $this->endSection() ?>