<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords"
          content="absensi display welcome notifikasi smartabsensi rfid">
    <meta name="author" content="bacify">
    <title>$title</title>
    
    <link rel="shortcut icon" href="<?=base_url();?>/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?=base_url();?>/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="57x57" href="<?=base_url();?>/assets/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?=base_url();?>/assets/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?=base_url();?>/assets/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?=base_url();?>/assets/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?=base_url();?>/assets/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?=base_url();?>/assets/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?=base_url();?>/assets/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?=base_url();?>/assets/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?=base_url();?>/assets/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="<?=base_url();?>/assets/favicon/android-icon-192x192.png">
    
    <link rel="manifest" href="<?=base_url();?>/assets/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?=base_url();?>/assets/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    
    <link href="<?=base_url('assets/css/display.css');?>" rel="stylesheet" id="style">
    <!-- Bootstrap -->
    <link href="<?php echo base_url();?>/assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">
    <!-- Font Awesome-->
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
</head>
<body onload="startTime()">

<!-- Page Content -->
<div class="container mt-4">
    <div class="row mt-5">
        <div class="col-md-6 offset-md-3 col-sm-10 offset-sm-1 text-center">
            <div class="card mt-5 " style="opacity:.75;">
                <div class="card-header">
                    <h2 class="font-weight-bold">Sistem Absensi RFID</h2>
                </div>
                <div class="card-body " style="min-height:250px;">
                    <div class="content">
                     
                        <h4 > Silahkan Tempelkan Kartu Anda</h4>
                        <h5 class="pesan mt-5 font-weight-bold"> Menunggu Kartu...</h5>
                    </div>
                    <div class="result"></div>
                </div>
                <div class="card-footer">
                        <p id="jam" class="tanggal m-0 p-0 font-weight-bold">10:37:00, 01 September 2022</p>
                </div>
            </div>
             
               
            
        </div>
    </div>
    </br></br></br>
    <!--footer-->
    <div class="footer text-white text-center">
        <p>© 2022 SmartAbsensi. All rights reserved | Design by <a href="https://seranggalangit.xyz">Seranggalangit</a></p>
    </div>
    <!--//footer-->
</div>
    <script src="<?php echo base_url();?>/assets/js/jquery-3.6.0.min.js"></script>	  	  
    <script src="<?php echo base_url();?>/assets/js/bootstrap.bundle.min.js"></script>


    <script>
function startTime() {
  const today = new Date();
  let h = today.getHours();
  let m = today.getMinutes();
  let s = today.getSeconds();
  m = checkTime(m);
  s = checkTime(s);
  var d = new Date();  
            var n = d.getDate();  
            var month = new Array();  
            month[0] = "January";  
            month[1] = "February";  
            month[2] = "March";  
            month[3] = "April";  
            month[4] = "May";  
            month[5] = "June";  
            month[6] = "July";  
            month[7] = "August";  
            month[8] = "September";  
            month[9] = "October";  
            month[10] = "November";  
            month[11] = "December";  
            var t = month[d.getMonth()];  
            var y = d.getFullYear(); 
  document.getElementById('jam').innerHTML =  h + ":" + m + ":" + s + " , " + n + " "+t+" "+y ;
  $('.pesan').toggle();
  setTimeout(startTime, 500);
}

function checkTime(i) {
  if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
  return i;
}
</script>
<script type="text/javascript">
	let time ='';
	function pulldisplay(timestamp)
    {
        var queryString = {'timestamp' : timestamp};
        
        $.ajax(
            {
                type: 'GET',
                url: '<?php echo base_url('getdisplay');?>',
                data: queryString,
                
            }
             ).done(function(data){                     
                     var obj = jQuery.parseJSON(data);
                     // console.log(obj);                     
                     updatedisplay(obj.tipe,obj.nama,obj.nomorkartu);
                     time = obj.timestamp;
                     pulldisplay(obj.timestamp);
            }).fail(function(){
                console.log('error, recall pooling ');
                pulldisplay(time);
            });
    }

    function updatedisplay(tipe, nama,nomorkartu){
        if(tipe == 1)
        $('.result').html('<h3 class="my-2">Selamat Datang</h3> <h2 class="font-weight-bold">'+nama+'</h2>');
        else if(tipe==2)
        $('.result').html('<h3 class="my-2">Semoga Selamat Sampai Tujuan</h3> <h2 class="font-weight-bold">'+nama+'</h2>');
        else if(tipe == 3){
            $('.result').html('<h2 class="my-5">Sudah Absen!</h2>');
        }
        else if(tipe == 4){
            $('.result').html('<h3 class="my-2">'+nomorkartu+'</h3> <h2 class="my-5">Kartu Tidak Terdaftar!</h2>');
        }
        else {
            $('.result').html('<h2 class="my-5">Belum Waktunya Absen!</h2>');
        }
        $('.result').show();
        $('.content').hide();

        setTimeout(() => {
            $('.result').hide();
            $('.result').html('');
            $('.content').show();
        }, 2000);
    }


    $(document).ready(function(){
        pulldisplay();
    })
</script>

</body>
</html>
