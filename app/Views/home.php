<?php $this->extend('layout/page_layout'); ?>

<?= $this->section('content') ?>
<h1>Absensi Online QRCODE READER</h1>

<main >
    <div class="row">    
        <div  id="reader" class="col <?php if($status == 0) echo 'd-none';?>" ></div>        
    </div>
   
    <div class="row mt-2">    
        <div  id="qr-reader-results" class="col border border-info text-center" style="min-height:100px;">
            <h4 class="text-center my-0"><u>Hasil</u></h4>
            <h1 class="my-2"><span id="tekserror" class="badge badge-danger"></span></h1>
            <h1 class="my-2"><span id="teksok" class="badge badge-primary"></span></h1>            
            <p class="teksno my-0"></p>
            <p class="teksnama my-0"></p>
        </div>        
    </div>
   
</main>


<?= $this->endSection() ?>


<?= $this->section('jslibrary') ?>
<script src="<?php echo base_url('assets/js/html5-qrcode.min.js');?>"></script>
 
<script>
$('#qr-reader-results').hide();


const config = { fps: 1, qrbox: 250 }; 
let html5QrCodescanner = new Html5QrcodeScanner("reader", config,true);

const qrCodeSuccessCallback = (decodedText, decodedResult) => {
    /* handle success */
    console.log(decodedText);
    console.log('aaaa');
    html5QrcodeScanner.clear();

};
function onScanSuccess(decodedText, decodedResult) {
  // handle the scanned code as you like, for example:
  console.log(`Code matched = ${decodedText}`, decodedResult);
  if (window.navigator && window.navigator.vibrate) {
        
        // Shake that device!
        // Vibrate once for 1 second
        navigator.vibrate(500);         
        setTimeout(()=>{alert('OK')}, 500);
        console.log('vibrate support');
        console.log('Code matchedx = '+decodedText+' '+decodedResult);
        $('#tekserror').html('');
        $('#teksok').html('');
        $('.teksno').html('');
        $('.teksnama').html('');
        $('#qr-reader-results').hide("slow");
        $.ajax({
                method: "POST",
                url: "<?php echo base_url('panel/a/absensi');?>",
                dataType: "json",
                data: {nis: decodedText}
                }).done(function(msg ) {
                    
                    if(msg.status == 1){                        
                        $('#teksok').html('OK');
                        $('.teksno').html(msg.siswa.nis);
                        $('.teksnama').html(msg.siswa.nama);
                        $('#qr-reader-results').show("slow");
                        
                    }else{
                        $('#tekserror').html(msg.text);
                        $('#qr-reader-results').show("slow");
                    
                    }
                
                }).fail(function(error){
                    console.log('error '+error);
                    
                });
    } else {
    // Not supported
        console.log('vibrate not support');
    }
   
}

function onScanFailure(error) {
  // handle scan failure, usually better to ignore and keep scanning.
  // for example:
  console.warn(`Code scan error = ${error}`);
}
 

html5QrCodescanner.render(onScanSuccess,onScanFailure);

 
</script>
<?= $this->endSection() ?>
