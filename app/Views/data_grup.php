<?php $this->extend('layout/page_layout'); ?>

<?= $this->section('content') ?>
        <?php if(session()->get('error')):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong><?php echo session()->get('error');?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif;?>
     <div class="card mb-4">
            <div class="card-header"> 
                <h3 class="card-title float-left"><?=$title;?></h3>
                <button class="btn btn-success float-right" onclick="addgrup()">Tambah</button>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-12">
                <table id="tabel-utama" class="table table-striped table-bordered datatable">
                    <thead>
                        <tr>
                        <th>No</th>
                        <th>Nama Grup</th>                         
                        <th>Tindakan</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    </table>
                </div>
                 
              </div>
            </div>
          </div>

<!-- Modal Add Product-->
<form id="form-tambah-siswa" action="<?php echo base_url('panel/d/grup_add');?>" method="post">
         <div class="modal fade " id="myModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
               <div class="modal-content  ">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Data Grup</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                   
                   </div>
                   <div class="modal-body">
                        <div class="form-group">
                            <label for="nama" class="mb-0 pb-0">Nama Grup</label>
                            <input type="hidden" name="ids" value="">
                            <input type="hidden" name="token" value="<?=$token;?>">
                           <input type="text" name="nama" class="form-control" placeholder="Grup A" autocomplete="off">                            
                       </div>                                   
 
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" name="submit" value="submit" class="btn btn-success tombolsubmit">Save</button>
                   </div>
                    </div>
            </div>
         </div>
</form>
 

 <!-- Modal Update Product-->
<form id="updateform" action="<?php echo base_url('panel/d/grup_update');?>" method="post">
         <div class="modal fade" id="ModalUpdate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
               <div class="modal-content">
                   <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Update Grup </h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                        <div class="form-group">
                            <label for="nama" class="mb-0 pb-0">Nama Grup</label>
                            <input type="hidden" name="ids" value="">
                            <input type="hidden" name="token" value="<?=$token;?>">
                           <input type="text" name="nama" class="form-control" placeholder="X1 " autocomplete="off">                            
                       </div>                                   
 
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>                        
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Update</button>
                   </div>
                </div>
            </div>
         </div>
</form>
  
  <!-- Modal delete Product-->
  <form id="deleteform" action="<?php echo base_url('panel/d/grup_delete');?>" method="post">
         <div class="modal fade" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Hapus Transaksi</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                           <input type="hidden" name="id" class="form-control" required>
                                                 <strong>Apakah anda yakin akan menghapus data ini?</strong>
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Hapus</button>
                   </div>
                    </div>
            </div>
         </div>
    </form>
<?= $this->endSection() ?>


<?= $this->section('jslibrary') ?>
<script src="<?php echo base_url('assets/vendor/datatables/datatables.min.js');?>"></script>
 
<script>

         

        function addgrup(){
            resetform();
            $('#myModalAdd').modal('show')
        }
        function resetform(){

            $('.loadingiconnisid').hide();
            $('.nisid-gagal').hide();
            $('.nisid-ok').hide();
            $('#form-tambah-siswa').trigger("reset");
            
        }


 $(document).ready(function() {
     
    $('select').selectpicker();
    
    $('#tabel-utama').DataTable({ 
            "language": 
                {
                 "url" :"<?php echo base_url('assets/vendor/datatables/lang/Indonesian.json');?>" 
                },
            processing: true,
            serverSide: true,
            responsive: true,
            order: [], //init datatable not ordering
            ajax: "<?php echo site_url('panel/grupajax')?>",
            "createdRow": function( row, data, dataIndex ) {                 
                $(row).addClass( 'align-middle' );
                
            },            
            columnDefs: [                
                { targets: 0, className: 'text-nowrap text-center', width:'5%'}, //last column center.
                
               
                
            ]
             
    });
    $('#tabel-utama').attr('style', 'border-collapse: collapse !important');

    // get Edit Records
    $('#tabel-utama').on('click','.edit_record',function(){
            let id=$(this).data('id');
            let nama=$(this).data('nama');
            $('#updateform [name="ids"]').val(id);                     
            $('#updateform [name="nama"]').val(nama);   
            $('#ModalUpdate').modal('show');
    });

    $('#tabel-utama').on('click','.delete_record',function(){
                var id=$(this).data('id');
                $('#ModalDelete').modal('show');
                $('#deleteform [name="id"]').val(id);
    });
});
</script>
<?= $this->endSection() ?>
