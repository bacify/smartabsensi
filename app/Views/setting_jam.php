<?php $this->extend('layout/page_layout'); ?>

<?= $this->section('content') ?>

        <?php if(session()->get('error')):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong><?php echo session()->get('error');?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif;?>        

        <div class="card mb-4 col-lg-8 col-md-12 col-sm-12">
            <div class="card-header"> 
                <h3 class="card-title float-left">Jam Absensi</h3>
                 
            </div>
             <div class="card-body">
                 
             <button type="button" class="btn btn-primary btn-jam" onclick="addJam()">Tambah Jam</button>
                <div class="form-group">
                     <div class="table-responsive">
                    <table id="tabel-utama" class="table table-striped table-bordered datatable">
                        <thead>
                            <tr>
                            <th>No</th>
                            <th>Label</th>
                            <th>Jam Masuk </th>                             
                            <th>Jam Pulang</th>
                            <th>Tindakan</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        
                        </table>
                    </div>
                </div>
            </div>
            <div class="card-footer">
            </div>
             
        </div>

<!-- Modal Add Product-->
<form id="form-tambah-jam" action="<?php echo base_url('panel/d/jam_add');?>" method="post">
         <div class="modal fade " id="myModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
               <div class="modal-content  ">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Data Jam</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                   
                   </div>
                   <div class="modal-body">
                        <div class="form-group">
                            <label for="nama" class="mb-0 pb-0">Label</label>
                            <input type="hidden" name="ids" value="0">
                            <input type="hidden" name="token" value="<?=$token;?>">
                           <input type="text" name="nama" class="form-control" placeholder="Nama member" autocomplete="off">                            
                       </div>
                        
                    <div class="form-group row">
                        <label for="nama" class="mb-0 pb-0">Absen Jam Masuk </label>
                        <input type="hidden" name="id" value="1">
                        <input type="hidden" name="token" value="<?=$token;?>">
                        <div class="col-5">
                            <div class="input-group date" id="timepick-masuk-start" data-target-input="nearest">                           
                                <input type="text" name="masuk_start" class="form-control datetimepicker-input" data-target="#timepick-masuk-start" value="">
                                <div class="input-group-append" data-target="#timepick-masuk-start" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="cil-clock"></i></div>
                                </div>
                            </div>
                        </div>    
                        <div class="col-2 text-center">
                            -
                        </div>
                        <div class="col-5">
                            <div class="input-group date" id="timepick-masuk-end" data-target-input="nearest">                           
                                <input type="text" name="masuk_end" class="form-control datetimepicker-input" data-target="#timepick-masuk-end" value="">
                                <div class="input-group-append" data-target="#timepick-masuk-end" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="cil-clock"></i></div>
                                </div>
                            </div>
                        </div>    
                                                
                    </div>
                    <div class="form-group row">
                        <label for="nama" class="mb-0 pb-0">Absen Jam Pulang </label>
                        <input type="hidden" name="id" value="1">
                        <input type="hidden" name="token" value="<?=$token;?>">
                        <div class="col-5">
                            <div class="input-group date" id="timepick-keluar-start" data-target-input="nearest">                           
                                <input type="text" name="keluar_start" class="form-control datetimepicker-input" data-target="#timepick-keluar-start" value="">
                                <div class="input-group-append" data-target="#timepick-keluar-start" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="cil-clock"></i></div>
                                </div>
                            </div>
                        </div>    
                        <div class="col-2 text-center">
                            -
                        </div>
                        <div class="col-5">
                            <div class="input-group date" id="timepick-keluar-end" data-target-input="nearest">                           
                                <input type="text" name="keluar_end" class="form-control datetimepicker-input" data-target="#timepick-keluar-end" value="">
                                <div class="input-group-append" data-target="#timepick-keluar-end" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="cil-clock"></i></div>
                                </div>
                            </div>
                        </div>    
                                                
                    </div>
                        
 
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" name="submit" value="submit" class="btn btn-success tombolsubmit">Save</button>
                   </div>
                    </div>
            </div>
         </div>
</form>
 

 <!-- Modal Update Product-->
<form id="updateform" action="<?php echo base_url('panel/d/jam_add');?>" method="post">
         <div class="modal fade" id="ModalUpdate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
               <div class="modal-content">
                   <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Update Transaksi <strong class="idmaster"></strong></h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                        <div class="form-group">
                            <label for="nama" class="mb-0 pb-0">Label</label>
                            <input type="hidden" name="ids" value="0">
                            <input type="hidden" name="token" value="<?=$token;?>">
                           <input type="text" name="nama" class="form-control" placeholder="Nama member" autocomplete="off">                            
                       </div>
                        
                    <div class="form-group row">
                        <label for="nama" class="mb-0 pb-0">Absen Jam Masuk </label>
                        <input type="hidden" name="id" value="1">
                        <input type="hidden" name="token" value="<?=$token;?>">
                        <div class="col-5">
                            <div class="input-group date" id="utimepick-masuk-start" data-target-input="nearest">                           
                                <input type="text" name="masuk_start" class="form-control datetimepicker-input" data-target="#utimepick-masuk-start" value="">
                                <div class="input-group-append" data-target="#utimepick-masuk-start" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="cil-clock"></i></div>
                                </div>
                            </div>
                        </div>    
                        <div class="col-2 text-center">
                            -
                        </div>
                        <div class="col-5">
                            <div class="input-group date" id="utimepick-masuk-end" data-target-input="nearest">                           
                                <input type="text" name="masuk_end" class="form-control datetimepicker-input" data-target="#utimepick-masuk-end" value="">
                                <div class="input-group-append" data-target="#utimepick-masuk-end" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="cil-clock"></i></div>
                                </div>
                            </div>
                        </div>    
                                                
                    </div>
                    <div class="form-group row">
                        <label for="nama" class="mb-0 pb-0">Absen Jam Pulang </label>
                        <input type="hidden" name="id" value="1">
                        <input type="hidden" name="token" value="<?=$token;?>">
                        <div class="col-5">
                            <div class="input-group date" id="utimepick-keluar-start" data-target-input="nearest">                           
                                <input type="text" name="keluar_start" class="form-control datetimepicker-input" data-target="#utimepick-keluar-start" value="">
                                <div class="input-group-append" data-target="#utimepick-keluar-start" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="cil-clock"></i></div>
                                </div>
                            </div>
                        </div>    
                        <div class="col-2 text-center">
                            -
                        </div>
                        <div class="col-5">
                            <div class="input-group date" id="utimepick-keluar-end" data-target-input="nearest">                           
                                <input type="text" name="keluar_end" class="form-control datetimepicker-input" data-target="#utimepick-keluar-end" value="">
                                <div class="input-group-append" data-target="#utimepick-keluar-end" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="cil-clock"></i></div>
                                </div>
                            </div>
                        </div>    
                                                
                    </div>
                        
 
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>                        
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Update</button>
                   </div>
                    </div>
            </div>
         </div>
</form>
          <!-- Modal delete Product-->
  <form id="deleteform" action="<?php echo base_url('panel/d/jam_delete');?>" method="post">
         <div class="modal fade" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Hapus Jam</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                           <input type="hidden" name="id" class="form-control" required>
                                                 <strong>Apakah anda yakin akan menghapus data ini?</strong>
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Hapus</button>
                   </div>
                    </div>
            </div>
         </div>
    </form>

<?= $this->endSection() ?>

<?= $this->section('jslibrary') ?>
<script src="<?php echo base_url('assets/vendor/datatables/datatables.min.js');?>"></script>
 
<script>
    function addJam(){
            
            resetform();            
            $('#myModalAdd').modal('show')
        }
        function resetform(){

            $('.loadingiconnokartuid').hide();
            $('.nokartuid-gagal').hide();
            $('.nokartuid-ok').hide();
            $('#form-tambah-member').trigger("reset");
            
        }
function deleteJam(id){
    
                
    $('#ModalDelete').modal('show');
    $('#deleteform [name="id"]').val(id);

}
function reloadsetting(btn){
     btn.disabled = true;
            
            $.ajax({
                method: "GET",
                url: "<?php echo base_url('panel/setting/reload');?>",
                dataType: "json",
                }).done(function(msg ) {
                    
                       if(msg.status){
                           alert(msg.pesan);
                           location.reload();
                       }else{
                            alert(msg.pesan);
                            btn.disabled = false;
                       }
                
                });
}
$(document).ready(function() {
    
    $('#timepick-masuk-start').datetimepicker({
            format: 'HH:mm:ss'
                });  
    $('#timepick-masuk-end').datetimepicker({
            format: 'HH:mm:ss'
                });
    $('#timepick-keluar-start').datetimepicker({
            format: 'HH:mm:ss'
                });
    $('#timepick-keluar-end').datetimepicker({
            format: 'HH:mm:ss'
                });
    $('#utimepick-masuk-start').datetimepicker({            
            format: 'HH:mm:ss'
                });  
            $('#utimepick-masuk-end').datetimepicker({
                    format: 'HH:mm:ss'
                        });
            $('#utimepick-keluar-start').datetimepicker({
                    format: 'HH:mm:ss'
                        });
            $('#utimepick-keluar-end').datetimepicker({
                    format: 'HH:mm:ss'
                        });
            
   
    let table = $('#tabel-utama').DataTable({ 
                "language": 
                {
                 "url" :"<?php echo base_url('assets/vendor/datatables/lang/Indonesian.json');?>" ,
                 "infoFiltered": "",
                 "infoPostFix": ""
                },
            processing: true,
            serverSide: true,
            "pageLength": 50,
            // responsive: true,
            order: [], //init datatable not ordering
            ajax: {
                url: "<?php echo site_url('panel/d/jam_absen')?>",
                   },
            "createdRow": function( row, data, dataIndex ) {                 
                $(row).addClass( 'align-middle' );
                 
                
            },            
            columnDefs: [
                
                { targets: -1, className: 'text-nowrap text-center'}, //last column center.
                                
            ],
            "dom": 'ti',
          
    });


// get Edit Records
    $('#tabel-utama').on('click','.edit_record',function(){
            var id=$(this).data('id');
            var nama=$(this).data('nama');
            var masuk_start=$(this).data('masuk_start');
            var masuk_end=$(this).data('masuk_end');
            var keluar_start=$(this).data('keluar_start');
            var keluar_end=$(this).data('keluar_end');
            $('#updateform [name="ids"]').val(id);            
            $('#updateform [name="nama"]').val(nama);  
            $('#utimepick-masuk-start').datetimepicker('destroy');
            $('#utimepick-masuk-end').datetimepicker('destroy');
            $('#utimepick-keluar-start').datetimepicker('destroy');
            $('#utimepick-keluar-end').datetimepicker('destroy');
            $('#utimepick-masuk-start').datetimepicker({
                date:masuk_start,
            format: 'HH:mm:ss'
                });  
            $('#utimepick-masuk-end').datetimepicker({
                date:masuk_end,
                    format: 'HH:mm:ss'
                        });
            $('#utimepick-keluar-start').datetimepicker({
                date:keluar_start,
                    format: 'HH:mm:ss'
                        });
            $('#utimepick-keluar-end').datetimepicker({
                date:keluar_end,
                    format: 'HH:mm:ss'
                        });
                    
           // $('#utimepick-masuk-start').val(masuk_start);  
           //  $('#utimepick-masuk-end').val(masuk_end);
           //  $('#utimepick-keluar-start').val(keluar_start);
           //  $('#utimepick-keluar-end').val(keluar_end);
            $('#ModalUpdate').modal('show');
              
    });

    

});
</script>
<?= $this->endSection() ?>