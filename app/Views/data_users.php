<?php $this->extend('layout/page_layout'); ?>

<?= $this->section('content') ?>
        <?php if(session()->get('error')):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong><?php echo session()->get('error');?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif;?>
     <div class="card mb-4">
            <div class="card-header"> 
                <h3 class="card-title float-left"><?=$title;?></h3>
                <button class="btn btn-success float-right" onclick="adduser()">Tambah</button>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-12">
                 
                <table id="tabel-utama" class="table table-striped table-bordered datatable">
                    <thead>
                        <tr>
                        <th>No</th> 
                        <th>Nama</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Akses</th>
                        
                        <th>Tindakan</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                     
                    </table>
                </div>
                 
              </div>
            </div>
          </div>


<!-- Modal Add Product-->
<form id="form-tambah-user" action="<?php echo base_url('panel/u/admin_add');?>" method="post">
         <div class="modal fade " id="myModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
               <div class="modal-content  ">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Tambah Pengguna Baru (admin)</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                   
                   </div>
                   <div class="modal-body">
                        <div class="form-group">
                        <label for="no_register" class="mb-0 pb-0">Nama</label>
                             
                           <input type="text" name="nama" class="form-control" placeholder="nama" required>
                       </div>
                       <div class="form-group">
                            <label for="judul" class="mb-0 pb-0">Username</label>
                           <small class="status"> 
                                <div class="spinner-border spinner-border-sm  loadingicon" role="status"  style="display:none">
                                    <span class="sr-only">Loading...</span>
                                </div>    
                                <i class="cil-x-circle ikon-gagal" style="display:none"> username sudah terpakai</i>
                                <i class="cil-check-circle ikon-ok"style="display:none"></i>
                                 </small>
                            <input type="text" name="username" class="form-control " placeholder="Username" required>
                                                    
                       </div>
                       <div class="form-group">
                            <label for="email" class="mb-0 pb-0">Email</label>
                           <input type="email" name="email" class="form-control" placeholder="contoh@gmail.com" >
                       </div>
                        <div class="form-group">
                            <label for="password" class="mb-0 pb-0">Password</label>
                           <input type="password" name="password" class="form-control" placeholder="password" required>
                       </div>

                       <div class="form-group">
                        <label for="kategori" class="mb-0 pb-0">Akses</label>
                           <select name="akses" class="form-control" data-live-search="true" title="Akses"   required>                                                                                               
                                                            <option value="1">Admin</option>
                                                            <option value="2" selected>operator</option>                                                        
                                                 </select>
                       </div>
                                                     
                                       
 
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        
                        <button type="submit" name="submit" value="submit" class="btn btn-success tombolsubmit">Save</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>


     <!-- Modal Add Product-->
     <form id="form-update-user" action="<?php echo base_url('panel/u/admin_add');?>" method="post">
         <div class="modal fade " id="myModalUpdate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
               <div class="modal-content  ">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Edit Data User(admin)</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                   
                   </div>
                   <div class="modal-body">
                        <div class="form-group">
                        <label for="no_register" class="mb-0 pb-0">Nama</label>
                            <input type="hidden" name="id" value="0">
                           <input type="text" name="nama" class="form-control" placeholder="nama" required>
                       </div>
                       <div class="form-group">
                            <label for="judul" class="mb-0 pb-0">Username</label>
                           <small class="status"> 
                                <div class="spinner-border spinner-border-sm  loadingicon" role="status"  style="display:none">
                                    <span class="sr-only">Loading...</span>
                                </div>    
                                <i class="cil-x-circle ikon-gagal" style="display:none"> username sudah terpakai</i>
                                <i class="cil-check-circle ikon-ok"style="display:none"></i>
                                 </small>
                            <input type="text" name="username" class="form-control " placeholder="Username" required>
                                                    
                       </div>
                       <div class="form-group">
                            <label for="email" class="mb-0 pb-0">Email</label>
                           <input type="email" name="email" class="form-control" placeholder="contoh@gmail.com" >
                       </div>
                        <div class="form-group">
                            <label for="password" class="mb-0 pb-0">Password</label>
                           <input type="text" name="password" class="form-control" placeholder="Kosongi jika tidak diubah" >
                       </div>

                       <div class="form-group">
                        <label for="kategori" class="mb-0 pb-0">Akses</label>
                           <select name="akses" class="form-control" data-live-search="true" title="Akses" required>
                                                                                               
                                                            <option value="1">Admin</option>
                                                            <option value="2">Operator</option>
                                                        
                                                 </select>
                       </div>
                                                     
                                       
 
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        
                        <button type="submit" name="submit" value="submit" class="btn btn-success tombolsubmit">Save</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>
  
  <!-- Modal delete Product-->
  <form id="deleteform" action="<?php echo base_url('panel/u/admin_delete');?>" method="post">
         <div class="modal fade" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Hapus UserAdmin</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                           <input type="hidden" name="id" class="form-control" required>
                                                 <strong>Apakah anda yakin akan menghapus data ini?</strong>
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Hapus</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>


<?= $this->endSection() ?>


<?= $this->section('jslibrary') ?>
<script src="<?php echo base_url('assets/vendor/datatables/datatables.min.js');?>"></script>
 
<script>
  $('input[name=username]').change(function(){
           $('.status').removeClass('text-success').removeClass('text-danger');
            $('.loadingicon').show();
            $('.ikon-gagal').hide();
            $('.ikon-ok').hide();
            var user= $(this).val();
            $('.tombolsubmit').hide();
            $.ajax({
                method: "POST",
                url: "<?php echo base_url('panel/u/checkadmin');?>",
                data: {username: user}
                }).done(function(msg ) {
                        setTimeout(
                    function() 
                    {   
                        if(msg=='1'){
                            $('.status').addClass('text-success');
                            $('.loadingicon').hide(); 
                            $('.ikon-ok').show();
                            $('.tombolsubmit').show();
                        }else{
                            $('.status').addClass('text-danger');
                            $('.loadingicon').hide(); 
                            $('.ikon-gagal').show();
                            $('.tombolsubmit').hide();
                        }
                    }, 1000);
                
                });
            
        });

        function adduser(){
            resetform();
            $('#myModalAdd').modal('show')
        }
        function resetform(){

            $('.loadingicon').hide();
            $('.ikon-gagal').hide();
            $('.ikon-ok').hide();
            $('#form-tambah-user').trigger("reset");
            $('#form-edit-user').trigger("reset");
        }

        // get Edit Records
        $('#tabel-utama').on('click','.edit_record',function(){
            resetform();
            var id=$(this).data('id');
            var nama=$(this).data('nama');
            var username=$(this).data('username');
            var email=$(this).data('email');
            var akses=$(this).data('akses');
            
            
                
                $('#myModalUpdate').modal('show');
                $('#form-update-user [name="id"]').val(id);
                $('#form-update-user [name="nama"]').val(nama);
                $('#form-update-user [name="username"]').val(username);
                $('#form-update-user [name="email"]').val(email);
                $('#form-update-user [name="akses"]').selectpicker('val', akses);

                
                if(id == 1 || akses != 1 ){    
                   
                    $('#form-update-user [name="akses"]').selectpicker('destroy');   
                    $('#form-update-user [name="akses"] option:not(:selected)').prop('disabled', true);
                    $('#form-update-user [name="akses"] option:selected ').prop('disabled', false);
                    
                     
                }else{
                    $('#form-update-user [name="akses"]').selectpicker();  
                    $('#form-update-user [name="akses"] option:not(:selected)').prop('disabled', false);
                    $('#form-update-user [name="akses"] option:selected ').prop('disabled', false);
                    
                }
                
               
                
                
            
        });

        $('#tabel-utama').on('click','.delete_record',function(){
            var id=$(this).data('id');
            $('#ModalDelete').modal('show');
            $('#deleteform [name="id"]').val(id);
         });


    $(document).ready(function(){
            // selectpicker
            $('select').selectpicker();

        // Setup datatables

        let table = $('#tabel-utama').DataTable({ 
            "language": 
                {
                    "url" :"<?php echo base_url('assets/vendor/datatables/lang/Indonesian.json');?>" 
                },
            processing: true,
            serverSide: true,
            responsive: true,
            order: [], //init datatable not ordering
            ajax: "<?php echo base_url('panel/useradminajax')?>",
            
            columnDefs: [
                
                { targets: -1, className: 'dt-body-nowrap text-center'}, //last column center.
                { targets: 4, className: 'dt-body-nowrap text-center'}, //last column center.
                { targets: 5, className: 'dt-body-nowrap text-center'}, //last column center.
            
                
            ],
            "dom": 'Bfrtip',
            buttons: [             
            
            {  extend: 'copy',                
                className: 'btn btn-default',
                exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
            },
            {  extend: 'excel',
                title: '<?php echo $title.' '.date('d-m-Y'); ?> ',
                className: 'btn btn-default',
                exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
            },
            {  extend: 'print',                
                className: 'btn btn-default',
                exportOptions: {
                    title: '<?php echo $title.' '.date('d-m-Y'); ?> ',
                    messageTop :'<?php echo $title.' '.date('d-m-Y'); ?> ',
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
            },
        ],
        });
    });


</script>
<?= $this->endSection() ?>
