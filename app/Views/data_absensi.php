<?php $this->extend('layout/page_layout'); ?>

<?= $this->section('content') ?>
        <?php if(session()->get('error')):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong><?php echo session()->get('error');?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif;?>
     <div class="card mb-4">
            <div class="card-header"> 
                <h3 class="card-title float-left"><?=$title;?></h3>
                
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-12">
                <div class="form-inline mb-2">
                    <div class="form-group">
                    <label for="tanggal" class="mb-0 pb-0 mr-2">Tanggal :</label> 
                                <select name="tanggal"  class="filtertanggal form-control" title="Pilih grup" >                                 
                                    <?php 
                                        foreach ($tanggal as $k) {
                                            echo '<option value="'.$k['input'].'" > '.$k['value'].' </option>';
                                        }
                                    ?>
                                </select>
                    </div>
                    <div class="form-group mx-3">
                    <label for="grup" class="mb-0 pb-0 mr-2">grup :</label> 
                                <select name="grup"  class="filtergrup form-control" title="Pilih grup" >                                 
                                    <?php 
                                        $x=1;
                                        foreach ($grup as $k) {
                                            if($x==1)
                                                echo '<option value="'.$k['id_grup'].'" selected> '.$k['nama_grup'].' </option>';
                                            else 
                                                echo '<option value="'.$k['id_grup'].'"> '.$k['nama_grup'].' </option>';

                                                $x++;
                                        }
                                    ?>
                                </select>
                    </div>
                </div>
                <div class="table-responsive">
                <table id="tabel-utama" class="table table-striped table-bordered datatable">
                    <thead>
                        <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Nama member</th>
                        <th>grup</th>                        
                        <th>Masuk</th>
                        <th>Keluar</th>
                        <th>Keterangan</th>
                        <th>Tindakan</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                     
                    </table>
                </div>
                </div>
                 
              </div>
            </div>
          </div>

<!-- Modal Add Product-->
<form id="form-tambah-member" action="<?php echo base_url('panel/a/absensi_manual');?>" method="post">
         <div class="modal fade " id="myModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
               <div class="modal-content  ">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Data Absensi</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                   
                   </div>
                   <div class="modal-body">
                        <div class="form-group">
                            <label for="nama" class="mb-0 pb-0">Nama member</label>
                            <input type="hidden" name="ids" value="0">
                            <input type="hidden" name="id_absen" value="0">
                            <input type="hidden" name="token" value="<?=$token;?>">
                            <input type="hidden" name="created_by" value="manual">
                           <input type="text" name="nama" class="form-control" placeholder="Nama member" autocomplete="off" readonly>                            
                       </div>
                        <div class="form-group">
                            <label for="nfc" class="mb-0 pb-0">nfc</label>                                  
                            <input type="text" id="nfc" name="nfc" class="form-control" placeholder="101101" autocomplete="off" readonly>
                       </div>
                       <div class="form-group">
                            <label for="grup" class="mb-0 pb-0">grup</label>
                            <select id="grup-add" name="grup" class="form-control" title="Pilih grup" readonly> 
                                <?php 
                                    foreach ($grup as $k) {
                                        echo '<option value="'.$k['id_grup'].'" > '.$k['nama_grup'].' </option>';
                                    }
                                ?>
                            </select>
                       </div>                        
                       <div class="form-group">
                            <label for="telp" class="mb-0 pb-0">keterangan</label>       
                            <select name="keterangan"  class="form-control" title="Pilih Keterangan" required> 
                                <option class="text-success font-weight-bold" value="M">Absen Masuk</option>
                                <option class="text-secondary font-weight-bold"  value="K">Absen Keluar</option>
                                <option class="text-primary font-weight-bold" value="I">Ijin</option>
                                <option class="text-danger font-weight-bold" value="A">Alpha</option>
                                <option class="text-info font-weight-bold" value="S">Sakit</option>                                
                            </select>
                       </div>               
 
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" name="submit" value="submit" class="btn btn-success tombolsubmit">Save</button>
                   </div>
                    </div>
            </div>
         </div>
</form>
 

 <!-- Modal Update Product-->
<form id="updateform" action="<?php echo base_url('panel/a/absensi_manual');?>" method="post">
         <div class="modal fade" id="ModalUpdate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
               <div class="modal-content">
                   <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Update Transaksi <strong class="idmaster"></strong></h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                        <div class="form-group">
                            <label for="nama" class="mb-0 pb-0">Nama member</label>
                            <input type="hidden" name="ids" value="0">
                            <input type="hidden" name="id_absen" value="0">
                            <input type="hidden" name="token" value="<?=$token;?>">
                            <input type="hidden" name="created_by" value="manual">
                           <input type="text" name="nama" class="form-control" placeholder="Nama member" autocomplete="off" readonly>                            
                       </div>
                        <div class="form-group">
                            <label for="nfc" class="mb-0 pb-0">idkartu</label>                                  
                            <input type="text" id="nfc" name="nfc" class="form-control" placeholder="101101" autocomplete="off" readonly>
                       </div>
                       <div class="form-group">
                            <label for="grup" class="mb-0 pb-0">grup</label>
                            <select id="grup-add" name="grup" class="form-control" title="Pilih grup" readonly> 
                                <?php 
                                    foreach ($grup as $k) {
                                        echo '<option value="'.$k['id_grup'].'" > '.$k['nama_grup'].' </option>';
                                    }
                                ?>
                            </select>
                       </div>                        
                       <div class="form-group">
                            <label for="telp" class="mb-0 pb-0">keterangan</label>       
                            <select name="keterangan"  class="form-control" title="Pilih Keterangan" required> 
                            <option class="text-success font-weight-bold" value="M">Absen Masuk</option>
                                <option class="text-secondary font-weight-bold"  value="K">Absen Keluar</option>
                                <option class="text-primary font-weight-bold" value="I">Ijin</option>
                                <option class="text-danger font-weight-bold" value="A">Alpha</option>
                                <option class="text-info font-weight-bold" value="S">Sakit</option>                                   
                            </select>
                       </div>               
 
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>                        
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Update</button>
                   </div>
                    </div>
            </div>
         </div>
</form>
  
  <!-- Modal delete Product-->
  <form id="deleteform" action="<?php echo base_url('panel/d/member_delete');?>" method="post">
         <div class="modal fade" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Hapus Transaksi</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                           <input type="hidden" name="id" class="form-control" required>
                                                 <strong>Apakah anda yakin akan menghapus data ini?</strong>
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Hapus</button>
                   </div>
                    </div>
            </div>
         </div>
    </form>
<?= $this->endSection() ?>


<?= $this->section('jslibrary') ?>
<script src="<?php echo base_url('assets/vendor/datatables/datatables.min.js');?>"></script>
 
<script>

        
        
       
        
        // SET IDENTITAS NUMBER ONLY
        setInputFilter(document.getElementById("nfc"), function(value) {
            return /^\d*\.?\d*$/.test(value); // Allow digits and '.' only, using a RegExp
        });
        function setInputFilter(textbox, inputFilter) {
            ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
                textbox.addEventListener(event, function() {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
                });
            });
        }



        // check ID
        $('input[name=nfc]').change(function(){
           $('.nfcid').removeClass('text-success').removeClass('text-danger');
            $('.loadingiconnfcid').show();
            $('.nfcid-gagal').hide();
            $('.nfcid-ok').hide();
            var user= $(this).val();
            $('.tombolsubmit').hide();
            $.ajax({
                method: "POST",
                url: "<?php echo base_url('panel/d/check_nfc');?>",
                data: {nfc: user}
                }).done(function(msg ) {
                        setTimeout(
                    function() 
                    {   
                        if(msg=='1'){
                            $('.nfcid').addClass('text-success');
                            $('.loadingiconnfcid').hide(); 
                            $('.nfcid-ok').show();
                            $('.tombolsubmit').show();
                        }else{
                            $('.nfcid').addClass('text-danger');
                            $('.loadingiconnfcid').hide(); 
                            $('.nfcid-gagal').show();
                            $('.tombolsubmit').hide();
                        }
                    }, 1000);
                
                });
            
        });

        function adduser(){
            resetform();
            $('#myModalAdd').modal('show')
        }
        function resetform(){

            $('.loadingiconnfcid').hide();
            $('.nfcid-gagal').hide();
            $('.nfcid-ok').hide();
            $('#form-tambah-member').trigger("reset");
            
        }


 $(document).ready(function() {
     
    $('select').selectpicker();
    $('.filtertanggal').selectpicker('val','<?=date("Y-m-d");?>');
    

    let table = $('#tabel-utama').DataTable({ 
            "language": 
                {
                 "url" :"<?php echo base_url('assets/vendor/datatables/lang/Indonesian.json');?>" ,
                 "infoFiltered": "",
                 "infoPostFix": ""
                },
            processing: true,
            serverSide: true,
            "pageLength": 50,
            // responsive: true,
            order: [], //init datatable not ordering
            ajax: {
                url: "<?php echo site_url('panel/absensi_ajax')?>",
                data:function(d){
                    d.grup = $('.filtergrup').selectpicker('val');
                    d.tanggal = $('.filtertanggal').selectpicker('val');
                }
                },
            "createdRow": function( row, data, dataIndex ) {                 
                $(row).addClass( 'align-middle' );
                 
                
            },            
            columnDefs: [
                
                { targets: -1, className: 'text-nowrap text-center'}, //last column center.
                { targets: -2, className: 'text-center'}, //last column center.
                { targets: [0,-1,-2], searchable: false}, //last column center.
               
                
            ],
            "dom": 'Bfrtip',
            buttons: [             
             
             {  extend: 'copy',                
                className: 'btn btn-info',
                exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
             },
             {  extend: 'excel',
                title: '<?php echo $title.' '.date('d-m-Y'); ?> ',
                className: 'btn btn-success',
                exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
             },
             {  extend: 'print',                
                className: 'btn btn-default',
                exportOptions: {
                    title: '<?php echo $title.' '.date('d-m-Y'); ?> ',
                    messageTop :'<?php echo $title.' '.date('d-m-Y'); ?> ',
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
             },
         ],
    });

    $('.filtergrup').change(function(event){
        table.ajax.reload();
    })
    $('.filtertanggal').change(function(event){
        table.ajax.reload();
    })

    $('#tabel-utama').attr('style', 'border-collapse: collapse !important');

    $('#tabel-utama').on('click','.add_record',function(){
            let id=$(this).data('id');
            
            $.ajax({
                method: "GET",
                url: "<?php echo base_url('panel/d/member_detail');?>/"+id, 
                dataType: "json",               
                }).done(function(msg ) {
                    console.log(msg);
                    $('#form-tambah-member [name="ids"]').val(id);
                    $('#form-tambah-member [name="nfc"]').val(msg.result.nfc);
                    $('#form-tambah-member [name="nama"]').val(msg.result.nama);
                    $('#form-tambah-member [name="grup"]').selectpicker('val',msg.result.grup_id);   
                    $('#form-tambah-member [name="grup"]').prop('disabled', true);   
                    $('#form-tambah-member [name="grup"]').selectpicker('refresh');         
                                   
                    $('#myModalAdd').modal('show')
                        
                
                });
            
            
              
    });
    // get Edit Records
    $('#tabel-utama').on('click','.edit_record',function(){
            let id=$(this).data('id');
            let absen=$(this).data('idabsen');
            $.ajax({
                method: "GET",
                url: "<?php echo base_url('panel/d/member_detail');?>/"+id, 
                dataType: "json",               
                }).done(function(msg ) {
                     
                    $('#updateform [name="ids"]').val(id);
                    $('#updateform [name="id_absen"]').val(absen);
                    $('#updateform [name="nfc"]').val(msg.result.nfc);
                    $('#updateform [name="nama"]').val(msg.result.nama);
                    $('#updateform [name="grup"]').selectpicker('val',msg.result.grup);   
                    $('#updateform [name="grup"]').prop('disabled', true);   
                    $('#updateform [name="grup"]').selectpicker('refresh');         
                                   
                    $('#ModalUpdate').modal('show')
                        
                
                });
             
              
    });

    $('#tabel-utama').on('click','.delete_record',function(){
                var id=$(this).data('id');
                $('#ModalDelete').modal('show');
                $('#deleteform [name="id"]').val(id);
    });
});
</script>
<?= $this->endSection() ?>
