<!DOCTYPE html>
<!--
* CoreUI - Free Bootstrap Admin Template
* @version v4.0.1
* @link https://coreui.io
* Copyright (c) 2021 creativeLabs Łukasz Holeczek
* Licensed under MIT (https://coreui.io/license)
-->

<html lang="en">
  <head>
  
    <meta charset="utf-8">    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="Sistem Absensi Sekolah">
    <meta name="author" content="Seranggalangit">
    
    <title>System Absensi Sekolah | <?php if(isset($title)) echo $title;?></title>
    <link rel="apple-touch-icon" sizes="57x57" href="assets/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="assets/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="assets/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="assets/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="assets/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="assets/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="assets/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="assets/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="assets/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    
    <!-- Core UI -->    
    <link href="<?php echo base_url();?>/assets/css/coreui.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>/assets/css/custom.css" rel="stylesheet">

    <!-- CORE UI ICON -->
    <link href="<?php echo base_url();?>/assets/icons/css/all.min.css" rel="stylesheet">

  </head>
  <body>
    <div class="bg-dark min-vh-100 d-flex flex-row align-items-center">
    <a href="<?=base_url('display');?>" class="btn btn-outline-info float-button">
    <i class="cil-devices"></i>
    </a>
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-8">
            
            <div class="card-group d-block d-md-flex row">
              <div class="card col-md-7 p-4 mb-0">
                <div class="card-body">
                 <form role="form" action="<?=base_url('pauth');?>" method="POST">
                    <h1>Login</h1>
                    <p class="text-medium-emphasis">Sign In to your account</p>
                    <div class="input-group mb-3"><span class="input-group-text">
                        
                          <i class="cil-user"></i>
                        </span>
                      <input class="form-control" id="username" type="username" name="username" placeholder="username" value="" autocomplete="off">
                    </div>
                    <div class="input-group mb-4"><span class="input-group-text">
                        
                          <i class="cil-lock-locked"></i>
                        </span>
                      <input class="form-control" type="password" name="password" placeholder="Password">
                    </div>
                    <div class="row">
                      <div class="col-6">
                      
                        <button class="btn btn-primary px-4" type="submit">Masuk</button>
                      </div>
                      <div class="col-6 text-end">
                        <button class="btn btn-link px-0" type="button">Lupa password?</button>
                      </div>                      
                    </div>
                  </form>
                  
                </div>
                
              </div>
              <div class="card col-md-5 text-white bg-danger py-5">
                <div class="card-body text-center">
                  <div>
                    <img class="img-fluid mx-auto" width="50%" src="<?=base_url();?>/assets/img/logo.png">
                    <h4  class="text-center mt-3" >Sistem Absensi Online</h4>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
   
    <script src="<?php echo base_url();?>/assets/js/jquery-3.6.0.min.js"></script>
    <script src="<?php echo base_url();?>/assets/js/coreui.bundle.min.js"></script>    
    <!-- <script src="https://unpkg.com/@coreui/coreui@3.4.0/dist/js/coreui.bundle.js"></script>      -->
    
    <script>
     
      
    </script>
  </body>
</html>