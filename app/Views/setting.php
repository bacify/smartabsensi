<<?php $this->extend('layout/page_layout'); ?>

<?= $this->section('content') ?>

        <?php if(session()->get('error')):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong><?php echo session()->get('error');?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif;?>
        <div class="card mb-4 col-lg-8 col-md-12 col-sm-12">
            <div class="card-header"> 
                <h3 class="card-title float-left"><?=$title;?></h3>
                 
            </div>
            <form id="form-pengaturan" action="<?php echo base_url('panel/setting/save');?>" method="post">
            <div class="card-body">
                
                     <div class="form-group">
                            <label for="limit" class="mb-0 pb-0">Instansi</label>       
                            <input type="text" name="instansi" class="form-control" placeholder="WW Computer" value="<?=$setting['nama_perusahaan']?>">
                            <small id="passwordHelpBlock" class="form-text text-muted">
                                    *digunakan untuk mengirim laporan absensi 
                            </small>
                    </div> 
                    <div class="form-group row">
                        <label for="nama" class="mb-0 pb-0">Absen Jam Masuk </label>
                        <input type="hidden" name="id" value="1">
                        <input type="hidden" name="token" value="<?=$token;?>">
                        <div class="col-5">
                            <div class="input-group date" id="timepick-masuk-start" data-target-input="nearest">                           
                                <input type="text" name="masuk_start" class="form-control datetimepicker-input" data-target="#timepick-masuk-start" value="<?=$setting['jam_masuk_start']?>">
                                <div class="input-group-append" data-target="#timepick-masuk-start" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="cil-clock"></i></div>
                                </div>
                            </div>
                        </div>    
                        <div class="col-2 text-center">
                            -
                        </div>
                        <div class="col-5">
                            <div class="input-group date" id="timepick-masuk-end" data-target-input="nearest">                           
                                <input type="text" name="masuk_end" class="form-control datetimepicker-input" data-target="#timepick-masuk-end" value="<?=$setting['jam_masuk_end']?>">
                                <div class="input-group-append" data-target="#timepick-masuk-end" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="cil-clock"></i></div>
                                </div>
                            </div>
                        </div>    
                                                
                    </div>
                    <div class="form-group row">
                        <label for="nama" class="mb-0 pb-0">Absen Jam Pulang </label>
                        <input type="hidden" name="id" value="1">
                        <input type="hidden" name="token" value="<?=$token;?>">
                        <div class="col-5">
                            <div class="input-group date" id="timepick-keluar-start" data-target-input="nearest">                           
                                <input type="text" name="keluar_start" class="form-control datetimepicker-input" data-target="#timepick-keluar-start" value="<?=$setting['jam_pulang_start']?>">
                                <div class="input-group-append" data-target="#timepick-keluar-start" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="cil-clock"></i></div>
                                </div>
                            </div>
                        </div>    
                        <div class="col-2 text-center">
                            -
                        </div>
                        <div class="col-5">
                            <div class="input-group date" id="timepick-keluar-end" data-target-input="nearest">                           
                                <input type="text" name="keluar_end" class="form-control datetimepicker-input" data-target="#timepick-keluar-end" value="<?=$setting['jam_pulang_end']?>">
                                <div class="input-group-append" data-target="#timepick-keluar-end" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="cil-clock"></i></div>
                                </div>
                            </div>
                        </div>    
                                                
                    </div>
                    <div class="form-group">
                            <label for="limit" class="mb-0 pb-0">Status WhatsApp</label>    
                            <div class="d-block"   >
                                <input id="statuswa" name="status" type="checkbox" value="1" <?php if($setting['wa_status']==1) echo 'checked';?> data-toggle="toggle">
                            </div>
                    </div>                     
                    <div class="form-group">
                            <label for="token" class="mb-0 pb-0">Token WhatsApp</label>  
                        <div class="input-group mb-3">     
                            <input type="text" name="watoken" class="form-control" placeholder="token Whatsapp" value="<?=$setting['wa_token']?>">
                            <div class="input-group-append">
                                <button class="btn btn-outline-info" type="button" onclick="reloadsetting(this)">Cek Token</button>
                            </div>
                        </div>
                    </div> 
                    <div class="form-group">
                            <label for="limit" class="mb-0 pb-0">Masa Aktif TokenWhatsApp</label>       
                            <input type="text" name="exp" class="form-control" placeholder="Klik Cek Token untuk Melihat" value="<?=$setting['wa_exp']?>" disabled>
                    </div>                     
                    <div class="form-group">
                            <label for="limit" class="mb-0 pb-0">Email</label>       
                            <input type="email" name="email" class="form-control" placeholder="contoh@gmail.com" value="<?=$setting['email']?>">
                            <small id="passwordHelpBlock" class="form-text text-muted">
                                    *digunakan untuk mengirim laporan absensi 
                            </small>
                    </div> 
                    <div class="form-group">
                            <label for="limit" class="mb-0 pb-0">Jam Notifikasi Email</label>       
                            <input type="number" name="email_jam" class="form-control" max="23" placeholder="0" value="<?=$setting['email_jam']?>">
                            <small id="passwordHelpBlock" class="form-text text-muted">
                                    *Jam Notifikasi email harian kepada Email Admin
                            </small>
                    </div> 
                    <div class="form-group">
                            <label for="limit" class="mb-0 pb-0">Jam Notifikasi WhatsaApp</label>       
                            <input type="number" name="wa_jam" class="form-control" max="23" placeholder="0" value="<?=$setting['wa_jam']?>">
                            <small id="passwordHelpBlock" class="form-text text-muted">
                                    *Jam Notifikasi yang dikirimkan pada nomor WA siswa
                            </small>
                    </div> 
                    <div class="form-group">
                            <label for="limit" class="mb-0 pb-0">Status Notifikasi Email</label>    
                            <div class="d-block"   >
                                <input id="email_notif" name="email_notif" type="checkbox" value="1" <?php if($setting['email_notif']==1) echo 'checked';?> data-toggle="toggle">
                            </div>
                    </div>  
                    <div class="form-group">
                            <label for="limit" class="mb-0 pb-0">Status Notifikasi WhatsaApp</label>    
                            <div class="d-block"   >
                                <input id="wa_notif" name="wa_notif" type="checkbox" value="1" <?php if($setting['wa_notif']==1) echo 'checked';?> data-toggle="toggle">
                            </div>
                    </div>  
            
            </div>
            <div class="card-footer">
                <button type="submit" name="submit" value="submit" class="btn btn-success tombolsubmit float-right mb-1">Simpan</button>
            </div>
            </form>
        </div>
<?= $this->endSection() ?>

<?= $this->section('jslibrary') ?>
<script>

function reloadsetting(btn){
     btn.disabled = true;
            
            $.ajax({
                method: "GET",
                url: "<?php echo base_url('panel/setting/reload');?>",
                dataType: "json",
                }).done(function(msg ) {
                    
                       if(msg.status){
                           alert(msg.pesan);
                           location.reload();
                       }else{
                            alert(msg.pesan);
                            btn.disabled = false;
                       }
                
                });
}
$(document).ready(function() {
    $('#timepick-masuk-start').datetimepicker({
            format: 'HH:mm:ss'
                });  
    $('#timepick-masuk-end').datetimepicker({
            format: 'HH:mm:ss'
                });
    $('#timepick-keluar-start').datetimepicker({
            format: 'HH:mm:ss'
                });
    $('#timepick-keluar-end').datetimepicker({
            format: 'HH:mm:ss'
                });
    $('#statuswa').bootstrapToggle();



});
</script>
<?= $this->endSection() ?>