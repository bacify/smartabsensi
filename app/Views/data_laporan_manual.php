<?php $this->extend('layout/page_layout'); ?>

<?= $this->section('content') ?>
        <?php if(session()->get('error')):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong><?php echo session()->get('error');?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif;?>
     <div class="card mb-4">
            <div class="card-header"> 
                <h3 class="card-title float-left"><?=$title;?></h3>
                
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-12">
                <div class="form-row mb-2">
                    <div class="col border-right">
                    <label for="tahun" class="mb-0 pb-0 mr-2">Start :</label> 
                                <select name="tahun"  class="filtertahun form-control" title="Pilih grup" >                                 
                                    <?php 
                                        foreach ($tahun as $t) {
                                            if(date('Y')==$t)
                                                echo '<option value="'.$t.'" selected> '.$t.' </option>';
                                            else
                                            echo '<option value="'.$t.'"> '.$t.' </option>';
                                        }
                                    ?>
                                </select>
                                <label for="bulan" class="mb-0 pb-0 mr-2"></label> 
                                <select name="bulan"  class="filtertanggal form-control" title="Pilih Bulan" >                                 
                                    <?php 
                                        foreach ($bulan as $k) {
                                            
                                            if(date('m') == $k['input'])
                                                echo '<option value="'.$k['input'].'" selected> '.$k['value'].' </option>';
                                            else 
                                                echo '<option value="'.$k['input'].'" > '.$k['value'].' </option>';
                                        }
                                    ?>
                                </select>
                    </div>
                    <div class="col border-right">
                    <label for="tanggal" class="mb-0 pb-0 mr-2">Akhir :</label> 
                                <select name="tahunakhir"  class="filtertahunakhir form-control" title="Pilih grup" >                                 
                                    <?php 
                                        foreach ($tahun as $t) {
                                            if(date('Y')==$t)
                                                echo '<option value="'.$t.'" selected> '.$t.' </option>';
                                            else
                                            echo '<option value="'.$t.'"> '.$t.' </option>';
                                        }
                                    ?>
                                </select>
                                <label for="bulan" class="mb-0 pb-0 mr-2"></label> 
                                <select name="bulanakhir"  class="filtertanggalakhir form-control" title="Pilih Bulan" >                                 
                                    <?php 
                                        foreach ($bulan as $k) {
                                            
                                            if(date('m') == $k['input'])
                                                echo '<option value="'.$k['input'].'" selected> '.$k['value'].' </option>';
                                            else 
                                                echo '<option value="'.$k['input'].'" > '.$k['value'].' </option>';
                                        }
                                    ?>
                                </select>
                   
                    </div>
                    <div class="col mx-3">
                    <label for="grup" class="mb-0 pb-0 mr-2">grup :</label> 
                                <select name="grup"  class="filtergrup form-control" title="Pilih grup" >                                 
                                    <?php 
                                        $x=1;
                                        foreach ($grup as $k) {
                                            if($x==1)
                                                echo '<option value="'.$k['id_grup'].'" selected> '.$k['nama_grup'].' </option>';
                                            else 
                                                echo '<option value="'.$k['id_grup'].'"> '.$k['nama_grup'].' </option>';

                                                $x++;
                                        }
                                    ?>
                                </select>
                    </div>
                </div>
                 <div class="table-responsive">
                    <table id="tabel-utama" class="table table-striped table-bordered datatable table-sm">
                        <thead>
                            <tr class=text-center>
                            <th style="width:5%">No</th>
                            <th style="width:10%">NoKartu</th>
                            <th>Nama</th>    
                            <th>A</th>                   
                            <th>I</th>
                            <th>S</th>
                            <th>M1</th>
                            <th>M2</th>
                            <th>X</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        
                    </table>
                    <small id="passwordHelpBlock" class="form-text text-info">
                             *A = Absen, I = Ijin, S=Sakit, M1 = Absen 1x, M2 = Absen Lengkap, X = Libur
                      </small>
                 </div>
                </div>
                 
              </div>
            </div>
          </div>
 
 
  
<?= $this->endSection() ?>


<?= $this->section('jslibrary') ?>
<script src="<?php echo base_url('assets/vendor/datatables/datatables.min.js');?>"></script>
 
<script>

        $('.tombolsubmit').hide();

 $(document).ready(function() {
     
    $('select').selectpicker();
    let kel_text =$('.filtergrup option:selected' ).text();
    let bb = $('.filtertanggal').selectpicker('val');
    let bb_text= $('.filtertanggal option:selected' ).text();

    let tahun = $('.filtertahun').selectpicker('val');    

    let bbakhir = $('.filtertanggalakhir').selectpicker('val');
    let bb_textakhir= $('.filtertanggalakhir option:selected' ).text();

    let tahunakhir = $('.filtertahunakhir').selectpicker('val');    


    let kel = $('.filtergrup').selectpicker('val');    

    let table = $('#tabel-utama').DataTable({ 
            "language": 
                {
                 "url" :"<?php echo base_url('assets/vendor/datatables/lang/Indonesian.json');?>" ,
                 
                },
            processing: true,
            serverSide: true,
            "pageLength": 100,
            "searching": true, 
            order: [], //init datatable not ordering
            
            ajax: {
                url: "<?php echo site_url('panel/report_manual_ajax')?>",
                data:function(d){
                    d.grup = $('.filtergrup').selectpicker('val');
                    d.bulan = $('.filtertanggal').selectpicker('val');
                    d.tahun = $('.filtertahun').selectpicker('val');
                    d.tahunakhir = $('.filtertahunakhir').selectpicker('val');
                    d.bulanakhir = $('.filtertanggalakhir').selectpicker('val');
                }
                },
            "createdRow": function( row, data, dataIndex ) {                 
                $(row).addClass( 'align-middle' );
                 
                
            },            
            columnDefs: [
                { targets: [1,2], className: 'text-nowrap'}, //last column center.                
                { targets: '_all', className: 'text-center'}, //last column center.                               
                { targets: [1,2], searchable: true}, //last column center.              
                { targets: '_all', searchable: false}, //last column center.                
                { targets: [1,2], orderable: true}, //last column center.              
                { targets: '_all', orderable: false}, //last column center.  
                               
                
            ],
            "dom": 'Bfrti',
            buttons: [             
             
             {  extend: 'copy',                
                className: 'btn btn-info',
                 
             },
             {  extend: 'excel',
                title: function (){
                    return '<?php echo $title.' '; ?>'+kel_text+' '+bb_text+' '+tahun+' - '+bb_textakhir+' '+tahunakhir;
                },
                className: 'btn btn-success',
               
             },
             {  extend: 'print',                
                className: 'btn btn-default',
                 title: '',
                messageTop : function (){
                    return '<?php echo $title.' '; ?>'+kel_text+' '+bb_text+' '+tahun+' - '+bb_textakhir+' '+tahunakhir;
                },
                customize: function(win)
                    {
        
                        var last = null;
                        var current = null;
                        var bod = [];
        
                        var css = '@page { size: landscape; }',
                            head = win.document.head || win.document.getElementsByTagName('head')[0],
                            style = win.document.createElement('style');
        
                        style.type = 'text/css';
                        style.media = 'print';
        
                        if (style.styleSheet)
                        {
                        style.styleSheet.cssText = css;
                        }
                        else
                        {
                        style.appendChild(win.document.createTextNode(css));
                        }
        
                        head.appendChild(style);
                }
             },
         ],
    });



    $('.filtergrup').change(function(event){
        kel = $('.filtergrup').selectpicker('val'); 
        kel_text =$('.filtergrup option:selected' ).text();   
        table.ajax.reload();

        
    })
    $('.filtertanggal').change(function(event){        
        bb = $('.filtertanggal').selectpicker('val');
        bb_text= $('.filtertanggal option:selected' ).text();
        table.ajax.reload();

        console.log(bb_text +' '+bb_textakhir);
    })
    $('.filtertahun').change(function(event){
        tahun = $('.filtertahun').selectpicker('val'); 
        table.ajax.reload();
        console.log(tahun +' '+tahunakhir);
    })
    $('.filtertanggalakhir').change(function(event){        
        bbakhir = $('.filtertanggalakhir').selectpicker('val');
        bb_textakhir= $('.filtertanggalakhir option:selected' ).text();
        table.ajax.reload();
        console.log(bb_text +' '+bb_textakhir);
    })
    $('.filtertahunakhir').change(function(event){
        tahunakhir = $('.filtertahunakhir').selectpicker('val'); 
        table.ajax.reload();
        console.log(tahun +' '+tahunakhir);
    })
    function reloadtable(){
         
         var column = table.column( 1 );
 
        // Toggle the visibility
        column.visible( ! column.visible() );
        
        let bb = $('.filtertanggal').selectpicker('val');
        let tahun = $('.filtertahun').selectpicker('val');        
        let dm = new Date(tahun, bb+1, 0).getDate();

        // if(dm == 31){
        //     // Get the column API object
        // var column = table.column(-1); 
        // // Toggle the visibility
        // column.visible(true);
        // }else if(dm == 30){
        //     var column = table.column(-1); 
        //     // Toggle the visibility
        //     column.visible(false);
        // }else if(dm == 29){
        //     var column = table.column(32); 
        //     // Toggle the visibility
        //     column.visible(true);
        //     var column2 = table.column([33,34]);
        //     column2.visible(false);
        // }else{
            
        //     var column2 = table.column([31,32,33]);
        //     column2.visible(false);
        // }
        // table.ajax.reload();
    }

    $('#tabel-utama').attr('style', 'border-collapse: collapse !important');
 
});
</script>
<?= $this->endSection() ?>
