<?php $this->extend('layout/page_layout'); ?>

<?= $this->section('content') ?>
        <?php if(session()->get('error')):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong><?php echo session()->get('error');?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif;?>
     <div class="card mb-4">
            <div class="card-header"> 
                <h3 class="card-title float-left"><?=$title;?></h3>
                <button class="btn btn-success float-right" onclick="adduser()">Tambah</button>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-12">
                <div class="form-inline form-group">
                <label for="kelas" class="mb-0 pb-0 mr-2">Kelas :</label> 
                            <select name="kelas"  class="filterkelas form-control" title="Pilih Kelas" required> 
                                <option  value selected>Semua</option>
                                <?php 
                                    foreach ($kelas as $k) {
                                        echo '<option value="'.$k['id_kelas'].'" > '.$k['nama_kelas'].' </option>';
                                    }
                                ?>
                            </select>
                </div>
                <table id="tabel-utama" class="table table-striped table-bordered datatable">
                    <thead>
                        <tr>
                        <th>No</th>
                        <th>Nama Siswa</th>
                        <th>NIS</th>
                        <th>Kelas</th>
                        <th>Alamat</th>
                        <th>No WhatsApp</th>
                        <th>Tindakan</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                     
                    </table>
                </div>
                 
              </div>
            </div>
          </div>

<!-- Modal Add Product-->
<form id="form-tambah-siswa" action="<?php echo base_url('panel/d/siswa_add');?>" method="post">
         <div class="modal fade " id="myModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
               <div class="modal-content  ">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Data Siswa</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                   
                   </div>
                   <div class="modal-body">
                        <div class="form-group">
                            <label for="nama" class="mb-0 pb-0">Nama Siswa</label>
                            <input type="hidden" name="ids" value="0">
                            <input type="hidden" name="token" value="<?=$token;?>">
                           <input type="text" name="nama" class="form-control" placeholder="Nama Siswa" autocomplete="off">                            
                       </div>
                        <div class="form-group">
                            <label for="nis" class="mb-0 pb-0">NIS</label>  
                            <small class="nisid"> 
                                <div class="spinner-border spinner-border-sm  loadingiconnisid" role="status"  style="display:none">
                                    <span class="sr-only">Loading...</span>
                                </div>    
                                <i class="cil-x-circle nisid-gagal text-danger"  > Identitas sudah terpakai</i>
                                <i class="cil-check-circle nisid-ok"  ></i>
                                 </small>     
                            <input type="text" id="nis" name="nis" class="form-control" placeholder="101101" autocomplete="off" required>
                       </div>
                       <div class="form-group">
                            <label for="alamat" class="mb-0 pb-0">Alamat</label>       
                            <input type="text" name="alamat" class="form-control" placeholder="">
                       </div>
                        
                       <div class="form-group">
                            <label for="kelas" class="mb-0 pb-0">Kelas</label>
                            <select name="kelas" class="form-control" data-live-search="true" title="Pilih Kelas" required> 
                                <?php 
                                    foreach ($kelas as $k) {
                                        echo '<option value="'.$k['id_kelas'].'" > '.$k['nama_kelas'].' </option>';
                                    }
                                ?>
                            </select>
                       </div>                           
                       <div class="form-group">
                            <label for="telp" class="mb-0 pb-0">No WhatsApp Wali</label>       
                            <input type="text" name="telp" id="telp" class="form-control" autocomplete="off" placeholder="">
                       </div>               
 
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" name="submit" value="submit" class="btn btn-success tombolsubmit">Save</button>
                   </div>
                    </div>
            </div>
         </div>
</form>
 

 <!-- Modal Update Product-->
<form id="updateform" action="<?php echo base_url('panel/d/siswa_update');?>" method="post">
         <div class="modal fade" id="ModalUpdate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
               <div class="modal-content">
                   <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Update Transaksi <strong class="idmaster"></strong></h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                        <div class="form-group">
                            <label for="nama" class="mb-0 pb-0">Nama Siswa</label>
                            <input type="hidden" name="ids" value="0">
                            <input type="hidden" name="token" value="<?=$token;?>">
                           <input type="text" name="nama" class="form-control" placeholder="Nama Siswa" autocomplete="off">                            
                       </div>
                        <div class="form-group">
                            <label for="nis" class="mb-0 pb-0">NIS</label>  
                             
                            <input type="text" id="nis" name="nis" class="form-control" placeholder="101101" autocomplete="off" readonly>
                       </div>
                       <div class="form-group">
                            <label for="alamat" class="mb-0 pb-0">Alamat</label>       
                            <input type="text" name="alamat" class="form-control" placeholder="">
                       </div>
                        
                       <div class="form-group">
                            <label for="kelas" class="mb-0 pb-0">Kelas</label>
                            <select name="kelas" class="form-control" data-live-search="true" title="Pilih Kelas" required> 
                                <?php 
                                    foreach ($kelas as $k) {
                                        echo '<option value="'.$k['id_kelas'].'" > '.$k['nama_kelas'].' </option>';
                                    }
                                ?>
                            </select>
                       </div>                           
                       <div class="form-group">
                            <label for="telp" class="mb-0 pb-0">No WhatsApp Wali</label>       
                            <input type="text" name="telp" id="telp" class="form-control" autocomplete="off" placeholder="">
                       </div>               
 
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>                        
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Update</button>
                   </div>
                    </div>
            </div>
         </div>
</form>
  
  <!-- Modal delete Product-->
  <form id="deleteform" action="<?php echo base_url('panel/d/siswa_delete');?>" method="post">
         <div class="modal fade" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Hapus Transaksi</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                           <input type="hidden" name="id" class="form-control" required>
                                                 <strong>Apakah anda yakin akan menghapus data ini?</strong>
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Hapus</button>
                   </div>
                    </div>
            </div>
         </div>
    </form>
<?= $this->endSection() ?>


<?= $this->section('jslibrary') ?>
<script src="<?php echo base_url('assets/vendor/datatables/datatables.min.js');?>"></script>
 
<script>

        $('.tombolsubmit').hide();
        
        // SET IDENTITAS NUMBER ONLY
        setInputFilter(document.getElementById("telp"), function(value) {
            return /^\d*\.?\d*$/.test(value); // Allow digits and '.' only, using a RegExp
        });
        // SET IDENTITAS NUMBER ONLY
        setInputFilter(document.getElementById("nis"), function(value) {
            return /^\d*\.?\d*$/.test(value); // Allow digits and '.' only, using a RegExp
        });
        function setInputFilter(textbox, inputFilter) {
            ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
                textbox.addEventListener(event, function() {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
                });
            });
        }



        // check ID
        $('input[name=nis]').change(function(){
           $('.nisid').removeClass('text-success').removeClass('text-danger');
            $('.loadingiconnisid').show();
            $('.nisid-gagal').hide();
            $('.nisid-ok').hide();
            var user= $(this).val();
            $('.tombolsubmit').hide();
            $.ajax({
                method: "POST",
                url: "<?php echo base_url('panel/d/check_nis');?>",
                data: {nis: user}
                }).done(function(msg ) {
                        setTimeout(
                    function() 
                    {   
                        if(msg=='1'){
                            $('.nisid').addClass('text-success');
                            $('.loadingiconnisid').hide(); 
                            $('.nisid-ok').show();
                            $('.tombolsubmit').show();
                        }else{
                            $('.nisid').addClass('text-danger');
                            $('.loadingiconnisid').hide(); 
                            $('.nisid-gagal').show();
                            $('.tombolsubmit').hide();
                        }
                    }, 1000);
                
                });
            
            });

        function adduser(){
            resetform();
            $('#myModalAdd').modal('show')
        }
        function resetform(){

            $('.loadingiconnisid').hide();
            $('.nisid-gagal').hide();
            $('.nisid-ok').hide();
            $('#form-tambah-siswa').trigger("reset");
            
        }


 $(document).ready(function() {
     
    $('select').selectpicker();
    

    let table = $('#tabel-utama').DataTable({ 
            "language": 
                {
                 "url" :"<?php echo base_url('assets/vendor/datatables/lang/Indonesian.json');?>" 
                },
            processing: true,
            serverSide: true,
            responsive: true,
            order: [], //init datatable not ordering
            ajax: {
                url: "<?php echo site_url('panel/siswaajax')?>",
                data:function(d){
                    d.kelas = $('.filterkelas').selectpicker('val');
                }
                },
            "createdRow": function( row, data, dataIndex ) {                 
                $(row).addClass( 'align-middle' );
                
            },            
            columnDefs: [
                
                { targets: -1, className: 'text-nowrap text-center'}, //last column center.
                { targets: 5, className: 'text-center'}, //last column center.
                { targets: [0,6], searchable: false}, //last column center.
               
                
            ],
            "dom": 'Bfrtip',
            buttons: [             
             
             {  extend: 'copy',                
                className: 'btn btn-info',
                exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
             },
             {  extend: 'excel',
                title: '<?php echo $title.' '.date('d-m-Y'); ?> ',
                className: 'btn btn-success',
                exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
             },
             {  extend: 'print',                
                className: 'btn btn-default',
                exportOptions: {
                    title: '<?php echo $title.' '.date('d-m-Y'); ?> ',
                    messageTop :'<?php echo $title.' '.date('d-m-Y'); ?> ',
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
             },
         ],
    });

    $('.filterkelas').change(function(event){
        table.ajax.reload();
    })

    $('#tabel-utama').attr('style', 'border-collapse: collapse !important');

    // get Edit Records
    $('#tabel-utama').on('click','.edit_record',function(){
            var id=$(this).data('id');
            var nis=$(this).data('nis');
            var nama=$(this).data('nama');
            var kelas=$(this).data('kelas');
            var alamat=$(this).data('alamat');
            var telp=$(this).data('telp');
            $('#updateform [name="ids"]').val(id);
            $('#updateform [name="nis"]').val(nis);
            $('#updateform [name="nama"]').val(nama);
            $('#updateform [name="kelas"]').selectpicker('val',kelas);     
            $('#updateform [name="alamat"]').val(alamat)
            $('#updateform [name="telp"]').val(telp)     
            $('#ModalUpdate').modal('show');
              
    });

    $('#tabel-utama').on('click','.delete_record',function(){
                var id=$(this).data('id');
                $('#ModalDelete').modal('show');
                $('#deleteform [name="id"]').val(id);
    });
});
</script>
<?= $this->endSection() ?>
