<?php $this->extend('layout/page_layout'); ?>

<?= $this->section('content') ?>
        <?php if(session()->get('error')):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong><?php echo session()->get('error');?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif;?>
     <div class="card mb-4">
            <div class="card-header"> 
                <h3 class="card-title float-left"><?=$title;?></h3>
                <button class="btn btn-success float-right" onclick="adduser()">Tambah</button>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-12">
                <div class="form-inline form-group">
                <label for="grup" class="mb-0 pb-0 mr-2">grup :</label> 
                            <select name="grup"  class="filtergrup form-control" title="Pilih grup" required> 
                                <option  value selected>Semua</option>
                                <?php 
                                    foreach ($grup as $k) {
                                        echo '<option value="'.$k['id_grup'].'" > '.$k['nama_grup'].' </option>';
                                    }
                                ?>
                            </select>
                </div>
                <table id="tabel-utama" class="table table-striped table-bordered datatable">
                    <thead>
                        <tr>
                        <th>No</th>
                        <th>Nama member</th>
                        <th>nokartu</th>
                        <th>grup</th>
                        <th>Alamat</th>
                        <th>No WhatsApp</th>
                        <th>Jam Absensi</th>
                        <th>Notifikasi</th>
                        <th>Tindakan</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                     
                    </table>
                </div>
                 
              </div>
            </div>
          </div>
<!-- Modal Available Card-->
         <div class="modal fade " id="ModalKartu" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
               <div class="modal-content  ">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Data Kartu Yang Belum Didaftarkan</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                   
                   </div>
                   <div class="modal-body"> 
                        <div class="row" >
                            <div class="col-12">
                            <table id="tabel-kartu" class="table table-striped table-bordered datatable" style="width:100%;">
                                <thead>
                                <tr>
                                <th>No</th>
                                <th>No Kartu</th>
                                <th>Asal Device</th>
                                <th>Jam</th>
                                <th>Tindakan</th>                            
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            
                            </table>
                            </div>
                        </div>             
                     </div>
                   <div class="modal-footer">
                        
                   </div>
                    </div>
            </div>
         </div>
 
<!-- Modal Add Product-->
<form id="form-tambah-member" action="<?php echo base_url('panel/d/member_add');?>" method="post">
         <div class="modal fade " id="myModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
               <div class="modal-content  ">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Data member</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                   
                   </div>
                   <div class="modal-body">
                        <div class="form-group">
                            <label for="nama" class="mb-0 pb-0">Nama member</label>
                            <input type="hidden" name="ids" value="0">
                            <input type="hidden" name="token" value="<?=$token;?>">
                           <input type="text" name="nama" class="form-control" placeholder="Nama member" autocomplete="off">                            
                       </div>
                        <div class="form-group">
                            <label for="nokartu" class="mb-0 pb-0">nokartu</label>                                  
                            <input type="text" id="nokartu" name="nokartu" class="form-control" placeholder="101101" autocomplete="off" required readonly>
                       </div>
                       <div class="form-group">
                            <label for="alamat" class="mb-0 pb-0">Alamat</label>       
                            <input type="text" name="alamat" class="form-control" placeholder="">
                       </div>
                        
                       <div class="form-group">
                            <label for="grup" class="mb-0 pb-0">grup</label>
                            <select name="grup" class="form-control" data-live-search="true" title="Pilih grup" required> 
                                <?php 
                                    foreach ($grup as $k) {
                                        echo '<option value="'.$k['id_grup'].'" > '.$k['nama_grup'].' </option>';
                                    }
                                ?>
                            </select>
                       </div>
                       <div class="form-group">
                            <label for="jam" class="mb-0 pb-0">Jam Absen</label>
                            <select name="jam" class="form-control" data-live-search="true" title="Pilih jam" required> 
                                <?php 
                                    foreach ($jam_absen as $j) {
                                        echo '<option value="'.$j['id_jam'].'" > '.$j['nama_jam'].' ('.$j['jam_masuk_start'].' - '.$j['jam_pulang_start'].') </option>';
                                    }
                                ?>
                            </select>
                       </div>          
                       <div class="form-group">
                            <label for="grup" class="mb-0 pb-0">Notifikasi</label>
                            <select name="notifikasi" class="form-control" data-live-search="true" title="Status Notifikasi" required> 
                                <option value="0">Tidak Aktif</option>
                                <option value="1">Aktif</option>
                                
                            </select>
                       </div>                           
                       <div class="form-group">
                            <label for="telp" class="mb-0 pb-0">No WhatsApp</label>       
                            <input type="text" name="telp" id="telp" class="form-control" autocomplete="off" placeholder="">
                       </div>               
 
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" name="submit" value="submit" class="btn btn-success tombolsubmit">Save</button>
                   </div>
                    </div>
            </div>
         </div>
</form>
 

 <!-- Modal Update Product-->
<form id="updateform" action="<?php echo base_url('panel/d/member_update');?>" method="post">
         <div class="modal fade" id="ModalUpdate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
               <div class="modal-content">
                   <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Update Transaksi <strong class="idmaster"></strong></h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                        <div class="form-group">
                            <label for="nama" class="mb-0 pb-0">Nama member</label>
                            <input type="hidden" name="ids" value="0">
                            <input type="hidden" name="token" value="<?=$token;?>">
                           <input type="text" name="nama" class="form-control" placeholder="Nama member" autocomplete="off">                            
                       </div>
                        <div class="form-group">
                            <label for="nokartu" class="mb-0 pb-0">nokartu</label>  
                             
                            <input type="text" id="nokartu" name="nokartu" class="form-control" placeholder="101101" autocomplete="off" readonly>
                       </div>
                       <div class="form-group">
                            <label for="alamat" class="mb-0 pb-0">Alamat</label>       
                            <input type="text" name="alamat" class="form-control" placeholder="">
                       </div>
                        
                       <div class="form-group">
                            <label for="grup" class="mb-0 pb-0">grup</label>
                            <select name="grup" class="form-control" data-live-search="true" title="Pilih grup" required> 
                                <?php 
                                    foreach ($grup as $k) {
                                        echo '<option value="'.$k['id_grup'].'" > '.$k['nama_grup'].' </option>';
                                    }
                                ?>
                            </select>
                       </div>  
                       <div class="form-group">
                            <label for="grup" class="mb-0 pb-0">Jam Absen</label>
                            <select name="jam" class="form-control" data-live-search="true" title="Pilih jam" required> 
                                <?php 
                                    foreach ($jam_absen as $j) {
                                        echo '<option value="'.$j['id_jam'].'" > '.$j['nama_jam'].' ('.$j['jam_masuk_start'].' - '.$j['jam_pulang_start'].') </option>';
                                    }
                                ?>
                            </select>
                       </div>       
                       <div class="form-group">
                            <label for="grup" class="mb-0 pb-0">Notifikasi</label>
                            <select name="notifikasi" class="form-control" data-live-search="true" title="Status Notifikasi" required> 
                                <option value="0">Tidak Aktif</option>
                                <option value="1">Aktif</option>
                                
                            </select>
                       </div>                         
                       <div class="form-group">
                            <label for="telp" class="mb-0 pb-0">No WhatsApp</label>       
                            <input type="text" name="telp" id="telp" class="form-control" autocomplete="off" placeholder="">
                       </div>               
 
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>                        
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Update</button>
                   </div>
                    </div>
            </div>
         </div>
</form>
  
  <!-- Modal delete Product-->
  <form id="deleteform" action="<?php echo base_url('panel/d/member_delete');?>" method="post">
         <div class="modal fade" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Hapus Transaksi</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                           <input type="hidden" name="id" class="form-control" required>
                                                 <strong>Apakah anda yakin akan menghapus data ini?</strong>
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Hapus</button>
                   </div>
                    </div>
            </div>
         </div>
    </form>
<?= $this->endSection() ?>


<?= $this->section('jslibrary') ?>
<script src="<?php echo base_url('assets/vendor/datatables/datatables.min.js');?>"></script>
 
<script>

 
        
        // SET IDENTITAS NUMBER ONLY
        setInputFilter(document.getElementById("telp"), function(value) {
            return /^\d*\.?\d*$/.test(value); // Allow digits and '.' only, using a RegExp
        });
         
        function setInputFilter(textbox, inputFilter) {
            ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
                textbox.addEventListener(event, function() {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
                });
            });
        }



         

        function adduser(){
            resetform();
            $('#ModalKartu').modal('show')
            // resetform();
            // $('#myModalAdd').modal('show')
        }
        function newmember(nokartu){
            $('#ModalKartu').modal('hide');
            resetform();
            $('#form-tambah-member [name="nokartu"]').val(nokartu);
            $('#myModalAdd').modal('show')
        }
        function resetform(){

            $('.loadingiconnokartuid').hide();
            $('.nokartuid-gagal').hide();
            $('.nokartuid-ok').hide();
            $('#form-tambah-member').trigger("reset");
            
        }


 $(document).ready(function() {
     
    $('select').selectpicker();
    

    let table = $('#tabel-utama').DataTable({ 
            "language": 
                {
                 "url" :"<?php echo base_url('assets/vendor/datatables/lang/Indonesian.json');?>" 
                },
            processing: true,
            serverSide: true,
            responsive: true,
            order: [], //init datatable not ordering
            ajax: {
                url: "<?php echo site_url('panel/memberajax')?>",
                data:function(d){
                    d.grup = $('.filtergrup').selectpicker('val');
                }
                },
            "createdRow": function( row, data, dataIndex ) {                 
                $(row).addClass( 'align-middle' );
                
            },            
            columnDefs: [
                
                { targets: -1, className: 'text-nowrap text-center'}, //last column center.
                { targets: 5, className: 'text-center'}, //last column center.
                { targets: [0,6], searchable: false}, //last column center.
               
                
            ],
            "dom": 'Bfrtip',
            buttons: [             
             
             {  extend: 'copy',                
                className: 'btn btn-info',
                exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
             },
             {  extend: 'excel',
                title: '<?php echo $title.' '.date('d-m-Y'); ?> ',
                className: 'btn btn-success',
                exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
             },
             {  extend: 'print',                
                className: 'btn btn-default',
                exportOptions: {
                    title: '<?php echo $title.' '.date('d-m-Y'); ?> ',
                    messageTop :'<?php echo $title.' '.date('d-m-Y'); ?> ',
                    columns: [ 0, 1, 2, 3,4,5,6 ]
                }
             },
         ],
    });

    let table2 = $('#tabel-kartu').DataTable({ 
            "language": 
                {
                 "url" :"<?php echo base_url('assets/vendor/datatables/lang/Indonesian.json');?>" 
                },
            processing: true,
            serverSide: true,
            responsive: true,
            order: [], //init datatable not ordering
            ajax: {
                url: "<?php echo site_url('panel/d/getcard')?>"                
                },                       
            columnDefs: [
                
                { targets: -1, className: 'text-nowrap text-center'}, //last column center.
                
               
                
            ],
            "dom": 'ti',
          
    });


    $('.filtergrup').change(function(event){
        table.ajax.reload();
    })

    $('#tabel-utama').attr('style', 'border-collapse: collapse !important');

    // get Edit Records
    $('#tabel-utama').on('click','.edit_record',function(){
            var id=$(this).data('id');
            var nokartu=$(this).data('nfc');
            var nama=$(this).data('nama');
            var grup=$(this).data('grup');
            var alamat=$(this).data('alamat');
            var telp=$(this).data('telp');
            var notif=$(this).data('notifikasi');
            var jam=$(this).data('jam');
            $('#updateform [name="ids"]').val(id);
            $('#updateform [name="nokartu"]').val(nokartu);
            console.log(nokartu);
            $('#updateform [name="nama"]').val(nama);
            $('#updateform [name="grup"]').selectpicker('val',grup);     
            $('#updateform [name="jam"]').selectpicker('val',jam);     
            $('#updateform [name="alamat"]').val(alamat)
            $('#updateform [name="telp"]').val(telp)     
            $('#updateform [name="notifikasi"]').selectpicker('val',notif);  
            $('#ModalUpdate').modal('show');
              
    });

    $('#tabel-utama').on('click','.delete_record',function(){
                var id=$(this).data('id');
                $('#ModalDelete').modal('show');
                $('#deleteform [name="id"]').val(id);
    });
});
</script>
<?= $this->endSection() ?>
