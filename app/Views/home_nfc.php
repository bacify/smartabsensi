<?php $this->extend('layout/page_layout'); ?>

<?= $this->section('content') ?>
<h1>Absensi Online NFC READER</h1>

<main >
    <div class="rowa">    

<button id="scanButton" class="btn btn-primary">Scan</button>
<!-- <button id="writeButton" class="btn btn-info">Write</button>     -->
    </div>
   
    <div class="row mt-2">    
        <div  id="qr-reader-results" class="col border border-info text-center" style="min-height:100px;">
             
        </div>        
    </div>
   
</main>


<?= $this->endSection() ?>


<?= $this->section('jslibrary') ?>
<script src="<?php echo base_url('assets/js/html5-qrcode.min.js');?>"></script>
 
<script>
// $('#qr-reader-results').hide();
// log = ChromeSamples.log;
let a =$('#qr-reader-results');
if (!("NDEFReader" in window))
  a.append(
    "Web NFC is not available.<br>" +
      'Fitur Ini Hanya bisa dibuka menggunakan hp Android dengan NFC.<br>'
  );
  scanButton.addEventListener("click", async () => {
    a.append("User clicked scan button<br>");

  try {
    const ndef = new NDEFReader();
    await ndef.scan();
    a.append("> Scan started<br>");

    ndef.addEventListener("readingerror", () => {
        a.append("Argh! Cannot read data from the NFC tag. Try another one?<br>");
    });

    ndef.addEventListener("reading", ({ message, serialNumber }) => {
        a.append(`> Serial Number: ${serialNumber}<br>`);
        a.append(`> Records: (${message.records.length})<br>`);
        a.append(`> Records: (${message.records})<br>`);
        for (const record of message.records) {
            a.append("Record type:  " + record.recordType+"<br>");
            a.append("MIME type:    " + record.mediaType+"<br>");
            a.append("Record id:    " + record.id+"<br>");
            switch (record.recordType) {
            case "text":
                a.append('record type text <br>');
                // TODO: Read text record with record data, lang, and encoding.
                break;
            case "url":
                // TODO: Read URL record with record data.
                a.append('record type URL <br>');
                break;
            default:
            a.append('record type other <br>');
                // TODO: Handle other records with record data.
            }
        }
        console.log(message.records);
    });
  } catch (error) {
    a.append("Argh! " + error+"<br>");
  }
});

writeButton.addEventListener("click", async () => {
    a.append("User clicked write button<br>");

  try {
  /*   const ndef = new NDEFReader();
    await ndef.write("Hello world!<br>"); */
    a.append("> Message written<br>");
  } catch (error) {
    a.append("Argh! " + error +"<br>");
  }
});
 
</script>
<?= $this->endSection() ?>
